use
sistema_final;

-- Para truncar en pruebas
truncate producto_entrega_mercaderia;
truncate entrega_mercaderia;

truncate producto_guia_remision;
truncate guia_remision;

truncate producto_nota_devolucion;
truncate nota_devolucion;

truncate producto_nota_pedido;
truncate nota_pedido;

truncate producto_venta;
truncate ventas;

truncate control_stock;
truncate stock_general;

-- No truncar
truncate producto;
truncate tipo_producto;
truncate dependencia_laboral;
truncate vendedor;



