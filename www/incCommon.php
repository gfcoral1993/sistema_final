<?php

	#########################################################
	/*
	~~~~~~ LIST OF FUNCTIONS ~~~~~~
		getTableList() -- returns an associative array (tableName => tableData, tableData is array(tableCaption, tableDescription, tableIcon)) of tables accessible by current user
		get_table_groups() -- returns an associative array (table_group => tables_array)
		logInMember() -- checks POST login. If not valid, redirects to index.php, else returns TRUE
		getTablePermissions($tn) -- returns an array of permissions allowed for logged member to given table (allowAccess, allowInsert, allowView, allowEdit, allowDelete) -- allowAccess is set to true if any access level is allowed
		get_sql_fields($tn) -- returns the SELECT part of the table view query
		get_sql_from($tn[, true, [, false]]) -- returns the FROM part of the table view query, with full joins (unless third paramaeter is set to true), optionally skipping permissions if true passed as 2nd param.
		get_joined_record($table, $id[, true]) -- returns assoc array of record values for given PK value of given table, with full joins, optionally skipping permissions if true passed as 3rd param.
		get_defaults($table) -- returns assoc array of table fields as array keys and default values (or empty), excluding automatic values as array values
		htmlUserBar() -- returns html code for displaying user login status to be used on top of pages.
		showNotifications($msg, $class) -- returns html code for displaying a notification. If no parameters provided, processes the GET request for possible notifications.
		parseMySQLDate(a, b) -- returns a if valid mysql date, or b if valid mysql date, or today if b is true, or empty if b is false.
		parseCode(code) -- calculates and returns special values to be inserted in automatic fields.
		addFilter(i, filterAnd, filterField, filterOperator, filterValue) -- enforce a filter over data
		clearFilters() -- clear all filters
		loadView($view, $data) -- passes $data to templates/{$view}.php and returns the output
		loadTable($table, $data) -- loads table template, passing $data to it
		filterDropdownBy($filterable, $filterers, $parentFilterers, $parentPKField, $parentCaption, $parentTable, &$filterableCombo) -- applies cascading drop-downs for a lookup field, returns js code to be inserted into the page
		br2nl($text) -- replaces all variations of HTML <br> tags with a new line character
		htmlspecialchars_decode($text) -- inverse of htmlspecialchars()
		entitiesToUTF8($text) -- convert unicode entities (e.g. &#1234;) to actual UTF8 characters, requires multibyte string PHP extension
		func_get_args_byref() -- returns an array of arguments passed to a function, by reference
		permissions_sql($table, $level) -- returns an array containing the FROM and WHERE additions for applying permissions to an SQL query
		error_message($msg[, $back_url]) -- returns html code for a styled error message .. pass explicit false in second param to suppress back button
		toMySQLDate($formattedDate, $sep = datalist_date_separator, $ord = datalist_date_format)
		reIndex(&$arr) -- returns a copy of the given array, with keys replaced by 1-based numeric indices, and values replaced by original keys
		get_embed($provider, $url[, $width, $height, $retrieve]) -- returns embed code for a given url (supported providers: youtube, googlemap)
		check_record_permission($table, $id, $perm = 'view') -- returns true if current user has the specified permission $perm ('view', 'edit' or 'delete') for the given recors, false otherwise
		NavMenus($options) -- returns the HTML code for the top navigation menus. $options is not implemented currently.
		StyleSheet() -- returns the HTML code for included style sheet files to be placed in the <head> section.
		getUploadDir($dir) -- if dir is empty, returns upload dir configured in defaultLang.php, else returns $dir.
		PrepareUploadedFile($FieldName, $MaxSize, $FileTypes='jpg|jpeg|gif|png', $NoRename=false, $dir="") -- validates and moves uploaded file for given $FieldName into the given $dir (or the default one if empty)
		get_home_links($homeLinks, $default_classes, $tgroup) -- process $homeLinks array and return custom links for homepage. Applies $default_classes to links if links have classes defined, and filters links by $tgroup (using '*' matches all table_group values)
		quick_search_html($search_term, $label, $separate_dv = true) -- returns HTML code for the quick search box.
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	*/

	#########################################################

	function getTableList($skip_authentication = false) {
		$arrAccessTables = [];
		$arrTables = [
			/* 'table_name' => ['table caption', 'homepage description', 'icon', 'table group name'] */   
			'bitacora' => ['BITACORA', 'S', 'resources/table_icons/newspaper_add.png', 'ADMINISTRACION'],
			'provincia' => ['PROVINCIA', 'EN ESTE M&#211;DULO ENCONTRAREMOS TODAS LAS PROVINCIAS DE ECUADOR', 'resources/table_icons/map_edit.png', 'GEOGRAFIA'],
			'canton' => ['CANT&#211;N', 'EN ESTE M&#211;DULO ENCONTRAREMOS TODOS LOS CANTONES DE ECUADOR', 'resources/table_icons/google_map.png', 'GEOGRAFIA'],
			'parroquia' => ['PARROQUIA', 'EN ESTE M&#211;DULO ENCONTRAREMOS TODAS LAS PARROQUIAS DE ECUADOR', 'resources/table_icons/map_magnify.png', 'GEOGRAFIA'],
			'dependencia_laboral' => ['DEPENDENCIA LABORAL', 'EN ESTE MODULO ENCONTRAREMOS LOS TIPOS DE DEPENDENCIA LABORAL DE LOS CLIENTES', 'resources/table_icons/network-share.png', 'VENTAS'],
			'tipo_producto' => ['TIPO DE PRODUCTO', 'EN ESTE M&#211;DULO ENCONTRAREMOS TODOS LOS TIPOS DE PRODUCTOS DE LA EMPRESA', 'resources/table_icons/barcode.png', 'INVENTARIOS'],
			'vendedor' => ['VENDEDORES', 'EN ESTE M&#211;DULO PODREMOS ENCONTRAR TODOS LOS VENDEDORES DE LA EMPRESA ', 'resources/table_icons/administrator.png', 'RRHH'],
			'tipo_movimiento' => ['TIPO DE MOVIMIENTO', 'EN ESTE M&#211;DULO SE REGISTRAN TODOS LOS MOVIMIENTOS POSIBLES PARA EL SISTEMA.', 'resources/table_icons/shape_move_back.png', 'ADMINISTRACION'],
			'aprobaciones' => ['APROBACIONES', '', 'resources/table_icons/lightning_add.png', 'VENTAS'],
			'cliente' => ['CLIENTE', '', 'resources/table_icons/client_account_template.png', 'VENTAS'],
			'producto' => ['PRODUCTO', 'EN ESTE M&#211;DULO ENCONTRAREMOS TODOS LOS PRODUCTOS DE LA EMPRESA', 'resources/table_icons/books.png', 'INVENTARIOS'],
			'ventas' => ['VENTAS', 'EN ESTE M&#211;DULO SE REGISTRAN LAS VENTAS DE LOS VENDEDORES.', 'resources/table_icons/money_add.png', 'VENTAS'],
			'producto_venta' => ['PRODUCTO VS VENTAS', 'RELACI&#211;N DE LOS PRODUCTOS VS LAS VENTAS DE LOS VENDEDORES A LA EMPRESA.', 'resources/table_icons/newspaper_go.png', 'VENTAS'],
			'nota_pedido' => ['NOTA DE PEDIDO', 'EN ESTE M&#211;DULO SE REGISTRAN LAS NOTAS DE PEDIDOS QUE REALIZAN LOS VENDEDORES A LA EMPRESA.', 'resources/table_icons/newspaper_add.png', 'INVENTARIOS'],
			'producto_nota_pedido' => ['PROD. VS NOTA PEDIDO', 'RELACI&#211;N DE LOS PRODUCTOS VS LAS NOTAS DE PEDIDOS DE LOS VENDEDORES A LA EMPRESA.', 'resources/table_icons/newspaper_add.png', 'INVENTARIOS'],
			'nota_devolucion' => ['DEVOLUCIONES', 'EN ESTE M&#211;DULO SE REGISTRAN LAS DEVOLUCIONES DE LOS VENDEDORES.', 'resources/table_icons/cart_put.png', 'INVENTARIOS'],
			'producto_nota_devolucion' => ['PROD. VS NOTA DEVOLUCI&#211;N', 'RELACI&#211;N DE LOS PRODUCTOS VS LAS NOTAS DE DEVOLUCI&#211;N DE LOS VENDEDORES.', 'resources/table_icons/newspaper_delete.png', 'INVENTARIOS'],
			'stock_general' => ['STOCK GENERAL', 'EN ESTE M&#211;DULO SE MUESTRA EL STOCK ACTUAL DE TODOS LOS PRODUCTOS DE LOS VENDEDORES.', 'resources/table_icons/statistics.png', 'INVENTARIOS'],
			'control_stock' => ['CONTROL STOCK', 'EN ESTE M&#211;DULO PODREMOS VER TODOS LOS MOVIMIENTOS DE PRODUCTOS: VENTAS, NOTAS DE PEDIDO, NOTAS DE DEVOLUCIONES.', 'resources/table_icons/chart_stock.png', 'INVENTARIOS'],
			'guia_remision' => ['GUIA DE REMISION', 'MODULO QUE CONTENDRA TODAS LAS GUIAS DE REMISION', 'resources/table_icons/to_do_list_cheked_all.png', 'BODEGA'],
			'producto_guia_remision' => ['PROD VS GUIA REMISION', 'RELACI&#211;N DE LOS PRODUCTOS VS LA GUIA DE REMISION DE LOS VENDEDORES A LA EMPRESA.', 'resources/table_icons/newspaper_go.png', 'BODEGA'],
			'tipo_venta' => ['TIPO DE VENTA  ', 'EN ESTE M&#211;DULO SE REGISTRAN TODOS LOS TIPOS DE VENTAS POSIBLES PARA EL SISTEMA.', 'resources/table_icons/shape_move_back.png', 'VENTAS'],
			'inventario_bodega_central' => ['INVENTARIO BC', 'INVENTARIO BODEGA CENTRAL', 'resources/table_icons/calendar_copy.png', 'BODEGA'],
			'compras' => ['COMPRAS', 'MODULO DE COMPRAS', 'resources/table_icons/textfield_add.png', 'BODEGA'],
			'compra_producto' => ['COMPRA VS PRODUCTO', 'MODULO DE COMPRA VS PRODUCTO', 'resources/table_icons/production_copyleft.png', 'BODEGA'],
			'ruc' => ['Ruc', '', 'resources/table_icons/small_business.png', 'BODEGA'],
		];
		if($skip_authentication || getLoggedAdmin()) return $arrTables;

		if(is_array($arrTables)) {
			foreach($arrTables as $tn => $tc) {
				$arrPerm = getTablePermissions($tn);
				if($arrPerm['access']) $arrAccessTables[$tn] = $tc;
			}
		}

		return $arrAccessTables;
	}

	#########################################################

	function get_table_groups($skip_authentication = false) {
		$tables = getTableList($skip_authentication);
		$all_groups = ['BODEGA', 'INVENTARIOS', 'VENTAS', 'ADMINISTRACION', 'GEOGRAFIA', 'RRHH', 'None'];

		$groups = [];
		foreach($all_groups as $grp) {
			foreach($tables as $tn => $td) {
				if($td[3] && $td[3] == $grp) $groups[$grp][] = $tn;
				if(!$td[3]) $groups[0][] = $tn;
			}
		}

		return $groups;
	}

	#########################################################

	function getTablePermissions($tn) {
		static $table_permissions = [];
		if(isset($table_permissions[$tn])) return $table_permissions[$tn];

		$groupID = getLoggedGroupID();
		$memberID = makeSafe(getLoggedMemberID());
		$res_group = sql("SELECT `tableName`, `allowInsert`, `allowView`, `allowEdit`, `allowDelete` FROM `membership_grouppermissions` WHERE `groupID`='{$groupID}'", $eo);
		$res_user  = sql("SELECT `tableName`, `allowInsert`, `allowView`, `allowEdit`, `allowDelete` FROM `membership_userpermissions`  WHERE LCASE(`memberID`)='{$memberID}'", $eo);

		while($row = db_fetch_assoc($res_group)) {
			$table_permissions[$row['tableName']] = [
				1 => intval($row['allowInsert']),
				2 => intval($row['allowView']),
				3 => intval($row['allowEdit']),
				4 => intval($row['allowDelete']),
				'insert' => intval($row['allowInsert']),
				'view' => intval($row['allowView']),
				'edit' => intval($row['allowEdit']),
				'delete' => intval($row['allowDelete'])
			];
		}

		// user-specific permissions, if specified, overwrite his group permissions
		while($row = db_fetch_assoc($res_user)) {
			$table_permissions[$row['tableName']] = [
				1 => intval($row['allowInsert']),
				2 => intval($row['allowView']),
				3 => intval($row['allowEdit']),
				4 => intval($row['allowDelete']),
				'insert' => intval($row['allowInsert']),
				'view' => intval($row['allowView']),
				'edit' => intval($row['allowEdit']),
				'delete' => intval($row['allowDelete'])
			];
		}

		// if user has any type of access, set 'access' flag
		foreach($table_permissions as $t => $p) {
			$table_permissions[$t]['access'] = $table_permissions[$t][0] = false;

			if($p['insert'] || $p['view'] || $p['edit'] || $p['delete']) {
				$table_permissions[$t]['access'] = $table_permissions[$t][0] = true;
			}
		}

		return $table_permissions[$tn];
	}

	#########################################################

	function get_sql_fields($table_name) {
		$sql_fields = [
			'bitacora' => "`bitacora`.`id_bitacora` as 'id_bitacora', `bitacora`.`texto_bitacora` as 'texto_bitacora'",
			'provincia' => "`provincia`.`id_pr` as 'id_pr', if(`provincia`.`fecha_creacion_pr`,date_format(`provincia`.`fecha_creacion_pr`,'%d/%m/%Y %h:%i %p'),'') as 'fecha_creacion_pr', `provincia`.`nombre_pr` as 'nombre_pr', `provincia`.`estado` as 'estado'",
			'canton' => "`canton`.`id_c` as 'id_c', if(`canton`.`fecha_creacion_c`,date_format(`canton`.`fecha_creacion_c`,'%d/%m/%Y %h:%i %p'),'') as 'fecha_creacion_c', IF(    CHAR_LENGTH(`provincia1`.`nombre_pr`), CONCAT_WS('',   `provincia1`.`nombre_pr`), '') as 'codigo_provincia_c', `canton`.`nombre_c` as 'nombre_c', `canton`.`estado` as 'estado'",
			'parroquia' => "`parroquia`.`id_pa` as 'id_pa', if(`parroquia`.`fecha_creacion_pa`,date_format(`parroquia`.`fecha_creacion_pa`,'%d/%m/%Y %h:%i %p'),'') as 'fecha_creacion_pa', IF(    CHAR_LENGTH(`canton1`.`nombre_c`), CONCAT_WS('',   `canton1`.`nombre_c`), '') as 'codigo_canton_pa', `parroquia`.`nombre_pa` as 'nombre_pa', `parroquia`.`estado` as 'estado'",
			'dependencia_laboral' => "`dependencia_laboral`.`id_dependencia_laboral` as 'id_dependencia_laboral', `dependencia_laboral`.`nombre_dependencia_laboral` as 'nombre_dependencia_laboral'",
			'tipo_producto' => "`tipo_producto`.`id_tp` as 'id_tp', if(`tipo_producto`.`fecha_creacion_tp`,date_format(`tipo_producto`.`fecha_creacion_tp`,'%d/%m/%Y %h:%i %p'),'') as 'fecha_creacion_tp', `tipo_producto`.`nombre_tp` as 'nombre_tp', `tipo_producto`.`estado` as 'estado'",
			'vendedor' => "`vendedor`.`id_vendedor` as 'id_vendedor', `vendedor`.`user` as 'user', `vendedor`.`nombre` as 'nombre', `vendedor`.`apellido` as 'apellido', `vendedor`.`email_personal` as 'email_personal', `vendedor`.`email_trabajo` as 'email_trabajo', `vendedor`.`cedula` as 'cedula', `vendedor`.`celular` as 'celular', `vendedor`.`telefono` as 'telefono', `vendedor`.`aprobado` as 'aprobado', `vendedor`.`fecha_registro` as 'fecha_registro', `vendedor`.`codigo_lead` as 'codigo_lead'",
			'tipo_movimiento' => "`tipo_movimiento`.`id` as 'id', `tipo_movimiento`.`nombre` as 'nombre'",
			'aprobaciones' => "`aprobaciones`.`id_aprobaciones` as 'id_aprobaciones', `aprobaciones`.`nombre_aprobaciones` as 'nombre_aprobaciones'",
			'cliente' => "`cliente`.`id_cliente` as 'id_cliente', `cliente`.`cedula_cliente` as 'cedula_cliente', `cliente`.`nombre_cliente` as 'nombre_cliente', `cliente`.`apellido_cliente` as 'apellido_cliente', `cliente`.`correo_cliente` as 'correo_cliente', `cliente`.`celular_cliente` as 'celular_cliente'",
			'producto' => "`producto`.`id_p` as 'id_p', if(`producto`.`fecha_creacion_p`,date_format(`producto`.`fecha_creacion_p`,'%d/%m/%Y %h:%i %p'),'') as 'fecha_creacion_p', `producto`.`codigo_p` as 'codigo_p', `producto`.`nombre_p` as 'nombre_p', IF(    CHAR_LENGTH(`tipo_producto1`.`nombre_tp`), CONCAT_WS('',   `tipo_producto1`.`nombre_tp`), '') as 'tipo_producto_p', `producto`.`estado` as 'estado', `producto`.`pvp_producto` as 'pvp_producto', `producto`.`manejo_serie` as 'manejo_serie'",
			'ventas' => "`ventas`.`id` as 'id', IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') as 'vendedor', `ventas`.`fecha_ingreso` as 'fecha_ingreso', `ventas`.`usuario_creador` as 'usuario_creador', `ventas`.`fecha_actualizacion` as 'fecha_actualizacion', `ventas`.`usuario_actualizo` as 'usuario_actualizo', if(`ventas`.`fecha`,date_format(`ventas`.`fecha`,'%d/%m/%Y'),'') as 'fecha', `ventas`.`numero_contrato` as 'numero_contrato', `ventas`.`cedula` as 'cedula', `ventas`.`nombre` as 'nombre', `ventas`.`mes_comision_venta` as 'mes_comision_venta', `ventas`.`valor_contrato` as 'valor_contrato', `ventas`.`pvp` as 'pvp', IF(    CHAR_LENGTH(`vendedor2`.`nombre`) || CHAR_LENGTH(`vendedor2`.`apellido`), CONCAT_WS('',   `vendedor2`.`nombre`, ' ', `vendedor2`.`apellido`), '') as 'a_medias', IF(    CHAR_LENGTH(`dependencia_laboral1`.`nombre_dependencia_laboral`), CONCAT_WS('',   `dependencia_laboral1`.`nombre_dependencia_laboral`), '') as 'dependencia_laboral', IF(    CHAR_LENGTH(`aprobaciones1`.`nombre_aprobaciones`), CONCAT_WS('',   `aprobaciones1`.`nombre_aprobaciones`), '') as 'aprobaciones', `ventas`.`cobro_particular` as 'cobro_particular', `ventas`.`entrega_cedula` as 'entrega_cedula', `ventas`.`cuotas` as 'cuotas', `ventas`.`valor_cuota` as 'valor_cuota', `ventas`.`cantidad_productos` as 'cantidad_productos', IF(    CHAR_LENGTH(`provincia1`.`nombre_pr`), CONCAT_WS('',   `provincia1`.`nombre_pr`), '') as 'provincia', IF(    CHAR_LENGTH(`canton1`.`nombre_c`), CONCAT_WS('',   `canton1`.`nombre_c`), '') as 'canton', IF(    CHAR_LENGTH(`parroquia1`.`nombre_pa`), CONCAT_WS('',   `parroquia1`.`nombre_pa`), '') as 'parroquia', `ventas`.`categorias_descripcion` as 'categorias_descripcion', IF(    CHAR_LENGTH(`tipo_venta1`.`nombre_tipo_venta`), CONCAT_WS('',   `tipo_venta1`.`nombre_tipo_venta`), '') as 'tipo_venta'",
			'producto_venta' => "`producto_venta`.`id` as 'id', IF(    CHAR_LENGTH(`ventas1`.`numero_contrato`), CONCAT_WS('',   `ventas1`.`numero_contrato`), '') as 'id_venta', IF(    CHAR_LENGTH(`producto1`.`nombre_p`), CONCAT_WS('',   `producto1`.`nombre_p`), '') as 'id_producto', `producto_venta`.`cantidad` as 'cantidad'",
			'nota_pedido' => "`nota_pedido`.`id` as 'id', `nota_pedido`.`fecha_ingreso` as 'fecha_ingreso', `nota_pedido`.`numero_guia` as 'numero_guia', IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') as 'vendedor', if(`nota_pedido`.`fecha`,date_format(`nota_pedido`.`fecha`,'%d/%m/%Y'),'') as 'fecha', `nota_pedido`.`cantidad_productos` as 'cantidad_productos', `nota_pedido`.`observaciones` as 'observaciones', `nota_pedido`.`usuario_creador` as 'usuario_creador'",
			'producto_nota_pedido' => "`producto_nota_pedido`.`id` as 'id', IF(    CHAR_LENGTH(`nota_pedido1`.`numero_guia`), CONCAT_WS('',   `nota_pedido1`.`numero_guia`), '') as 'id_nota_pedido', IF(    CHAR_LENGTH(`producto1`.`nombre_p`), CONCAT_WS('',   `producto1`.`nombre_p`), '') as 'id_producto', `producto_nota_pedido`.`cantidad` as 'cantidad'",
			'nota_devolucion' => "`nota_devolucion`.`id` as 'id', `nota_devolucion`.`fecha_ingreso` as 'fecha_ingreso', `nota_devolucion`.`numero_guia` as 'numero_guia', IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') as 'vendedor', if(`nota_devolucion`.`fecha`,date_format(`nota_devolucion`.`fecha`,'%d/%m/%Y'),'') as 'fecha', `nota_devolucion`.`cantidad_productos` as 'cantidad_productos', `nota_devolucion`.`observaciones` as 'observaciones', `nota_devolucion`.`usuario_creador` as 'usuario_creador', `nota_devolucion`.`archivo_nota_devolucion` as 'archivo_nota_devolucion'",
			'producto_nota_devolucion' => "`producto_nota_devolucion`.`id` as 'id', IF(    CHAR_LENGTH(`nota_devolucion1`.`numero_guia`), CONCAT_WS('',   `nota_devolucion1`.`numero_guia`), '') as 'id_nota_devolucion', IF(    CHAR_LENGTH(`producto1`.`nombre_p`), CONCAT_WS('',   `producto1`.`nombre_p`), '') as 'id_producto', `producto_nota_devolucion`.`cantidad` as 'cantidad', `producto_nota_devolucion`.`numero_serie_imei` as 'numero_serie_imei'",
			'stock_general' => "`stock_general`.`id` as 'id', `stock_general`.`usuario_creador` as 'usuario_creador', `stock_general`.`usuario_actualizo` as 'usuario_actualizo', `stock_general`.`fecha_ingreso` as 'fecha_ingreso', `stock_general`.`fecha_actualizacion` as 'fecha_actualizacion', IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') as 'vendedor', `stock_general`.`ano` as 'ano', `stock_general`.`mes` as 'mes', IF(    CHAR_LENGTH(`producto1`.`id_p`), CONCAT_WS('',   `producto1`.`id_p`), '') as 'id_producto', IF(    CHAR_LENGTH(`producto2`.`codigo_p`), CONCAT_WS('',   `producto2`.`codigo_p`), '') as 'codigo_producto', IF(    CHAR_LENGTH(`tipo_producto1`.`nombre_tp`), CONCAT_WS('',   `tipo_producto1`.`nombre_tp`), '') as 'tipo_producto', IF(    CHAR_LENGTH(`producto4`.`nombre_p`), CONCAT_WS('',   `producto4`.`nombre_p`), '') as 'nombre_producto', `stock_general`.`cantidad` as 'cantidad', `stock_general`.`pvp` as 'pvp'",
			'control_stock' => "`control_stock`.`id` as 'id', `control_stock`.`fecha_creacion` as 'fecha_creacion', if(`control_stock`.`fecha_movimiento`,date_format(`control_stock`.`fecha_movimiento`,'%d/%m/%Y'),'') as 'fecha_movimiento', IF(    CHAR_LENGTH(`tipo_movimiento1`.`nombre`), CONCAT_WS('',   `tipo_movimiento1`.`nombre`), '') as 'tipo_movimiento', `control_stock`.`id_transaccion` as 'id_transaccion', `control_stock`.`numero_guia_contrato` as 'numero_guia_contrato', IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') as 'vendedor', `control_stock`.`cliente` as 'cliente', IF(    CHAR_LENGTH(`tipo_producto1`.`nombre_tp`), CONCAT_WS('',   `tipo_producto1`.`nombre_tp`), '') as 'categoria_producto', IF(    CHAR_LENGTH(`producto1`.`nombre_p`), CONCAT_WS('',   `producto1`.`nombre_p`), '') as 'producto', IF(    CHAR_LENGTH(`producto1`.`codigo_p`), CONCAT_WS('',   `producto1`.`codigo_p`), '') as 'codigo_producto', `control_stock`.`cantidad` as 'cantidad'",
			'guia_remision' => "`guia_remision`.`numero_guia_remision` as 'numero_guia_remision', `guia_remision`.`fecha_creacion_guia_remision` as 'fecha_creacion_guia_remision', `guia_remision`.`usuario_creador` as 'usuario_creador', `guia_remision`.`fecha_actualizacion` as 'fecha_actualizacion', `guia_remision`.`usuario_actualizo` as 'usuario_actualizo', if(`guia_remision`.`fecha_inicio_traslado`,date_format(`guia_remision`.`fecha_inicio_traslado`,'%d/%m/%Y'),'') as 'fecha_inicio_traslado', if(`guia_remision`.`fecha_terminacion_traslado`,date_format(`guia_remision`.`fecha_terminacion_traslado`,'%d/%m/%Y'),'') as 'fecha_terminacion_traslado', `guia_remision`.`motivo_traslado` as 'motivo_traslado', IF(    CHAR_LENGTH(`canton1`.`nombre_c`), CONCAT_WS('',   `canton1`.`nombre_c`), '') as 'punto_partida', `guia_remision`.`destinatario` as 'destinatario', `guia_remision`.`ruc_ci` as 'ruc_ci', IF(    CHAR_LENGTH(`provincia1`.`nombre_pr`), CONCAT_WS('',   `provincia1`.`nombre_pr`), '') as 'punto_llegada', IF(    CHAR_LENGTH(`canton2`.`nombre_c`), CONCAT_WS('',   `canton2`.`nombre_c`), '') as 'punto_llegada_canton', `guia_remision`.`placa` as 'placa', `guia_remision`.`persona_encarga_nombre` as 'persona_encarga_nombre', `guia_remision`.`ruc_ci_persona_encargada` as 'ruc_ci_persona_encargada', IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') as 'vendedor', IF(    CHAR_LENGTH(`nota_pedido1`.`numero_guia`), CONCAT_WS('',   `nota_pedido1`.`numero_guia`), '') as 'nota_pedido', `guia_remision`.`archivo_guia_remision` as 'archivo_guia_remision', `guia_remision`.`archivo_entrega_mercaderia` as 'archivo_entrega_mercaderia'",
			'producto_guia_remision' => "`producto_guia_remision`.`id` as 'id', `producto_guia_remision`.`id_guia_remision` as 'id_guia_remision', IF(    CHAR_LENGTH(`producto1`.`nombre_p`), CONCAT_WS('',   `producto1`.`nombre_p`), '') as 'id_producto', IF(    CHAR_LENGTH(`producto1`.`codigo_p`), CONCAT_WS('',   `producto1`.`codigo_p`), '') as 'codigo_producto', `producto_guia_remision`.`cantidad` as 'cantidad', IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') as 'vendedor', `producto_guia_remision`.`mes` as 'mes', `producto_guia_remision`.`numero_serie_imei` as 'numero_serie_imei'",
			'tipo_venta' => "`tipo_venta`.`id_tipo_venta` as 'id_tipo_venta', `tipo_venta`.`nombre_tipo_venta` as 'nombre_tipo_venta'",
			'inventario_bodega_central' => "`inventario_bodega_central`.`id_inventario_bodega_central` as 'id_inventario_bodega_central', IF(    CHAR_LENGTH(`producto1`.`codigo_p`), CONCAT_WS('',   `producto1`.`codigo_p`), '') as 'codigo_producto', `inventario_bodega_central`.`nombre_producto` as 'nombre_producto', `inventario_bodega_central`.`cantidad_producto` as 'cantidad_producto'",
			'compras' => "`compras`.`id_compras` as 'id_compras', `compras`.`numero_factura` as 'numero_factura', IF(    CHAR_LENGTH(`ruc1`.`nombre_ruc`), CONCAT_WS('',   `ruc1`.`nombre_ruc`), '') as 'ruc'",
			'compra_producto' => "`compra_producto`.`id_compra_producto` as 'id_compra_producto', IF(    CHAR_LENGTH(`compras1`.`numero_factura`), CONCAT_WS('',   `compras1`.`numero_factura`), '') as 'compra_compra_producto', IF(    CHAR_LENGTH(`producto1`.`codigo_p`), CONCAT_WS('',   `producto1`.`codigo_p`), '') as 'producto_compra_producto', IF(    CHAR_LENGTH(`producto1`.`nombre_p`), CONCAT_WS('',   `producto1`.`nombre_p`), '') as 'nombre_producto_compra_producto'",
			'ruc' => "`ruc`.`id_ruc` as 'id_ruc', `ruc`.`nombre_ruc` as 'nombre_ruc', `ruc`.`numero_ruc` as 'numero_ruc'",
		];

		if(isset($sql_fields[$table_name])) return $sql_fields[$table_name];

		return false;
	}

	#########################################################

	function get_sql_from($table_name, $skip_permissions = false, $skip_joins = false, $lower_permissions = false) {
		$sql_from = [
			'bitacora' => "`bitacora` ",
			'provincia' => "`provincia` ",
			'canton' => "`canton` LEFT JOIN `provincia` as provincia1 ON `provincia1`.`id_pr`=`canton`.`codigo_provincia_c` ",
			'parroquia' => "`parroquia` LEFT JOIN `canton` as canton1 ON `canton1`.`id_c`=`parroquia`.`codigo_canton_pa` ",
			'dependencia_laboral' => "`dependencia_laboral` ",
			'tipo_producto' => "`tipo_producto` ",
			'vendedor' => "`vendedor` ",
			'tipo_movimiento' => "`tipo_movimiento` ",
			'aprobaciones' => "`aprobaciones` ",
			'cliente' => "`cliente` ",
			'producto' => "`producto` LEFT JOIN `tipo_producto` as tipo_producto1 ON `tipo_producto1`.`id_tp`=`producto`.`tipo_producto_p` ",
			'ventas' => "`ventas` LEFT JOIN `vendedor` as vendedor1 ON `vendedor1`.`id_vendedor`=`ventas`.`vendedor` LEFT JOIN `vendedor` as vendedor2 ON `vendedor2`.`id_vendedor`=`ventas`.`a_medias` LEFT JOIN `dependencia_laboral` as dependencia_laboral1 ON `dependencia_laboral1`.`id_dependencia_laboral`=`ventas`.`dependencia_laboral` LEFT JOIN `aprobaciones` as aprobaciones1 ON `aprobaciones1`.`id_aprobaciones`=`ventas`.`aprobaciones` LEFT JOIN `provincia` as provincia1 ON `provincia1`.`id_pr`=`ventas`.`provincia` LEFT JOIN `canton` as canton1 ON `canton1`.`id_c`=`ventas`.`canton` LEFT JOIN `parroquia` as parroquia1 ON `parroquia1`.`id_pa`=`ventas`.`parroquia` LEFT JOIN `tipo_venta` as tipo_venta1 ON `tipo_venta1`.`id_tipo_venta`=`ventas`.`tipo_venta` ",
			'producto_venta' => "`producto_venta` LEFT JOIN `ventas` as ventas1 ON `ventas1`.`numero_contrato`=`producto_venta`.`id_venta` LEFT JOIN `producto` as producto1 ON `producto1`.`id_p`=`producto_venta`.`id_producto` ",
			'nota_pedido' => "`nota_pedido` LEFT JOIN `vendedor` as vendedor1 ON `vendedor1`.`id_vendedor`=`nota_pedido`.`vendedor` ",
			'producto_nota_pedido' => "`producto_nota_pedido` LEFT JOIN `nota_pedido` as nota_pedido1 ON `nota_pedido1`.`numero_guia`=`producto_nota_pedido`.`id_nota_pedido` LEFT JOIN `producto` as producto1 ON `producto1`.`id_p`=`producto_nota_pedido`.`id_producto` ",
			'nota_devolucion' => "`nota_devolucion` LEFT JOIN `vendedor` as vendedor1 ON `vendedor1`.`id_vendedor`=`nota_devolucion`.`vendedor` ",
			'producto_nota_devolucion' => "`producto_nota_devolucion` LEFT JOIN `nota_devolucion` as nota_devolucion1 ON `nota_devolucion1`.`numero_guia`=`producto_nota_devolucion`.`id_nota_devolucion` LEFT JOIN `producto` as producto1 ON `producto1`.`id_p`=`producto_nota_devolucion`.`id_producto` ",
			'stock_general' => "`stock_general` LEFT JOIN `vendedor` as vendedor1 ON `vendedor1`.`id_vendedor`=`stock_general`.`vendedor` LEFT JOIN `producto` as producto1 ON `producto1`.`id_p`=`stock_general`.`id_producto` LEFT JOIN `producto` as producto2 ON `producto2`.`id_p`=`stock_general`.`codigo_producto` LEFT JOIN `producto` as producto3 ON `producto3`.`id_p`=`stock_general`.`tipo_producto` LEFT JOIN `tipo_producto` as tipo_producto1 ON `tipo_producto1`.`id_tp`=`producto3`.`tipo_producto_p` LEFT JOIN `producto` as producto4 ON `producto4`.`id_p`=`stock_general`.`nombre_producto` ",
			'control_stock' => "`control_stock` LEFT JOIN `tipo_movimiento` as tipo_movimiento1 ON `tipo_movimiento1`.`id`=`control_stock`.`tipo_movimiento` LEFT JOIN `vendedor` as vendedor1 ON `vendedor1`.`id_vendedor`=`control_stock`.`vendedor` LEFT JOIN `tipo_producto` as tipo_producto1 ON `tipo_producto1`.`id_tp`=`control_stock`.`categoria_producto` LEFT JOIN `producto` as producto1 ON `producto1`.`id_p`=`control_stock`.`producto` ",
			'guia_remision' => "`guia_remision` LEFT JOIN `canton` as canton1 ON `canton1`.`id_c`=`guia_remision`.`punto_partida` LEFT JOIN `provincia` as provincia1 ON `provincia1`.`id_pr`=`guia_remision`.`punto_llegada` LEFT JOIN `canton` as canton2 ON `canton2`.`id_c`=`guia_remision`.`punto_llegada_canton` LEFT JOIN `vendedor` as vendedor1 ON `vendedor1`.`id_vendedor`=`guia_remision`.`vendedor` LEFT JOIN `nota_pedido` as nota_pedido1 ON `nota_pedido1`.`numero_guia`=`guia_remision`.`nota_pedido` ",
			'producto_guia_remision' => "`producto_guia_remision` LEFT JOIN `producto` as producto1 ON `producto1`.`id_p`=`producto_guia_remision`.`id_producto` LEFT JOIN `vendedor` as vendedor1 ON `vendedor1`.`id_vendedor`=`producto_guia_remision`.`vendedor` ",
			'tipo_venta' => "`tipo_venta` ",
			'inventario_bodega_central' => "`inventario_bodega_central` LEFT JOIN `producto` as producto1 ON `producto1`.`id_p`=`inventario_bodega_central`.`codigo_producto` ",
			'compras' => "`compras` LEFT JOIN `ruc` as ruc1 ON `ruc1`.`id_ruc`=`compras`.`ruc` ",
			'compra_producto' => "`compra_producto` LEFT JOIN `compras` as compras1 ON `compras1`.`id_compras`=`compra_producto`.`compra_compra_producto` LEFT JOIN `producto` as producto1 ON `producto1`.`id_p`=`compra_producto`.`producto_compra_producto` ",
			'ruc' => "`ruc` ",
		];

		$pkey = [
			'bitacora' => 'id_bitacora',
			'provincia' => 'id_pr',
			'canton' => 'id_c',
			'parroquia' => 'id_pa',
			'dependencia_laboral' => 'id_dependencia_laboral',
			'tipo_producto' => 'id_tp',
			'vendedor' => 'id_vendedor',
			'tipo_movimiento' => 'id',
			'aprobaciones' => 'id_aprobaciones',
			'cliente' => 'id_cliente',
			'producto' => 'id_p',
			'ventas' => 'numero_contrato',
			'producto_venta' => 'id',
			'nota_pedido' => 'numero_guia',
			'producto_nota_pedido' => 'id',
			'nota_devolucion' => 'numero_guia',
			'producto_nota_devolucion' => 'id',
			'stock_general' => 'id',
			'control_stock' => 'id',
			'guia_remision' => 'numero_guia_remision',
			'producto_guia_remision' => 'id',
			'tipo_venta' => 'id_tipo_venta',
			'inventario_bodega_central' => 'id_inventario_bodega_central',
			'compras' => 'id_compras',
			'compra_producto' => 'id_compra_producto',
			'ruc' => 'id_ruc',
		];

		if(!isset($sql_from[$table_name])) return false;

		$from = ($skip_joins ? "`{$table_name}`" : $sql_from[$table_name]);

		if($skip_permissions) return $from . ' WHERE 1=1';

		// mm: build the query based on current member's permissions
		// allowing lower permissions if $lower_permissions set to 'user' or 'group'
		$perm = getTablePermissions($table_name);
		if($perm['view'] == 1 || ($perm['view'] > 1 && $lower_permissions == 'user')) { // view owner only
			$from .= ", `membership_userrecords` WHERE `{$table_name}`.`{$pkey[$table_name]}`=`membership_userrecords`.`pkValue` AND `membership_userrecords`.`tableName`='{$table_name}' AND LCASE(`membership_userrecords`.`memberID`)='" . getLoggedMemberID() . "'";
		} elseif($perm['view'] == 2 || ($perm['view'] > 2 && $lower_permissions == 'group')) { // view group only
			$from .= ", `membership_userrecords` WHERE `{$table_name}`.`{$pkey[$table_name]}`=`membership_userrecords`.`pkValue` AND `membership_userrecords`.`tableName`='{$table_name}' AND `membership_userrecords`.`groupID`='" . getLoggedGroupID() . "'";
		} elseif($perm['view'] == 3) { // view all
			$from .= ' WHERE 1=1';
		} else { // view none
			return false;
		}

		return $from;
	}

	#########################################################

	function get_joined_record($table, $id, $skip_permissions = false) {
		$sql_fields = get_sql_fields($table);
		$sql_from = get_sql_from($table, $skip_permissions);

		if(!$sql_fields || !$sql_from) return false;

		$pk = getPKFieldName($table);
		if(!$pk) return false;

		$safe_id = makeSafe($id, false);
		$sql = "SELECT {$sql_fields} FROM {$sql_from} AND `{$table}`.`{$pk}`='{$safe_id}'";
		$eo['silentErrors'] = true;
		$res = sql($sql, $eo);
		if($row = db_fetch_assoc($res)) return $row;

		return false;
	}

	#########################################################

	function get_defaults($table) {
		/* array of tables and their fields, with default values (or empty), excluding automatic values */
		$defaults = [
			'bitacora' => [
				'id_bitacora' => '',
				'texto_bitacora' => '',
			],
			'provincia' => [
				'id_pr' => '',
				'fecha_creacion_pr' => '',
				'nombre_pr' => '',
				'estado' => '1',
			],
			'canton' => [
				'id_c' => '',
				'fecha_creacion_c' => '',
				'codigo_provincia_c' => '',
				'nombre_c' => '',
				'estado' => '1',
			],
			'parroquia' => [
				'id_pa' => '',
				'fecha_creacion_pa' => '',
				'codigo_canton_pa' => '',
				'nombre_pa' => '',
				'estado' => '1',
			],
			'dependencia_laboral' => [
				'id_dependencia_laboral' => '',
				'nombre_dependencia_laboral' => '',
			],
			'tipo_producto' => [
				'id_tp' => '',
				'fecha_creacion_tp' => '',
				'nombre_tp' => '',
				'estado' => '1',
			],
			'vendedor' => [
				'id_vendedor' => '',
				'user' => '',
				'nombre' => '',
				'apellido' => '',
				'email_personal' => '',
				'email_trabajo' => '',
				'cedula' => '',
				'celular' => '',
				'telefono' => '',
				'aprobado' => '',
				'fecha_registro' => '',
				'codigo_lead' => '',
			],
			'tipo_movimiento' => [
				'id' => '',
				'nombre' => '',
			],
			'aprobaciones' => [
				'id_aprobaciones' => '',
				'nombre_aprobaciones' => '',
			],
			'cliente' => [
				'id_cliente' => '',
				'cedula_cliente' => '',
				'nombre_cliente' => '',
				'apellido_cliente' => '',
				'correo_cliente' => '',
				'celular_cliente' => '',
			],
			'producto' => [
				'id_p' => '',
				'fecha_creacion_p' => '',
				'codigo_p' => '',
				'nombre_p' => '',
				'tipo_producto_p' => '',
				'estado' => '1',
				'pvp_producto' => '',
				'manejo_serie' => '',
			],
			'ventas' => [
				'id' => '',
				'vendedor' => '',
				'fecha_ingreso' => '',
				'usuario_creador' => '',
				'fecha_actualizacion' => '',
				'usuario_actualizo' => '',
				'fecha' => '1',
				'numero_contrato' => '',
				'cedula' => '',
				'nombre' => '',
				'mes_comision_venta' => '',
				'valor_contrato' => '',
				'pvp' => '',
				'a_medias' => '',
				'dependencia_laboral' => '',
				'aprobaciones' => '',
				'cobro_particular' => '',
				'entrega_cedula' => '',
				'cuotas' => '',
				'valor_cuota' => '',
				'cantidad_productos' => '',
				'provincia' => '',
				'canton' => '',
				'parroquia' => '',
				'categorias_descripcion' => '',
				'tipo_venta' => '',
			],
			'producto_venta' => [
				'id' => '',
				'id_venta' => '',
				'id_producto' => '',
				'cantidad' => '',
			],
			'nota_pedido' => [
				'id' => '',
				'fecha_ingreso' => '',
				'numero_guia' => '',
				'vendedor' => '',
				'fecha' => '1',
				'cantidad_productos' => '',
				'observaciones' => '',
				'usuario_creador' => '',
			],
			'producto_nota_pedido' => [
				'id' => '',
				'id_nota_pedido' => '',
				'id_producto' => '',
				'cantidad' => '',
			],
			'nota_devolucion' => [
				'id' => '',
				'fecha_ingreso' => '',
				'numero_guia' => '',
				'vendedor' => '',
				'fecha' => '1',
				'cantidad_productos' => '',
				'observaciones' => '',
				'usuario_creador' => '',
				'archivo_nota_devolucion' => '',
			],
			'producto_nota_devolucion' => [
				'id' => '',
				'id_nota_devolucion' => '',
				'id_producto' => '',
				'cantidad' => '',
				'numero_serie_imei' => '',
			],
			'stock_general' => [
				'id' => '',
				'usuario_creador' => '',
				'usuario_actualizo' => '',
				'fecha_ingreso' => '',
				'fecha_actualizacion' => '',
				'vendedor' => '',
				'ano' => '',
				'mes' => '',
				'id_producto' => '',
				'codigo_producto' => '',
				'tipo_producto' => '',
				'nombre_producto' => '',
				'cantidad' => '',
				'pvp' => '',
			],
			'control_stock' => [
				'id' => '',
				'fecha_creacion' => '',
				'fecha_movimiento' => '',
				'tipo_movimiento' => '',
				'id_transaccion' => '',
				'numero_guia_contrato' => '',
				'vendedor' => '',
				'cliente' => '',
				'categoria_producto' => '',
				'producto' => '',
				'codigo_producto' => '',
				'cantidad' => '',
			],
			'guia_remision' => [
				'numero_guia_remision' => '',
				'fecha_creacion_guia_remision' => '',
				'usuario_creador' => '',
				'fecha_actualizacion' => '',
				'usuario_actualizo' => '',
				'fecha_inicio_traslado' => '1',
				'fecha_terminacion_traslado' => '1',
				'motivo_traslado' => '',
				'punto_partida' => '',
				'destinatario' => '',
				'ruc_ci' => '',
				'punto_llegada' => '',
				'punto_llegada_canton' => '',
				'placa' => '',
				'persona_encarga_nombre' => '',
				'ruc_ci_persona_encargada' => '',
				'vendedor' => '',
				'nota_pedido' => '',
				'archivo_guia_remision' => '',
				'archivo_entrega_mercaderia' => '',
			],
			'producto_guia_remision' => [
				'id' => '',
				'id_guia_remision' => '',
				'id_producto' => '',
				'codigo_producto' => '',
				'cantidad' => '',
				'vendedor' => '',
				'mes' => '',
				'numero_serie_imei' => '',
			],
			'tipo_venta' => [
				'id_tipo_venta' => '',
				'nombre_tipo_venta' => '',
			],
			'inventario_bodega_central' => [
				'id_inventario_bodega_central' => '',
				'codigo_producto' => '',
				'nombre_producto' => '',
				'cantidad_producto' => '',
			],
			'compras' => [
				'id_compras' => '',
				'numero_factura' => '',
				'ruc' => '',
			],
			'compra_producto' => [
				'id_compra_producto' => '',
				'compra_compra_producto' => '',
				'producto_compra_producto' => '',
				'nombre_producto_compra_producto' => '',
			],
			'ruc' => [
				'id_ruc' => '',
				'nombre_ruc' => '',
				'numero_ruc' => '',
			],
		];

		return isset($defaults[$table]) ? $defaults[$table] : [];
	}

	#########################################################

	function logInMember() {
		$redir = 'index.php';
		if($_POST['signIn'] != '') {
			if($_POST['username'] != '' && $_POST['password'] != '') {
				$username = makeSafe(strtolower($_POST['username']));
				$hash = sqlValue("select passMD5 from membership_users where lcase(memberID)='{$username}' and isApproved=1 and isBanned=0");
				$password = $_POST['password'];

				if(password_match($password, $hash)) {
					$_SESSION['memberID'] = $username;
					$_SESSION['memberGroupID'] = sqlValue("SELECT `groupID` FROM `membership_users` WHERE LCASE(`memberID`)='{$username}'");

					if($_POST['rememberMe'] == 1) {
						RememberMe::login($username);
					} else {
						RememberMe::delete();
					}

					// harden user's password hash
					password_harden($username, $password, $hash);

					// hook: login_ok
					if(function_exists('login_ok')) {
						$args = [];
						if(!$redir = login_ok(getMemberInfo(), $args)) {
							$redir = 'index.php';
						}
					}

					redirect($redir);
					exit;
				}
			}

			// hook: login_failed
			if(function_exists('login_failed')) {
				$args = [];
				login_failed([
					'username' => $_POST['username'],
					'password' => $_POST['password'],
					'IP' => $_SERVER['REMOTE_ADDR']
				], $args);
			}

			if(!headers_sent()) header('HTTP/1.0 403 Forbidden');
			redirect("index.php?loginFailed=1");
			exit;
		}

		/* do we have a JWT auth header? */
		jwt_check_login();

		if(!empty($_SESSION['memberID']) && !empty($_SESSION['memberGroupID'])) return;

		/* check if a rememberMe cookie exists and sign in user if so */
		if(RememberMe::check()) {
			$username = makeSafe(strtolower(RememberMe::user()));
			$_SESSION['memberID'] = $username;
			$_SESSION['memberGroupID'] = sqlValue("SELECT `groupID` FROM `membership_users` WHERE LCASE(`memberID`)='{$username}'");
		}
	}

	#########################################################

	function htmlUserBar() {
		global $Translation;
		if(!defined('PREPEND_PATH')) define('PREPEND_PATH', '');

		$mi = getMemberInfo();
		$adminConfig = config('adminConfig');
		$home_page = (basename($_SERVER['PHP_SELF']) == 'index.php');
		ob_start();

		?>
		<nav class="navbar navbar-default navbar-fixed-top hidden-print" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- application title is obtained from the name besides the yellow database icon in AppGini, use underscores for spaces -->
				<a class="navbar-brand" href="<?php echo PREPEND_PATH; ?>index.php"><i class="glyphicon glyphicon-home"></i> sistema final</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav"><?php echo ($home_page ? '' : NavMenus()); ?></ul>

				<?php if(userCanImport()){ ?>
					<ul class="nav navbar-nav">
						<a href="<?php echo PREPEND_PATH; ?>import-csv.php" class="btn btn-default navbar-btn hidden-xs btn-import-csv" title="<?php echo html_attr($Translation['import csv file']); ?>"><i class="glyphicon glyphicon-th"></i> <?php echo $Translation['import CSV']; ?></a>
						<a href="<?php echo PREPEND_PATH; ?>import-csv.php" class="btn btn-default navbar-btn visible-xs btn-lg btn-import-csv" title="<?php echo html_attr($Translation['import csv file']); ?>"><i class="glyphicon glyphicon-th"></i> <?php echo $Translation['import CSV']; ?></a>
					</ul>
				<?php } ?>

				<?php if(getLoggedAdmin() !== false) { ?>
					<ul class="nav navbar-nav">
						<a href="<?php echo PREPEND_PATH; ?>admin/pageHome.php" class="btn btn-danger navbar-btn hidden-xs" title="<?php echo html_attr($Translation['admin area']); ?>"><i class="glyphicon glyphicon-cog"></i> <?php echo $Translation['admin area']; ?></a>
						<a href="<?php echo PREPEND_PATH; ?>admin/pageHome.php" class="btn btn-danger navbar-btn visible-xs btn-lg" title="<?php echo html_attr($Translation['admin area']); ?>"><i class="glyphicon glyphicon-cog"></i> <?php echo $Translation['admin area']; ?></a>
					</ul>
				<?php } ?>

				<?php if(!$_GET['signIn'] && !$_GET['loginFailed']) { ?>
					<?php if(!$mi['username'] || $mi['username'] == $adminConfig['anonymousMember']) { ?>
						<p class="navbar-text navbar-right">&nbsp;</p>
						<a href="<?php echo PREPEND_PATH; ?>index.php?signIn=1" class="btn btn-success navbar-btn navbar-right"><?php echo $Translation['sign in']; ?></a>
						<p class="navbar-text navbar-right">
							<?php echo $Translation['not signed in']; ?>
						</p>
					<?php } else { ?>
						<ul class="nav navbar-nav navbar-right hidden-xs" style="min-width: 330px;">
							<a class="btn navbar-btn btn-default" href="<?php echo PREPEND_PATH; ?>index.php?signOut=1"><i class="glyphicon glyphicon-log-out"></i> <?php echo $Translation['sign out']; ?></a>

							<p class="navbar-text signed-in-as">
								<?php echo $Translation['signed as']; ?> <strong><a href="<?php echo PREPEND_PATH; ?>membership_profile.php" class="navbar-link username"><?php echo $mi['username']; ?></a></strong>
							</p>
						</ul>
						<ul class="nav navbar-nav visible-xs">
							<a class="btn navbar-btn btn-default btn-lg visible-xs" href="<?php echo PREPEND_PATH; ?>index.php?signOut=1"><i class="glyphicon glyphicon-log-out"></i> <?php echo $Translation['sign out']; ?></a>
							<p class="navbar-text text-center signed-in-as">
								<?php echo $Translation['signed as']; ?> <strong><a href="<?php echo PREPEND_PATH; ?>membership_profile.php" class="navbar-link username"><?php echo $mi['username']; ?></a></strong>
							</p>
						</ul>
						<script>
							/* periodically check if user is still signed in */
							setInterval(function() {
								$j.ajax({
									url: '<?php echo PREPEND_PATH; ?>ajax_check_login.php',
									success: function(username) {
										if(!username.length) window.location = '<?php echo PREPEND_PATH; ?>index.php?signIn=1';
									}
								});
							}, 60000);
						</script>
					<?php } ?>
				<?php } ?>

				<p class="navbar-text navbar-right help-shortcuts-launcher-container hidden-xs">
					<img
						class="help-shortcuts-launcher" 
						src="<?php echo PREPEND_PATH; ?>resources/images/keyboard.png" 
						title="<?php echo html_attr($Translation['keyboard shortcuts']); ?>">
				</p>
			</div>
		</nav>
		<?php

		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	#########################################################

	function showNotifications($msg = '', $class = '', $fadeout = true) {
		global $Translation;
		if($error_message = strip_tags($_REQUEST['error_message']))
			$error_message = '<div class="text-bold">' . $error_message . '</div>';

		if(!$msg) { // if no msg, use url to detect message to display
			if($_REQUEST['record-added-ok'] != '') {
				$msg = $Translation['new record saved'];
				$class = 'alert-success';
			} elseif($_REQUEST['record-added-error'] != '') {
				$msg = $Translation['Couldn\'t save the new record'] . $error_message;
				$class = 'alert-danger';
				$fadeout = false;
			} elseif($_REQUEST['record-updated-ok'] != '') {
				$msg = $Translation['record updated'];
				$class = 'alert-success';
			} elseif($_REQUEST['record-updated-error'] != '') {
				$msg = $Translation['Couldn\'t save changes to the record'] . $error_message;
				$class = 'alert-danger';
				$fadeout = false;
			} elseif($_REQUEST['record-deleted-ok'] != '') {
				$msg = $Translation['The record has been deleted successfully'];
				$class = 'alert-success';
			} elseif($_REQUEST['record-deleted-error'] != '') {
				$msg = $Translation['Couldn\'t delete this record'] . $error_message;
				$class = 'alert-danger';
				$fadeout = false;
			} else {
				return '';
			}
		}
		$id = 'notification-' . rand();

		ob_start();
		// notification template
		?>
		<div id="%%ID%%" class="alert alert-dismissable %%CLASS%%" style="opacity: 1; padding-top: 6px; padding-bottom: 6px; animation: fadeIn 1.5s ease-out;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			%%MSG%%
		</div>
		<script>
			$j(function() {
				var autoDismiss = <?php echo $fadeout ? 'true' : 'false'; ?>,
					embedded = !$j('nav').length,
					messageDelay = 10, fadeDelay = 1.5;

				if(!autoDismiss) {
					if(embedded)
						$j('#%%ID%%').before('<div style="height: 2rem;"></div>');
					else
						$j('#%%ID%%').css({ margin: '0 0 1rem' });

					return;
				}

				// below code runs only in case of autoDismiss

				if(embedded)
					$j('#%%ID%%').css({ margin: '1rem 0 -1rem' });
				else
					$j('#%%ID%%').css({ margin: '-15px 0 -20px' });

				setTimeout(function() {
					$j('#%%ID%%').css({    animation: 'fadeOut ' + fadeDelay + 's ease-out' });
				}, messageDelay * 1000);

				setTimeout(function() {
					$j('#%%ID%%').css({    visibility: 'hidden' });
				}, (messageDelay + fadeDelay) * 1000);
			})
		</script>
		<style>
			@keyframes fadeIn {
				0%   { opacity: 0; }
				100% { opacity: 1; }
			}
			@keyframes fadeOut {
				0%   { opacity: 1; }
				100% { opacity: 0; }
			}
		</style>

		<?php
		$out = ob_get_clean();

		$out = str_replace('%%ID%%', $id, $out);
		$out = str_replace('%%MSG%%', $msg, $out);
		$out = str_replace('%%CLASS%%', $class, $out);

		return $out;
	}

	#########################################################

	function parseMySQLDate($date, $altDate) {
		// is $date valid?
		if(preg_match("/^\d{4}-\d{1,2}-\d{1,2}$/", trim($date))) {
			return trim($date);
		}

		if($date != '--' && preg_match("/^\d{4}-\d{1,2}-\d{1,2}$/", trim($altDate))) {
			return trim($altDate);
		}

		if($date != '--' && $altDate && intval($altDate)==$altDate) {
			return @date('Y-m-d', @time() + ($altDate >= 1 ? $altDate - 1 : $altDate) * 86400);
		}

		return '';
	}

	#########################################################

	function parseCode($code, $isInsert = true, $rawData = false) {
		if($isInsert) {
			$arrCodes = [
				'<%%creatorusername%%>' => $_SESSION['memberID'],
				'<%%creatorgroupid%%>' => $_SESSION['memberGroupID'],
				'<%%creatorip%%>' => $_SERVER['REMOTE_ADDR'],
				'<%%creatorgroup%%>' => sqlValue("SELECT `name` FROM `membership_groups` WHERE `groupID`='{$_SESSION['memberGroupID']}'"),

				'<%%creationdate%%>' => ($rawData ? @date('Y-m-d') : @date('j/n/Y')),
				'<%%creationtime%%>' => ($rawData ? @date('H:i:s') : @date('h:i:s a')),
				'<%%creationdatetime%%>' => ($rawData ? @date('Y-m-d H:i:s') : @date('j/n/Y h:i:s a')),
				'<%%creationtimestamp%%>' => ($rawData ? @date('Y-m-d H:i:s') : @time())
			];
		} else {
			$arrCodes = [
				'<%%editorusername%%>' => $_SESSION['memberID'],
				'<%%editorgroupid%%>' => $_SESSION['memberGroupID'],
				'<%%editorip%%>' => $_SERVER['REMOTE_ADDR'],
				'<%%editorgroup%%>' => sqlValue("SELECT `name` FROM `membership_groups` WHERE `groupID`='{$_SESSION['memberGroupID']}'"),

				'<%%editingdate%%>' => ($rawData ? @date('Y-m-d') : @date('j/n/Y')),
				'<%%editingtime%%>' => ($rawData ? @date('H:i:s') : @date('h:i:s a')),
				'<%%editingdatetime%%>' => ($rawData ? @date('Y-m-d H:i:s') : @date('j/n/Y h:i:s a')),
				'<%%editingtimestamp%%>' => ($rawData ? @date('Y-m-d H:i:s') : @time())
			];
		}

		$pc = str_ireplace(array_keys($arrCodes), array_values($arrCodes), $code);

		return $pc;
	}

	#########################################################

	function addFilter($index, $filterAnd, $filterField, $filterOperator, $filterValue) {
		// validate input
		if($index < 1 || $index > 80 || !is_int($index)) return false;
		if($filterAnd != 'or')   $filterAnd = 'and';
		$filterField = intval($filterField);

		/* backward compatibility */
		if(in_array($filterOperator, $GLOBALS['filter_operators'])) {
			$filterOperator = array_search($filterOperator, $GLOBALS['filter_operators']);
		}

		if(!in_array($filterOperator, array_keys($GLOBALS['filter_operators']))) {
			$filterOperator = 'like';
		}

		if(!$filterField) {
			$filterOperator = '';
			$filterValue = '';
		}

		$_REQUEST['FilterAnd'][$index] = $filterAnd;
		$_REQUEST['FilterField'][$index] = $filterField;
		$_REQUEST['FilterOperator'][$index] = $filterOperator;
		$_REQUEST['FilterValue'][$index] = $filterValue;

		return true;
	}

	#########################################################

	function clearFilters() {
		for($i=1; $i<=80; $i++) {
			addFilter($i, '', 0, '', '');
		}
	}

	#########################################################

	if(!function_exists('str_ireplace')) {
		function str_ireplace($search, $replace, $subject) {
			$ret=$subject;
			if(is_array($search)) {
				for($i=0; $i<count($search); $i++) {
					$ret=str_ireplace($search[$i], $replace[$i], $ret);
				}
			} else {
				$ret=preg_replace('/'.preg_quote($search, '/').'/i', $replace, $ret);
			}

			return $ret;
		} 
	} 

	#########################################################

	/**
	* Loads a given view from the templates folder, passing the given data to it
	* @param $view the name of a php file (without extension) to be loaded from the 'templates' folder
	* @param $the_data_to_pass_to_the_view (optional) associative array containing the data to pass to the view
	* @return the output of the parsed view as a string
	*/
	function loadView($view, $the_data_to_pass_to_the_view=false) {
		global $Translation;

		$view = dirname(__FILE__)."/templates/$view.php";
		if(!is_file($view)) return false;

		if(is_array($the_data_to_pass_to_the_view)) {
			foreach($the_data_to_pass_to_the_view as $k => $v)
				$$k = $v;
		}
		unset($the_data_to_pass_to_the_view, $k, $v);

		ob_start();
		@include($view);
		$out=ob_get_contents();
		ob_end_clean();

		return $out;
	}

	#########################################################

	/**
	* Loads a table template from the templates folder, passing the given data to it
	* @param $table_name the name of the table whose template is to be loaded from the 'templates' folder
	* @param $the_data_to_pass_to_the_table associative array containing the data to pass to the table template
	* @return the output of the parsed table template as a string
	*/
	function loadTable($table_name, $the_data_to_pass_to_the_table = []) {
		$dont_load_header = $the_data_to_pass_to_the_table['dont_load_header'];
		$dont_load_footer = $the_data_to_pass_to_the_table['dont_load_footer'];

		$header = $table = $footer = '';

		if(!$dont_load_header) {
			// try to load tablename-header
			if(!($header = loadView("{$table_name}-header", $the_data_to_pass_to_the_table))) {
				$header = loadView('table-common-header', $the_data_to_pass_to_the_table);
			}
		}

		$table = loadView($table_name, $the_data_to_pass_to_the_table);

		if(!$dont_load_footer) {
			// try to load tablename-footer
			if(!($footer = loadView("{$table_name}-footer", $the_data_to_pass_to_the_table))) {
				$footer = loadView('table-common-footer', $the_data_to_pass_to_the_table);
			}
		}

		return "{$header}{$table}{$footer}";
	}

	#########################################################

	function filterDropdownBy($filterable, $filterers, $parentFilterers, $parentPKField, $parentCaption, $parentTable, &$filterableCombo) {
		$filterersArray = explode(',', $filterers);
		$parentFilterersArray = explode(',', $parentFilterers);
		$parentFiltererList = '`' . implode('`, `', $parentFilterersArray) . '`';
		$res=sql("SELECT `$parentPKField`, $parentCaption, $parentFiltererList FROM `$parentTable` ORDER BY 2", $eo);
		$filterableData = [];
		while($row=db_fetch_row($res)) {
			$filterableData[$row[0]] = $row[1];
			$filtererIndex = 0;
			foreach($filterersArray as $filterer) {
				$filterableDataByFilterer[$filterer][$row[$filtererIndex + 2]][$row[0]] = $row[1];
				$filtererIndex++;
			}
			$row[0] = addslashes($row[0]);
			$row[1] = addslashes($row[1]);
			$jsonFilterableData .= "\"{$row[0]}\":\"{$row[1]}\",";
		}
		$jsonFilterableData .= '}';
		$jsonFilterableData = '{'.str_replace(',}', '}', $jsonFilterableData);     
		$filterJS = "\nvar {$filterable}_data = $jsonFilterableData;";

		foreach($filterersArray as $filterer) {
			if(is_array($filterableDataByFilterer[$filterer])) foreach($filterableDataByFilterer[$filterer] as $filtererItem => $filterableItem) {
				$jsonFilterableDataByFilterer[$filterer] .= '"'.addslashes($filtererItem).'":{';
				foreach($filterableItem as $filterableItemID => $filterableItemData) {
					$jsonFilterableDataByFilterer[$filterer] .= '"'.addslashes($filterableItemID).'":"'.addslashes($filterableItemData).'",';
				}
				$jsonFilterableDataByFilterer[$filterer] .= '},';
			}
			$jsonFilterableDataByFilterer[$filterer] .= '}';
			$jsonFilterableDataByFilterer[$filterer] = '{'.str_replace(',}', '}', $jsonFilterableDataByFilterer[$filterer]);

			$filterJS.="\n\n// code for filtering {$filterable} by {$filterer}\n";
			$filterJS.="\nvar {$filterable}_data_by_{$filterer} = {$jsonFilterableDataByFilterer[$filterer]}; ";
			$filterJS.="\nvar selected_{$filterable} = \$j('#{$filterable}').val();";
			$filterJS.="\nvar {$filterable}_change_by_{$filterer} = function() {";
			$filterJS.="\n\t$('{$filterable}').options.length=0;";
			$filterJS.="\n\t$('{$filterable}').options[0] = new Option();";
			$filterJS.="\n\tif(\$j('#{$filterer}').val()) {";
			$filterJS.="\n\t\tfor({$filterable}_item in {$filterable}_data_by_{$filterer}[\$j('#{$filterer}').val()]) {";
			$filterJS.="\n\t\t\t$('{$filterable}').options[$('{$filterable}').options.length] = new Option(";
			$filterJS.="\n\t\t\t\t{$filterable}_data_by_{$filterer}[\$j('#{$filterer}').val()][{$filterable}_item],";
			$filterJS.="\n\t\t\t\t{$filterable}_item,";
			$filterJS.="\n\t\t\t\t({$filterable}_item == selected_{$filterable} ? true : false),";
			$filterJS.="\n\t\t\t\t({$filterable}_item == selected_{$filterable} ? true : false)";
			$filterJS.="\n\t\t\t);";
			$filterJS.="\n\t\t}";
			$filterJS.="\n\t} else {";
			$filterJS.="\n\t\tfor({$filterable}_item in {$filterable}_data) {";
			$filterJS.="\n\t\t\t$('{$filterable}').options[$('{$filterable}').options.length] = new Option(";
			$filterJS.="\n\t\t\t\t{$filterable}_data[{$filterable}_item],";
			$filterJS.="\n\t\t\t\t{$filterable}_item,";
			$filterJS.="\n\t\t\t\t({$filterable}_item == selected_{$filterable} ? true : false),";
			$filterJS.="\n\t\t\t\t({$filterable}_item == selected_{$filterable} ? true : false)";
			$filterJS.="\n\t\t\t);";
			$filterJS.="\n\t\t}";
			$filterJS.="\n\t\tif(selected_{$filterable} && selected_{$filterable} == \$j('#{$filterable}').val()) {";
			$filterJS.="\n\t\t\tfor({$filterer}_item in {$filterable}_data_by_{$filterer}) {";
			$filterJS.="\n\t\t\t\tfor({$filterable}_item in {$filterable}_data_by_{$filterer}[{$filterer}_item]) {";
			$filterJS.="\n\t\t\t\t\tif({$filterable}_item == selected_{$filterable}) {";
			$filterJS.="\n\t\t\t\t\t\t$('{$filterer}').value = {$filterer}_item;";
			$filterJS.="\n\t\t\t\t\t\tbreak;";
			$filterJS.="\n\t\t\t\t\t}";
			$filterJS.="\n\t\t\t\t}";
			$filterJS.="\n\t\t\t\tif({$filterable}_item == selected_{$filterable}) break;";
			$filterJS.="\n\t\t\t}";
			$filterJS.="\n\t\t}";
			$filterJS.="\n\t}";
			$filterJS.="\n\t$('{$filterable}').highlight();";
			$filterJS.="\n};";
			$filterJS.="\n$('{$filterer}').observe('change', function() { window.setTimeout({$filterable}_change_by_{$filterer}, 25); });";
			$filterJS.="\n";
		}

		$filterableCombo = new Combo;
		$filterableCombo->ListType = 0;
		$filterableCombo->ListItem = array_slice(array_values($filterableData), 0, 10);
		$filterableCombo->ListData = array_slice(array_keys($filterableData), 0, 10);
		$filterableCombo->SelectName = $filterable;
		$filterableCombo->AllowNull = true;

		return $filterJS;
	}

	#########################################################
	function br2nl($text) {
		return  preg_replace('/\<br(\s*)?\/?\>/i', "\n", $text);
	}

	#########################################################

	if(!function_exists('htmlspecialchars_decode')) {
		function htmlspecialchars_decode($string, $quote_style = ENT_COMPAT) {
			return strtr($string, array_flip(get_html_translation_table(HTML_SPECIALCHARS, $quote_style)));
		}
	}

	#########################################################

	function entitiesToUTF8($input) {
		return preg_replace_callback('/(&#[0-9]+;)/', '_toUTF8', $input);
	}

	function _toUTF8($m) {
		if(function_exists('mb_convert_encoding')) {
			return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES");
		} else {
			return $m[1];
		}
	}

	#########################################################

	function func_get_args_byref() {
		if(!function_exists('debug_backtrace')) return false;

		$trace = debug_backtrace();
		return $trace[1]['args'];
	}

	#########################################################

	function permissions_sql($table, $level = 'all') {
		if(!in_array($level, ['user', 'group'])) { $level = 'all'; }
		$perm = getTablePermissions($table);
		$from = '';
		$where = '';
		$pk = getPKFieldName($table);

		if($perm['view'] == 1 || ($perm['view'] > 1 && $level == 'user')) { // view owner only
			$from = 'membership_userrecords';
			$where = "(`$table`.`$pk`=membership_userrecords.pkValue and membership_userrecords.tableName='$table' and lcase(membership_userrecords.memberID)='" . getLoggedMemberID() . "')";
		} elseif($perm['view'] == 2 || ($perm['view'] > 2 && $level == 'group')) { // view group only
			$from = 'membership_userrecords';
			$where = "(`$table`.`$pk`=membership_userrecords.pkValue and membership_userrecords.tableName='$table' and membership_userrecords.groupID='" . getLoggedGroupID() . "')";
		} elseif($perm['view'] == 3) { // view all
			// no further action
		} elseif($perm['view'] == 0) { // view none
			return false;
		}

		return ['where' => $where, 'from' => $from, 0 => $where, 1 => $from];
	}

	#########################################################

	function error_message($msg, $back_url = '', $full_page = true) {
		$curr_dir = dirname(__FILE__);
		global $Translation;

		ob_start();

		if($full_page) include($curr_dir . '/header.php');

		echo '<div class="panel panel-danger">';
			echo '<div class="panel-heading"><h3 class="panel-title">' . $Translation['error:'] . '</h3></div>';
			echo '<div class="panel-body"><p class="text-danger">' . $msg . '</p>';
			if($back_url !== false) { // explicitly passing false suppresses the back link completely
				echo '<div class="text-center">';
				if($back_url) {
					echo '<a href="' . $back_url . '" class="btn btn-danger btn-lg vspacer-lg"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['< back'] . '</a>';
				} else {
					echo '<a href="#" class="btn btn-danger btn-lg vspacer-lg" onclick="history.go(-1); return false;"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['< back'] . '</a>';
				}
				echo '</div>';
			}
			echo '</div>';
		echo '</div>';

		if($full_page) include($curr_dir . '/footer.php');

		return ob_get_clean();
	}

	#########################################################

	function toMySQLDate($formattedDate, $sep = datalist_date_separator, $ord = datalist_date_format) {
		// extract date elements
		$de=explode($sep, $formattedDate);
		$mySQLDate=intval($de[strpos($ord, 'Y')]).'-'.intval($de[strpos($ord, 'm')]).'-'.intval($de[strpos($ord, 'd')]);
		return $mySQLDate;
	}

	#########################################################

	function reIndex(&$arr) {
		$i=1;
		foreach($arr as $n=>$v) {
			$arr2[$i]=$n;
			$i++;
		}
		return $arr2;
	}

	#########################################################

	function get_embed($provider, $url, $max_width = '', $max_height = '', $retrieve = 'html') {
		global $Translation;
		if(!$url) return '';

		$providers = [
			'youtube' => ['oembed' => 'https://www.youtube.com/oembed?'],
			'googlemap' => ['oembed' => '', 'regex' => '/^http.*\.google\..*maps/i'],
		];

		if(!isset($providers[$provider])) {
			return '<div class="text-danger">' . $Translation['invalid provider'] . '</div>';
		}

		if(isset($providers[$provider]['regex']) && !preg_match($providers[$provider]['regex'], $url)) {
			return '<div class="text-danger">' . $Translation['invalid url'] . '</div>';
		}

		if($providers[$provider]['oembed']) {
			$oembed = $providers[$provider]['oembed'] . 'url=' . urlencode($url) . "&amp;maxwidth={$max_width}&amp;maxheight={$max_height}&amp;format=json";
			$data_json = request_cache($oembed);

			$data = json_decode($data_json, true);
			if($data === null) {
				/* an error was returned rather than a json string */
				if($retrieve == 'html') return "<div class=\"text-danger\">{$data_json}\n<!-- {$oembed} --></div>";
				return '';
			}

			return (isset($data[$retrieve]) ? $data[$retrieve] : $data['html']);
		}

		/* special cases (where there is no oEmbed provider) */
		if($provider == 'googlemap') return get_embed_googlemap($url, $max_width, $max_height, $retrieve);

		return '<div class="text-danger">Invalid provider!</div>';
	}

	#########################################################

	function get_embed_googlemap($url, $max_width = '', $max_height = '', $retrieve = 'html') {
		global $Translation;
		$url_parts = parse_url($url);
		$coords_regex = '/-?\d+(\.\d+)?[,+]-?\d+(\.\d+)?(,\d{1,2}z)?/'; /* https://stackoverflow.com/questions/2660201 */

		if(preg_match($coords_regex, $url_parts['path'] . '?' . $url_parts['query'], $m)) {
			list($lat, $long, $zoom) = explode(',', $m[0]);
			$zoom = intval($zoom);
			if(!$zoom) $zoom = 10; /* default zoom */
			if(!$max_height) $max_height = 360;
			if(!$max_width) $max_width = 480;

			$api_key = config('adminConfig')['googleAPIKey'];
			$embed_url = "https://www.google.com/maps/embed/v1/view?key={$api_key}&amp;center={$lat},{$long}&amp;zoom={$zoom}&amp;maptype=roadmap";
			$thumbnail_url = "https://maps.googleapis.com/maps/api/staticmap?key={$api_key}&amp;center={$lat},{$long}&amp;zoom={$zoom}&amp;maptype=roadmap&amp;size={$max_width}x{$max_height}";

			if($retrieve == 'html') {
				return "<iframe width=\"{$max_width}\" height=\"{$max_height}\" frameborder=\"0\" style=\"border:0\" src=\"{$embed_url}\"></iframe>";
			} else {
				return $thumbnail_url;
			}
		} else {
			return '<div class="text-danger">' . $Translation['cant retrieve coordinates from url'] . '</div>';
		}
	}

	#########################################################

	function request_cache($request, $force_fetch = false) {
		$max_cache_lifetime = 7 * 86400; /* max cache lifetime in seconds before refreshing from source */

		/* membership_cache table exists? if not, create it */
		static $cache_table_exists = false;
		if(!$cache_table_exists && !$force_fetch) {
			$te = sqlValue("show tables like 'membership_cache'");
			if(!$te) {
				if(!sql("CREATE TABLE `membership_cache` (`request` VARCHAR(100) NOT NULL, `request_ts` INT, `response` TEXT NOT NULL, PRIMARY KEY (`request`))", $eo)) {
					/* table can't be created, so force fetching request */
					return request_cache($request, true);
				}
			}
			$cache_table_exists = true;
		}

		/* retrieve response from cache if exists */
		if(!$force_fetch) {
			$res = sql("select response, request_ts from membership_cache where request='" . md5($request) . "'", $eo);
			if(!$row = db_fetch_array($res)) return request_cache($request, true);

			$response = $row[0];
			$response_ts = $row[1];
			if($response_ts < time() - $max_cache_lifetime) return request_cache($request, true);
		}

		/* if no response in cache, issue a request */
		if(!$response || $force_fetch) {
			$response = @file_get_contents($request);
			if($response === false) {
				$error = error_get_last();
				$error_message = preg_replace('/.*: (.*)/', '$1', $error['message']);
				return $error_message;
			} elseif($cache_table_exists) {
				/* store response in cache */
				$ts = time();
				sql("replace into membership_cache set request='" . md5($request) . "', request_ts='{$ts}', response='" . makeSafe($response, false) . "'", $eo);
			}
		}

		return $response;
	}

	#########################################################

	function check_record_permission($table, $id, $perm = 'view') {
		if($perm != 'edit' && $perm != 'delete') $perm = 'view';

		$perms = getTablePermissions($table);
		if(!$perms[$perm]) return false;

		$safe_id = makeSafe($id);
		$safe_table = makeSafe($table);

		if($perms[$perm] == 1) { // own records only
			$username = getLoggedMemberID();
			$owner = sqlValue("select memberID from membership_userrecords where tableName='{$safe_table}' and pkValue='{$safe_id}'");
			if($owner == $username) return true;
		} elseif($perms[$perm] == 2) { // group records
			$group_id = getLoggedGroupID();
			$owner_group_id = sqlValue("select groupID from membership_userrecords where tableName='{$safe_table}' and pkValue='{$safe_id}'");
			if($owner_group_id == $group_id) return true;
		} elseif($perms[$perm] == 3) { // all records
			return true;
		}

		return false;
	}

	#########################################################

	function NavMenus($options = []) {
		if(!defined('PREPEND_PATH')) define('PREPEND_PATH', '');
		global $Translation;
		$prepend_path = PREPEND_PATH;

		/* default options */
		if(empty($options)) {
			$options = ['tabs' => 7];
		}

		$table_group_name = array_keys(get_table_groups()); /* 0 => group1, 1 => group2 .. */
		/* if only one group named 'None', set to translation of 'select a table' */
		if((count($table_group_name) == 1 && $table_group_name[0] == 'None') || count($table_group_name) < 1) $table_group_name[0] = $Translation['select a table'];
		$table_group_index = array_flip($table_group_name); /* group1 => 0, group2 => 1 .. */
		$menu = array_fill(0, count($table_group_name), '');

		$t = time();
		$arrTables = getTableList();
		if(is_array($arrTables)) {
			foreach($arrTables as $tn => $tc) {
				/* ---- list of tables where hide link in nav menu is set ---- */
				$tChkHL = array_search($tn, []);

				/* ---- list of tables where filter first is set ---- */
				$tChkFF = array_search($tn, ['stock_general','control_stock']);
				if($tChkFF !== false && $tChkFF !== null) {
					$searchFirst = '&Filter_x=1';
				} else {
					$searchFirst = '';
				}

				/* when no groups defined, $table_group_index['None'] is NULL, so $menu_index is still set to 0 */
				$menu_index = intval($table_group_index[$tc[3]]);
				if(!$tChkHL && $tChkHL !== 0) $menu[$menu_index] .= "<li><a href=\"{$prepend_path}{$tn}_view.php?t={$t}{$searchFirst}\"><img src=\"{$prepend_path}" . ($tc[2] ? $tc[2] : 'blank.gif') . "\" height=\"32\"> {$tc[0]}</a></li>";
			}
		}

		// custom nav links, as defined in "hooks/links-navmenu.php" 
		global $navLinks;
		if(is_array($navLinks)) {
			$memberInfo = getMemberInfo();
			$links_added = [];
			foreach($navLinks as $link) {
				if(!isset($link['url']) || !isset($link['title'])) continue;
				if(getLoggedAdmin() !== false || @in_array($memberInfo['group'], $link['groups']) || @in_array('*', $link['groups'])) {
					$menu_index = intval($link['table_group']);
					if(!$links_added[$menu_index]) $menu[$menu_index] .= '<li class="divider"></li>';

					/* add prepend_path to custom links if they aren't absolute links */
					if(!preg_match('/^(http|\/\/)/i', $link['url'])) $link['url'] = $prepend_path . $link['url'];
					if(!preg_match('/^(http|\/\/)/i', $link['icon']) && $link['icon']) $link['icon'] = $prepend_path . $link['icon'];

					$menu[$menu_index] .= "<li><a href=\"{$link['url']}\"><img src=\"" . ($link['icon'] ? $link['icon'] : "{$prepend_path}blank.gif") . "\" height=\"32\"> {$link['title']}</a></li>";
					$links_added[$menu_index]++;
				}
			}
		}

		$menu_wrapper = '';
		for($i = 0; $i < count($menu); $i++) {
			$menu_wrapper .= <<<EOT
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">{$table_group_name[$i]} <b class="caret"></b></a>
					<ul class="dropdown-menu" role="menu">{$menu[$i]}</ul>
				</li>
EOT;
		}

		return $menu_wrapper;
	}

	#########################################################

	function StyleSheet() {
		if(!defined('PREPEND_PATH')) define('PREPEND_PATH', '');
		$prepend_path = PREPEND_PATH;

		$css_links = <<<EOT

			<link rel="stylesheet" href="{$prepend_path}resources/initializr/css/superhero.css">
			<link rel="stylesheet" href="{$prepend_path}resources/lightbox/css/lightbox.css" media="screen">
			<link rel="stylesheet" href="{$prepend_path}resources/select2/select2.css" media="screen">
			<link rel="stylesheet" href="{$prepend_path}resources/timepicker/bootstrap-timepicker.min.css" media="screen">
			<link rel="stylesheet" href="{$prepend_path}dynamic.css">
EOT;

		return $css_links;
	}

	#########################################################

	function PrepareUploadedFile($FieldName, $MaxSize, $FileTypes = 'jpg|jpeg|gif|png', $NoRename = false, $dir = '') {
		global $Translation;
		$f = $_FILES[$FieldName];
		if($f['error'] == 4 || !$f['name']) return '';

		$dir = getUploadDir($dir);

		/* get php.ini upload_max_filesize in bytes */
		$php_upload_size_limit = trim(ini_get('upload_max_filesize'));
		$last = strtolower($php_upload_size_limit[strlen($php_upload_size_limit) - 1]);
		switch($last) {
			case 'g':
				$php_upload_size_limit *= 1024;
			case 'm':
				$php_upload_size_limit *= 1024;
			case 'k':
				$php_upload_size_limit *= 1024;
		}

		$MaxSize = min($MaxSize, $php_upload_size_limit);

		if($f['size'] > $MaxSize || $f['error']) {
			echo error_message(str_replace(['<MaxSize>', '{MaxSize}'], intval($MaxSize / 1024), $Translation['file too large']));
			exit;
		}
		if(!preg_match('/\.(' . $FileTypes . ')$/i', $f['name'], $ft)) {
			echo error_message(str_replace(['<FileTypes>', '{FileTypes}'], str_replace('|', ', ', $FileTypes), $Translation['invalid file type']));
			exit;
		}

		$name = str_replace(' ', '_', $f['name']);
		if(!$NoRename) $name = substr(md5(microtime() . rand(0, 100000)), -17) . $ft[0];

		if(!file_exists($dir)) @mkdir($dir, 0777);

		if(!@move_uploaded_file($f['tmp_name'], $dir . $name)) {
			echo error_message("Couldn't save the uploaded file. Try chmoding the upload folder '{$dir}' to 777.");
			exit;
		}

		@chmod($dir . $name, 0666);
		return $name;
	}

	#########################################################

	function get_home_links($homeLinks, $default_classes, $tgroup = '') {
		if(!is_array($homeLinks) || !count($homeLinks)) return '';

		$memberInfo = getMemberInfo();

		ob_start();
		foreach($homeLinks as $link) {
			if(!isset($link['url']) || !isset($link['title'])) continue;
			if($tgroup != $link['table_group'] && $tgroup != '*') continue;

			/* fall-back classes if none defined */
			if(!$link['grid_column_classes']) $link['grid_column_classes'] = $default_classes['grid_column'];
			if(!$link['panel_classes']) $link['panel_classes'] = $default_classes['panel'];
			if(!$link['link_classes']) $link['link_classes'] = $default_classes['link'];

			if(getLoggedAdmin() !== false || @in_array($memberInfo['group'], $link['groups']) || @in_array('*', $link['groups'])) {
				?>
				<div class="col-xs-12 <?php echo $link['grid_column_classes']; ?>">
					<div class="panel <?php echo $link['panel_classes']; ?>">
						<div class="panel-body">
							<a class="btn btn-block btn-lg <?php echo $link['link_classes']; ?>" title="<?php echo preg_replace("/&amp;(#[0-9]+|[a-z]+);/i", "&$1;", html_attr(strip_tags($link['description']))); ?>" href="<?php echo $link['url']; ?>"><?php echo ($link['icon'] ? '<img src="' . $link['icon'] . '">' : ''); ?><strong><?php echo $link['title']; ?></strong></a>
							<div class="panel-body-description"><?php echo $link['description']; ?></div>
						</div>
					</div>
				</div>
				<?php
			}
		}

		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	#########################################################

	function quick_search_html($search_term, $label, $separate_dv = true) {
		global $Translation;

		$safe_search = html_attr($search_term);
		$safe_label = html_attr($label);
		$safe_clear_label = html_attr($Translation['Reset Filters']);

		if($separate_dv) {
			$reset_selection = "document.myform.SelectedID.value = '';";
		} else {
			$reset_selection = "document.myform.writeAttribute('novalidate', 'novalidate');";
		}
		$reset_selection .= ' document.myform.NoDV.value=1; return true;';

		$html = <<<EOT
		<div class="input-group" id="quick-search">
			<input type="text" id="SearchString" name="SearchString" value="{$safe_search}" class="form-control" placeholder="{$safe_label}">
			<span class="input-group-btn">
				<button name="Search_x" value="1" id="Search" type="submit" onClick="{$reset_selection}" class="btn btn-default" title="{$safe_label}"><i class="glyphicon glyphicon-search"></i></button>
				<button name="ClearQuickSearch" value="1" id="ClearQuickSearch" type="submit" onClick="\$j('#SearchString').val(''); {$reset_selection}" class="btn btn-default" title="{$safe_clear_label}"><i class="glyphicon glyphicon-remove-circle"></i></button>
			</span>
		</div>
EOT;
		return $html;
	}

	#########################################################

