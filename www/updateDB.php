<?php
	// check this file's MD5 to make sure it wasn't called before
	$prevMD5 = @file_get_contents(dirname(__FILE__) . '/setup.md5');
	$thisMD5 = md5(@file_get_contents(dirname(__FILE__) . '/updateDB.php'));

	// check if setup already run
	if($thisMD5 != $prevMD5) {
		// $silent is set if this file is included from setup.php
		if(!isset($silent)) $silent = true;

		// set up tables
		setupTable(
			'bitacora', " 
			CREATE TABLE IF NOT EXISTS `bitacora` ( 
				`id_bitacora` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_bitacora`),
				`texto_bitacora` TEXT NOT NULL
			) CHARSET utf8",
			$silent
		);

		setupTable(
			'provincia', " 
			CREATE TABLE IF NOT EXISTS `provincia` ( 
				`id_pr` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_pr`),
				`fecha_creacion_pr` DATETIME NULL,
				`nombre_pr` VARCHAR(255) NOT NULL,
				`estado` INT NULL DEFAULT '1'
			) CHARSET utf8",
			$silent
		);

		setupTable(
			'canton', " 
			CREATE TABLE IF NOT EXISTS `canton` ( 
				`id_c` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_c`),
				`fecha_creacion_c` DATETIME NULL,
				`codigo_provincia_c` INT UNSIGNED NOT NULL,
				`nombre_c` VARCHAR(255) NOT NULL,
				`estado` INT NULL DEFAULT '1'
			) CHARSET utf8",
			$silent
		);
		setupIndexes('canton', ['codigo_provincia_c',]);

		setupTable(
			'parroquia', " 
			CREATE TABLE IF NOT EXISTS `parroquia` ( 
				`id_pa` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_pa`),
				`fecha_creacion_pa` DATETIME NULL,
				`codigo_canton_pa` INT UNSIGNED NOT NULL,
				`nombre_pa` VARCHAR(255) NOT NULL,
				`estado` INT NULL DEFAULT '1'
			) CHARSET utf8",
			$silent
		);
		setupIndexes('parroquia', ['codigo_canton_pa',]);

		setupTable(
			'dependencia_laboral', " 
			CREATE TABLE IF NOT EXISTS `dependencia_laboral` ( 
				`id_dependencia_laboral` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_dependencia_laboral`),
				`nombre_dependencia_laboral` VARCHAR(255) NOT NULL
			) CHARSET utf8",
			$silent
		);

		setupTable(
			'tipo_producto', " 
			CREATE TABLE IF NOT EXISTS `tipo_producto` ( 
				`id_tp` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_tp`),
				`fecha_creacion_tp` DATETIME NULL,
				`nombre_tp` VARCHAR(255) NOT NULL,
				`estado` INT NULL DEFAULT '1'
			) CHARSET utf8",
			$silent
		);

		setupTable(
			'vendedor', " 
			CREATE TABLE IF NOT EXISTS `vendedor` ( 
				`id_vendedor` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_vendedor`),
				`user` VARCHAR(40) NULL,
				`nombre` VARCHAR(255) NULL,
				`apellido` VARCHAR(255) NULL,
				`email_personal` VARCHAR(255) NULL,
				`email_trabajo` VARCHAR(255) NULL,
				`cedula` VARCHAR(255) NULL,
				`celular` VARCHAR(255) NULL,
				`telefono` VARCHAR(255) NULL,
				`aprobado` VARCHAR(255) NULL,
				`fecha_registro` TIMESTAMP NULL,
				`codigo_lead` VARCHAR(255) NULL
			) CHARSET utf8",
			$silent
		);

		setupTable(
			'tipo_movimiento', " 
			CREATE TABLE IF NOT EXISTS `tipo_movimiento` ( 
				`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id`),
				`nombre` VARCHAR(40) NOT NULL
			) CHARSET utf8",
			$silent
		);

		setupTable(
			'aprobaciones', " 
			CREATE TABLE IF NOT EXISTS `aprobaciones` ( 
				`id_aprobaciones` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_aprobaciones`),
				`nombre_aprobaciones` VARCHAR(255) NOT NULL
			) CHARSET utf8",
			$silent
		);

		setupTable(
			'cliente', " 
			CREATE TABLE IF NOT EXISTS `cliente` ( 
				`id_cliente` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_cliente`),
				`cedula_cliente` VARCHAR(255) NOT NULL,
				`nombre_cliente` VARCHAR(255) NOT NULL,
				`apellido_cliente` VARCHAR(255) NOT NULL,
				`correo_cliente` VARCHAR(255) NULL,
				`celular_cliente` VARCHAR(255) NULL
			) CHARSET utf8",
			$silent
		);

		setupTable(
			'producto', " 
			CREATE TABLE IF NOT EXISTS `producto` ( 
				`id_p` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_p`),
				`fecha_creacion_p` DATETIME NULL,
				`codigo_p` VARCHAR(40) NOT NULL,
				`nombre_p` VARCHAR(255) NOT NULL,
				`tipo_producto_p` INT UNSIGNED NULL,
				`estado` INT NULL DEFAULT '1',
				`pvp_producto` DECIMAL(10,2) NOT NULL,
				`manejo_serie` VARCHAR(40) NOT NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('producto', ['tipo_producto_p',]);

		setupTable(
			'ventas', " 
			CREATE TABLE IF NOT EXISTS `ventas` ( 
				`id` INT UNSIGNED NULL AUTO_INCREMENT,
				`vendedor` INT UNSIGNED NOT NULL,
				`fecha_ingreso` TIMESTAMP NULL,
				`usuario_creador` VARCHAR(40) NULL,
				`fecha_actualizacion` TIMESTAMP NULL,
				`usuario_actualizo` VARCHAR(40) NULL,
				`fecha` DATE NOT NULL,
				`numero_contrato` VARCHAR(50) NOT NULL,
				PRIMARY KEY (`numero_contrato`),
				`cedula` VARCHAR(13) NOT NULL,
				`nombre` VARCHAR(255) NOT NULL,
				`mes_comision_venta` VARCHAR(40) NOT NULL,
				`valor_contrato` DECIMAL(10,2) NOT NULL,
				`pvp` DECIMAL(10,2) NOT NULL,
				`a_medias` INT UNSIGNED NULL,
				`dependencia_laboral` INT UNSIGNED NOT NULL,
				`aprobaciones` INT UNSIGNED NOT NULL,
				`cobro_particular` VARCHAR(40) NOT NULL,
				`entrega_cedula` VARCHAR(40) NOT NULL,
				`cuotas` INT NOT NULL,
				`valor_cuota` DECIMAL(10,2) NOT NULL,
				`cantidad_productos` INT NOT NULL,
				`provincia` INT UNSIGNED NOT NULL,
				`canton` INT UNSIGNED NOT NULL,
				`parroquia` INT UNSIGNED NOT NULL,
				`categorias_descripcion` VARCHAR(255) NULL,
				`tipo_venta` INT UNSIGNED NOT NULL
			) CHARSET utf8",
			$silent, [
				"ALTER TABLE `ventas` DROP `pdf_contrato`",
				"ALTER TABLE `ventas` DROP `cedula_contrato`",
				"ALTER TABLE `ventas` DROP `observaciones`",
				"ALTER TABLE `ventas` DROP `pdf_guia_transporte`",
			]
		);
		setupIndexes('ventas', ['vendedor','a_medias','dependencia_laboral','aprobaciones','provincia','canton','parroquia','tipo_venta',]);

		setupTable(
			'producto_venta', " 
			CREATE TABLE IF NOT EXISTS `producto_venta` ( 
				`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id`),
				`id_venta` VARCHAR(50) NOT NULL,
				`id_producto` INT UNSIGNED NOT NULL,
				`cantidad` INT NOT NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('producto_venta', ['id_venta','id_producto',]);

		setupTable(
			'nota_pedido', " 
			CREATE TABLE IF NOT EXISTS `nota_pedido` ( 
				`id` INT UNSIGNED NULL AUTO_INCREMENT,
				`fecha_ingreso` TIMESTAMP NULL,
				`numero_guia` VARCHAR(40) NOT NULL,
				PRIMARY KEY (`numero_guia`),
				`vendedor` INT UNSIGNED NOT NULL,
				`fecha` DATE NOT NULL,
				`cantidad_productos` INT NOT NULL,
				`observaciones` TEXT NOT NULL,
				`usuario_creador` VARCHAR(40) NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('nota_pedido', ['vendedor',]);

		setupTable(
			'producto_nota_pedido', " 
			CREATE TABLE IF NOT EXISTS `producto_nota_pedido` ( 
				`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id`),
				`id_nota_pedido` VARCHAR(40) NOT NULL,
				`id_producto` INT UNSIGNED NOT NULL,
				`cantidad` INT NOT NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('producto_nota_pedido', ['id_nota_pedido','id_producto',]);

		setupTable(
			'nota_devolucion', " 
			CREATE TABLE IF NOT EXISTS `nota_devolucion` ( 
				`id` INT UNSIGNED NULL AUTO_INCREMENT,
				`fecha_ingreso` TIMESTAMP NULL,
				`numero_guia` VARCHAR(40) NOT NULL,
				PRIMARY KEY (`numero_guia`),
				`vendedor` INT UNSIGNED NOT NULL,
				`fecha` DATE NOT NULL,
				`cantidad_productos` INT NOT NULL,
				`observaciones` TEXT NOT NULL,
				`usuario_creador` VARCHAR(40) NULL,
				`archivo_nota_devolucion` VARCHAR(255) NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('nota_devolucion', ['vendedor',]);

		setupTable(
			'producto_nota_devolucion', " 
			CREATE TABLE IF NOT EXISTS `producto_nota_devolucion` ( 
				`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id`),
				`id_nota_devolucion` VARCHAR(40) NOT NULL,
				`id_producto` INT UNSIGNED NOT NULL,
				`cantidad` INT NOT NULL,
				`numero_serie_imei` VARCHAR(255) NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('producto_nota_devolucion', ['id_nota_devolucion','id_producto',]);

		setupTable(
			'stock_general', " 
			CREATE TABLE IF NOT EXISTS `stock_general` ( 
				`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id`),
				`usuario_creador` VARCHAR(40) NULL,
				`usuario_actualizo` VARCHAR(40) NULL,
				`fecha_ingreso` TIMESTAMP NULL,
				`fecha_actualizacion` TIMESTAMP NULL,
				`vendedor` INT UNSIGNED NOT NULL,
				`ano` VARCHAR(40) NOT NULL,
				`mes` VARCHAR(40) NOT NULL,
				`id_producto` INT UNSIGNED NULL,
				`codigo_producto` INT UNSIGNED NOT NULL,
				`tipo_producto` INT UNSIGNED NULL,
				`nombre_producto` INT UNSIGNED NULL,
				`cantidad` INT NOT NULL,
				`pvp` DECIMAL(10,2) UNSIGNED NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('stock_general', ['vendedor','id_producto','codigo_producto','tipo_producto','nombre_producto',]);

		setupTable(
			'control_stock', " 
			CREATE TABLE IF NOT EXISTS `control_stock` ( 
				`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id`),
				`fecha_creacion` TIMESTAMP NULL,
				`fecha_movimiento` DATE NULL,
				`tipo_movimiento` INT UNSIGNED NULL,
				`id_transaccion` VARCHAR(255) NULL,
				`numero_guia_contrato` VARCHAR(255) NULL,
				`vendedor` INT UNSIGNED NULL,
				`cliente` VARCHAR(255) NULL,
				`categoria_producto` INT UNSIGNED NULL,
				`producto` INT UNSIGNED NULL,
				`codigo_producto` INT UNSIGNED NULL,
				`cantidad` INT NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('control_stock', ['tipo_movimiento','vendedor','categoria_producto','producto',]);

		setupTable(
			'guia_remision', " 
			CREATE TABLE IF NOT EXISTS `guia_remision` ( 
				`numero_guia_remision` VARCHAR(255) NOT NULL,
				PRIMARY KEY (`numero_guia_remision`),
				`fecha_creacion_guia_remision` TIMESTAMP NULL,
				`usuario_creador` VARCHAR(255) NULL,
				`fecha_actualizacion` TIMESTAMP NULL,
				`usuario_actualizo` VARCHAR(255) NULL,
				`fecha_inicio_traslado` DATE NOT NULL,
				`fecha_terminacion_traslado` DATE NOT NULL,
				`motivo_traslado` VARCHAR(255) NOT NULL,
				`punto_partida` INT UNSIGNED NOT NULL,
				`destinatario` VARCHAR(255) NOT NULL,
				`ruc_ci` VARCHAR(255) NOT NULL,
				`punto_llegada` INT UNSIGNED NOT NULL,
				`punto_llegada_canton` INT UNSIGNED NOT NULL,
				`placa` VARCHAR(255) NOT NULL,
				`persona_encarga_nombre` VARCHAR(255) NOT NULL,
				`ruc_ci_persona_encargada` VARCHAR(255) NOT NULL,
				`vendedor` INT UNSIGNED NOT NULL,
				`nota_pedido` VARCHAR(40) NOT NULL,
				`archivo_guia_remision` VARCHAR(255) NULL,
				`archivo_entrega_mercaderia` VARCHAR(255) NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('guia_remision', ['punto_partida','punto_llegada','punto_llegada_canton','vendedor','nota_pedido',]);

		setupTable(
			'producto_guia_remision', " 
			CREATE TABLE IF NOT EXISTS `producto_guia_remision` ( 
				`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id`),
				`id_guia_remision` VARCHAR(255) NOT NULL,
				`id_producto` INT UNSIGNED NOT NULL,
				`codigo_producto` INT UNSIGNED NULL,
				`cantidad` INT NOT NULL,
				`vendedor` INT UNSIGNED NULL,
				`mes` VARCHAR(255) NULL,
				`numero_serie_imei` VARCHAR(255) NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('producto_guia_remision', ['id_producto','vendedor',]);

		setupTable(
			'tipo_venta', " 
			CREATE TABLE IF NOT EXISTS `tipo_venta` ( 
				`id_tipo_venta` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_tipo_venta`),
				`nombre_tipo_venta` VARCHAR(255) NOT NULL
			) CHARSET utf8",
			$silent
		);

		setupTable(
			'inventario_bodega_central', " 
			CREATE TABLE IF NOT EXISTS `inventario_bodega_central` ( 
				`id_inventario_bodega_central` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_inventario_bodega_central`),
				`codigo_producto` INT UNSIGNED NOT NULL,
				`nombre_producto` VARCHAR(255) NOT NULL,
				`cantidad_producto` INT NOT NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('inventario_bodega_central', ['codigo_producto',]);

		setupTable(
			'compras', " 
			CREATE TABLE IF NOT EXISTS `compras` ( 
				`id_compras` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_compras`),
				`numero_factura` VARCHAR(255) NOT NULL,
				`ruc` INT UNSIGNED NOT NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('compras', ['ruc',]);

		setupTable(
			'compra_producto', " 
			CREATE TABLE IF NOT EXISTS `compra_producto` ( 
				`id_compra_producto` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_compra_producto`),
				`compra_compra_producto` INT UNSIGNED NOT NULL,
				`producto_compra_producto` INT UNSIGNED NOT NULL,
				`nombre_producto_compra_producto` INT UNSIGNED NOT NULL
			) CHARSET utf8",
			$silent
		);
		setupIndexes('compra_producto', ['compra_compra_producto','producto_compra_producto',]);

		setupTable(
			'ruc', " 
			CREATE TABLE IF NOT EXISTS `ruc` ( 
				`id_ruc` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id_ruc`),
				`nombre_ruc` VARCHAR(255) NOT NULL,
				`numero_ruc` VARCHAR(255) NOT NULL
			) CHARSET utf8",
			$silent
		);



		// save MD5
		@file_put_contents(dirname(__FILE__) . '/setup.md5', $thisMD5);
	}


	function setupIndexes($tableName, $arrFields) {
		if(!is_array($arrFields) || !count($arrFields)) return false;

		foreach($arrFields as $fieldName) {
			if(!$res = @db_query("SHOW COLUMNS FROM `$tableName` like '$fieldName'")) continue;
			if(!$row = @db_fetch_assoc($res)) continue;
			if($row['Key']) continue;

			@db_query("ALTER TABLE `$tableName` ADD INDEX `$fieldName` (`$fieldName`)");
		}
	}


	function setupTable($tableName, $createSQL = '', $silent = true, $arrAlter = '') {
		global $Translation;
		$oldTableName = '';
		ob_start();

		echo '<div style="padding: 5px; border-bottom:solid 1px silver; font-family: verdana, arial; font-size: 10px;">';

		// is there a table rename query?
		if(is_array($arrAlter)) {
			$matches = [];
			if(preg_match("/ALTER TABLE `(.*)` RENAME `$tableName`/i", $arrAlter[0], $matches)) {
				$oldTableName = $matches[1];
			}
		}

		if($res = @db_query("SELECT COUNT(1) FROM `$tableName`")) { // table already exists
			if($row = @db_fetch_array($res)) {
				echo str_replace(['<TableName>', '<NumRecords>'], [$tableName, $row[0]], $Translation['table exists']);
				if(is_array($arrAlter)) {
					echo '<br>';
					foreach($arrAlter as $alter) {
						if($alter != '') {
							echo "$alter ... ";
							if(!@db_query($alter)) {
								echo '<span class="label label-danger">' . $Translation['failed'] . '</span>';
								echo '<div class="text-danger">' . $Translation['mysql said'] . ' ' . db_error(db_link()) . '</div>';
							} else {
								echo '<span class="label label-success">' . $Translation['ok'] . '</span>';
							}
						}
					}
				} else {
					echo $Translation['table uptodate'];
				}
			} else {
				echo str_replace('<TableName>', $tableName, $Translation['couldnt count']);
			}
		} else { // given tableName doesn't exist

			if($oldTableName != '') { // if we have a table rename query
				if($ro = @db_query("SELECT COUNT(1) FROM `$oldTableName`")) { // if old table exists, rename it.
					$renameQuery = array_shift($arrAlter); // get and remove rename query

					echo "$renameQuery ... ";
					if(!@db_query($renameQuery)) {
						echo '<span class="label label-danger">' . $Translation['failed'] . '</span>';
						echo '<div class="text-danger">' . $Translation['mysql said'] . ' ' . db_error(db_link()) . '</div>';
					} else {
						echo '<span class="label label-success">' . $Translation['ok'] . '</span>';
					}

					if(is_array($arrAlter)) setupTable($tableName, $createSQL, false, $arrAlter); // execute Alter queries on renamed table ...
				} else { // if old tableName doesn't exist (nor the new one since we're here), then just create the table.
					setupTable($tableName, $createSQL, false); // no Alter queries passed ...
				}
			} else { // tableName doesn't exist and no rename, so just create the table
				echo str_replace("<TableName>", $tableName, $Translation["creating table"]);
				if(!@db_query($createSQL)) {
					echo '<span class="label label-danger">' . $Translation['failed'] . '</span>';
					echo '<div class="text-danger">' . $Translation['mysql said'] . db_error(db_link()) . '</div>';
				} else {
					echo '<span class="label label-success">' . $Translation['ok'] . '</span>';
				}
			}
		}

		echo '</div>';

		$out = ob_get_clean();
		if(!$silent) echo $out;
	}
