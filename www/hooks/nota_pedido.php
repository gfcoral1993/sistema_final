<?php
/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Review Date: 2021-11-08
 * Time: 17:30
 */
/**
 * @param $options
 * @param $memberInfo
 * @param $args
 * @return bool
 */
function nota_pedido_init(&$options, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $contentType
 * @param $memberInfo
 * @param $args
 * @return string
 */
function nota_pedido_header($contentType, $memberInfo, &$args)
{
    $header = '';

    switch ($contentType) {
        case 'tableview':
            $header = '';
            break;

        case 'detailview':
            $header = '';
            break;

        case 'tableview+detailview':
            $header = '';
            break;

        case 'print-tableview':
            $header = '';
            break;

        case 'print-detailview':
            $header = '';
            break;

        case 'filters':
            $header = '';
            break;
    }

    return $header;
}

/**
 * @param $contentType
 * @param $memberInfo
 * @param $args
 * @return string
 */
function nota_pedido_footer($contentType, $memberInfo, &$args)
{
    $footer = '';

    switch ($contentType) {
        case 'tableview':
            $footer = '';
            break;

        case 'detailview':
            $footer = '';
            break;

        case 'tableview+detailview':
            $footer = '';
            break;

        case 'print-tableview':
            $footer = '';
            break;

        case 'print-detailview':
            $footer = '';
            break;

        case 'filters':
            $footer = '';
            break;
    }

    return $footer;
}

/**
 * @param $data
 * @param $memberInfo
 * @param $args
 * @return bool
 */
function nota_pedido_before_insert(&$data, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $data
 * @param $memberInfo
 * @param $args
 * @return bool
 */
function nota_pedido_after_insert($data, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $numero
 * @return string
 */
function getMes($numero)
{
    $mes = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"];

    $indice = (int)$numero;

    $indice = $indice - 1;

    return $mes[$indice];

}

/**
 * @param $data
 * @param $memberInfo
 * @param $args
 * @return bool
 */
function nota_pedido_before_update(&$data, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $data
 * @param $memberInfo
 * @param $args
 * @return bool
 */
function nota_pedido_after_update($data, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $selectedID
 * @param $skipChecks
 * @param $memberInfo
 * @param $args
 * @return bool
 */
function nota_pedido_before_delete($selectedID, &$skipChecks, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $selectedID
 * @param $memberInfo
 * @param $args
 */
function nota_pedido_after_delete($selectedID, $memberInfo, &$args)
{

}

/**
 * @param $selectedID
 * @param $memberInfo
 * @param $html
 * @param $args
 */
function nota_pedido_dv($selectedID, $memberInfo, &$html, &$args)
{
    $html .= '<div class="col-xs-12 detail_view">
    <h1 align="center">Ingrese los productos a elegir de la Nota de Pedido</h1>
</div>
<div class="col-xs-12 detail_view">
    <div class="row">
        <input class="col-xs-8 form-control" id="barra_producto"
               placeholder="Ingrese el nombre del producto o su codigo" style="width: 65% !important;"/>
        <input class="col-xs-2 btn btn-success" id="buscar_producto" type="button" value="Buscar"/>
        <input class="col-xs-2 btn btn-danger" id="eliminar_busqueda_producto" type="button" value="Borrar"/>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
</div>
<div class="col-xs-12 detail_view">
    <h3 align="center">Productos Encontrados</h3>
    <div id="result"></div>
    <hr/>
</div>
<div class="col-xs-12 detail_view">
    <div class="table-responsive">
        <table class="table" id="tabla_productos_nota_pedido" align="center">
            <thead class="thead-dark">
            <tr align="center">
                <th scope="col">Items</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Codigo</th>
                <th scope="col">Agregar a la Nota de Pedido</th>
            </tr>
            </thead>
            <tbody id="tbl_body_productos_nota_pedido">
            <tr>
                <td class="items">No</td>
                <td class="cantidad"> se han</td>
                <td class="producto"> agregado</td>
                <td class="codigo"> Productos</td>
                <td class="accion">a la Nota de Pedido</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <input id="btnAgregarProductos" type="button" value="Agregar a la Nota de Pedido" class="col-xs-2 btn btn-success"/>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <h3 align="center">Productos de la Nota de Pedido</h3>
</div>
<div class="col-xs-12 detail_view">
    <div class="table-responsive">
        <table class="table" id="tabla_productos_nota_pedido_final">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Items</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Codigo</th>
                 <th scope="col">Precio</th>
                <th scope="col">Eliminar de la Nota de Pedido</th>
            </tr>
            </thead>
            <tbody id="tbl_body_productos_nota_pedido_final">

            </tbody>
        </table>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <input id="btnEliminarProductosNotaPedido" type="button" value="Eliminar de la Nota de Pedido"
           class="col-xs-2 btn btn-danger"/>
    <br />
    <br />
    <br />
    <input id="btnSaveHijosProductoNotaPedido" type="button" value="Guardar Productos"
           class="col-xs-2 btn btn-success"/>
</div>';
}

/**
 * @param $query
 * @param $memberInfo
 * @param $args
 * @return mixed
 */
function nota_pedido_csv($query, $memberInfo, &$args)
{

    return $query;
}

/**
 * @param $args
 * @return array
 */
function nota_pedido_batch_actions(&$args)
{

    return [];
}
