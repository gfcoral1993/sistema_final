<head>
    <title>Inicio Recalculo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <style type="text/css">
    </style>
    <script type='text/javascript' src='../../resources/jquery/js/jquery-1.11.2.min.js'></script>
</head>
<?php
/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2020-07-09
 * Time: 17:05
 */
define('PREPEND_PATH', '../../');
include("../../defaultLang.php");
include("../../language.php");
include("../../lib.php");
$hooks_dir = dirname(__FILE__);
include_once("../../header.php");
/* grant access to the groups 'Admins' and 'Data entry' */
$mi = getMemberInfo();
if (!in_array($mi['group'], array('Admins', 'Profesores'))) {
    echo "<br /><br /><br />";
    echo "<div style=\"text-align: center\">
                <img class='img-responsive' src=\"../../hooks/resources/acceso_denegado.png\" align=\"middle\" width='200%' height='100%'>
          </div>";
    exit;
}

?>
<div align="center"><h1>Inicio de RECALCULO</h1></div>
<form action="recalcular.php" method="post">
    <div align="center">
        <table>
            <tr>
                <td>
                    Seleccione el vendedor para realizar el recalculo<br/>
                </td>
                <td align="right">
                    <br/>
                    <br/>
                    <label for="codigo_vendedor">Asesor Comercial: </label>
                    <select style="width:100% !important;" id="codigo_vendedor" name="user_codigo_vendedor"
                            class="form-control" required>

                    </select>
                </td>
            </tr>

            <tr>
                <td>
                    Seleccione el AÑ0 para realizar el recalculo<br/>
                </td>
                <td align="right">
                    <br/>
                    <br/>
                    <label for="ano_recalculo">AÑO: </label>
                    <select style="width:100% !important;" id="ano_recalculo" name="user_ano_recalculo"
                            class="form-control" required>
                        <option value="">SELECCIONE UN AÑO</option>
                        <option value='TODOS'>TODOS</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Seleccione el MES para realizar el recalculo<br/>
                </td>
                <td align="right">
                    <br/>
                    <br/>
                    <label for="mes_recalculo">MES: </label>
                    <select style="width:100% !important;" id="mes_recalculo" name="user_mes_recalculo"
                            class="form-control" required>
                        <option value="">SELECCIONE UN MES</option>
                        <option value='TODOS'>TODOS</option>
                        <option value="1">ENERO</option>
                        <option value="2">FEBRERO</option>
                        <option value="3">MARZO</option>
                        <option value="4">ABRIL</option>
                        <option value="5">MAYO</option>
                        <option value="6">JUNIO</option>
                        <option value="7">JULIO</option>
                        <option value="8">AGOSTO</option>
                        <option value="9">SEPTIEMBRE</option>
                        <option value="10">OCTUBRE</option>
                        <option value="11">NOVIEMBRE</option>
                        <option value="12">DICIEMBRE</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div align="center">
                        <br/>
                        <br/>
                        <button type="submit" class="btn btn-danger navbar-btn hidden-xs">INICIAR RECALCULO</button>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form>

<div align="center">
    <p>TOME EN CUENTA QUE SI ELIJE LA OPCION DE TODOS LOS VENDEDORES EL TIEMPO DE RECALCULO SERA AMPLIO<br />
        NO SE DEBEN INGRESAR NOTAS DE PEDIDO NI GUIAS DE REMISION NI VENTAS NI NOTAS DE DEVOLUCION DURANTE EL PROCESO DE RECALCULO<br />
        SE RECOMIENDA QUE EL RECALCULO LO EJECUTE UNA SOLA PERSONA Y EL RESTO DE USUARIOS ESTEN DESLOGUEADOS ( FUERA DEL SISTEMA ) DURANTE EL PROCESO DE RECALCULO
    </p>
</div>
<?php
include_once("$currDir/footer.php");
?>
<script type="text/javascript">
    $j(document).ready(function () {
        $j("#codigo_vendedor").empty().append('<option value="0">SELECCIONE UN VENDEDOR</option>');
        $j.ajax({
            type: 'POST',
            url: 'ajax/ajax-vendedores.php',
            data: {},
            dataType: 'json',
            contenttype: "application/json; charset=utf-8",
            cache: false,
            success: function (result) {
                var options = "";
                for (var i = 0; i < result.length; i += 1) {
                    var id_vendedor = result[i]['id_vendedor'];
                    var nombre_vendedor = result[i]['nombre_vendedor'];
                    options += "<option value=" + id_vendedor + ">" + nombre_vendedor + "</option>";
                }
                options += "<option value='TODOS'>TODOS</option>";
                //Limpio el combo box
                //$("#lista_provincias").empty().append('<option value="0">Seleccione una Provincia</option>');
                //Cargo las ciudades de la provincia respectiva
                $j("#codigo_vendedor").append(options);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                var response = JSON.parse(xhr.responseText);
                console.log(response);
            }
        });

    });
</script>
