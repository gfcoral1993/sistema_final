<head>
    <title>Recálculo de Stock's</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <style type="text/css">
    </style>
    <script type='text/javascript' src='../../resources/jquery/js/jquery-1.11.2.min.js'></script>
</head>
<?php
define('PREPEND_PATH', '../../');
include("../../defaultLang.php");
include("../../lib.php");
include("../../language.php");
$hooks_dir = dirname(__FILE__);
include_once("../../header.php");

/* grant access to the groups 'Admins' and 'Data entry' */
$mi = getMemberInfo();
if (!in_array($mi['group'], array('Admins', 'Profesores'))) {
    echo "<br /><br /><br />";
    echo "<div style=\"text-align: center\">
                <img class='img-responsive' src=\"../../hooks/resources/acceso_denegado.png\" align=\"middle\" width='200%' height='100%'>
          </div>";
    exit;
}
$usuario = $mi['username'];

//Borramos todos los movimientos del stock
sql("TRUNCATE control_stock;", $eo);

//Borramos todo el stock de los vendedores
sql("TRUNCATE stock_general;", $eo);

//Querys para la consulta
$query_nota_pedido = "SELECT * FROM nota_pedido;";
$query_venta = "SELECT * FROM ventas;";
$query_nota_devolucion = "SELECT * FROM nota_devolucion ORDER BY id;";

//Ejecucion de los querys
$res_nota_pedido = sql($query_nota_pedido, $eo);
$res_venta = sql($query_venta, $eo);
$res_nota_devolucion = sql($query_nota_devolucion, $eo);

//Arreglos para guardar la informacion de las notas y ventas sin productos
$notas_pedidos = array();
$ventas = array();
$notas_devoluciones = array();

//Llenado de la informacion
$i = 0;
if ($res_nota_pedido->num_rows > 0) {
    while ($data = $res_nota_pedido->fetch_assoc()) {
        $numero_guia = $data['numero_guia'];
        if (validar_productos_nota_pedido($numero_guia)) {
            recalculo_nota_pedido($data);
            $i++;
            echo $i . " => Se reindexo la NOTA DE PEDIDO: " . $data['numero_guia'] . " <br />";
        } else {
            array_push($notas_pedidos, $numero_guia);
        }
    }
}
$j = 0;
if ($res_venta->num_rows > 0) {
    while ($data = $res_venta->fetch_assoc()) {
        $numero_contrato = $data['numero_contrato'];
        if (validar_productos_venta($numero_contrato)) {
            recalculo_nota_ventas($data);
            $j++;
            echo $j . " =>Se reindexo la VENTA: " . $data['numero_contrato'] . " <br />";
        } else {
            array_push($ventas, $numero_contrato);
        }
    }
}
$k = 0;
if ($res_nota_devolucion->num_rows > 0) {
    while ($data = $res_nota_devolucion->fetch_assoc()) {
        $numero_guia = $data['numero_guia'];
        if (validar_productos_nota_devolucion($numero_guia)) {
            $k++;
            recalculo_nota_devolucion($data);
            echo $k . " =>Se reindexo la NOTA DE DEVOLUCION: " . $data['numero_guia'] . " <br />";
        } else {
            array_push($notas_devoluciones, $numero_guia);
        }
    }
}

//Mostrar las Notas de Pedidos que no tienen Productos
echo "<h3>Las siguientes Notas de Pedidos no tienen Productos, favor corregir: </h3>";

$u = 0;
if (count($notas_pedidos) > 0) {
    foreach ($notas_pedidos as $nota_pedido) {
        $u++;
        echo $u . ".- Nota de Pedido: " . $nota_pedido;
    }
}

//Mostrar las Ventas que no tienen Productos
echo "<h3>Las siguientes Ventas no tienen Productos, favor corregir: </h3>";

$l = 0;
if (count($ventas) > 0) {
    foreach ($ventas as $venta) {
        $l++;
        echo $l . ".- Nota de Pedido: " . $venta;
    }
}
//Mostrar las Notas de Pedidos que no tienen Productos
echo "<h3>Las siguientes Notas de Devoluciones no tienen Productos, favor corregir: </h3>";

$p = 0;
if (count($notas_devoluciones) > 0) {
    foreach ($notas_devoluciones as $notas_devolucion) {
        $p++;
        echo $p . ".- Nota de Pedido: " . $notas_devolucion;
    }
}

//Funciones
function getMes($numero)
{
    $mes = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"];

    $indice = (int)$numero;

    $indice = $indice - 1;

    return $mes[$indice];

}

function recalculo_nota_pedido($data)
{
    $fecha_movimiento = $data['fecha'];

    $tipo_movimiento = 1;

    $numero_guia = $data['numero_guia'];

    $vendedor = $data['vendedor'];

    $id_nota_pedido = $numero_guia;

    //Obtengo los productos de esta transaccion

    $query_productos_nota_pedido = "SELECT * FROM producto_nota_pedido WHERE id_nota_pedido = '$id_nota_pedido';";

    $res_query_productos_nota_pedido = sql($query_productos_nota_pedido, $eo);

    $productos_nota_pedido = array();

    if ($res_query_productos_nota_pedido->num_rows > 0) {
        while ($res_X = $res_query_productos_nota_pedido->fetch_assoc()) {
            $id_producto = $res_X["id_producto"];
            $cantidad_producto = $res_X["cantidad"];
            $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = $id_producto;");
            $tipo_producto = sqlValue("SELECT p.tipo_producto_p AS tipo FROM producto AS p WHERE p.`id_p` = $id_producto;");

            array_push($productos_nota_pedido, array(
                "id_producto" => $id_producto,
                "cantidad_producto" => $cantidad_producto,
                "tipo_producto" => $tipo_producto,
                "codigo_producto" => $codigo_producto
            ));
        }
    }

    $query_insert_stock = "INSERT INTO control_stock (fecha_creacion,fecha_movimiento,tipo_movimiento,numero_guia_contrato,id_transaccion,codigo_producto,vendedor,producto,categoria_producto,cantidad) VALUES ";

    $query_values = "";

    foreach ($productos_nota_pedido as $producto_nota_pedido) {

        $query_delete_update = "DELETE FROM control_stock where numero_guia_contrato = '$numero_guia' AND id_transaccion = '$id_nota_pedido' AND codigo_producto = '" . $producto_nota_pedido['id_producto'] . "';";
        sql($query_delete_update, $eo);

        $query_values .= "(CURRENT_TIMESTAMP,'" . $fecha_movimiento . "','$tipo_movimiento','" . $numero_guia . "','$id_nota_pedido','" . $producto_nota_pedido['id_producto'] . "','" . $vendedor . "','" . $producto_nota_pedido['id_producto'] . "','" . $producto_nota_pedido['tipo_producto'] . "','" . $producto_nota_pedido['cantidad_producto'] . "'),";
    }

    $query_values = rtrim($query_values, ',');

    $query_general = $query_insert_stock . $query_values . ";";

    $res_insercion_x = sql($query_general, $eo);

    // =================================================================================================================

    //Actualizacion del Stock actualizado

    //Obtengo el mes del stock general
    $mes_stock = date('m', strtotime($fecha_movimiento));
    $mes_stock = getMes($mes_stock);
    //Productos que ya estan en stock
    $productos_stock_old = array();
    //Productos nuevos que no estan en stock
    $productos_stock_new = array();
    $x = 0;
    $j = 0;
    foreach ($productos_nota_pedido as $productonp) {
        $id_producto_np = $productonp['id_producto'];
        $cantidad_producto_np = $productonp['cantidad_producto'];
        $query_validacion_2 = "SELECT COUNT(*) AS validacion FROM stock_general WHERE vendedor = $vendedor AND mes = '" . $mes_stock . "' AND id_producto = $id_producto_np;";
        $validacion = null;
        $res_xy = sql($query_validacion_2, $eo);
        if ($res_xy->num_rows > 0) {
            while ($res_ab = $res_xy->fetch_assoc()) {
                $validacion = (int)$res_ab['validacion'];
            }
        }
        if ($validacion >= 1) {
            array_push($productos_stock_old, array(
                "id_producto" => $id_producto_np,
                "cantidad_producto" => $cantidad_producto_np
            ));
            $x++;
        } else {
            array_push($productos_stock_new, array(
                "id_producto" => $id_producto_np,
                "cantidad_producto" => $cantidad_producto_np
            ));
            $j++;
        }
    }
    //echo $x;
    //echo $j;
    //Inserto los productos que no tenia en ese mes
    $estructura_insert_Sql = "INSERT INTO stock_general(`usuario_creador`,`usuario_actualizo`,`fecha_ingreso`,`fecha_actualizacion`,`id_producto`,`codigo_producto`,`nombre_producto`,`tipo_producto`,`cantidad`,`vendedor`,`mes`) VALUES";
    $values_insertar_new = "";
    if (count($productos_stock_new) > 0) {
        foreach ($productos_stock_new as $producto) {
            $id_producto_np = $producto['id_producto'];
            $cantidad_producto_np = $producto['cantidad_producto'];
            $values_insertar_new .= "('sistema','sistema',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,$id_producto_np,$id_producto_np,$id_producto_np,$id_producto_np,$cantidad_producto_np,$vendedor,'$mes_stock'),";
        }
        $values_insertar_new = rtrim($values_insertar_new, ",");
        $query_final_stocks_new = $estructura_insert_Sql . $values_insertar_new . ";";
        sql($query_final_stocks_new, $eo);
    }
    //Actualizo la cantidad en los productos que habia en ese mes
    if (count($productos_stock_old) > 0) {
        foreach ($productos_stock_old as $producto) {
            $id_producto_np = $producto['id_producto'];
            $cantidad_actual = sqlValue("SELECT cantidad FROM stock_general WHERE mes = '" . $mes_stock . "' AND vendedor=$vendedor AND id_producto=$id_producto_np;");
            $cantidad_producto_np = $producto['cantidad_producto'];
            $nueva_cantidad = $cantidad_actual + $cantidad_producto_np;
            $query_update_nota_pedido_producto = "UPDATE stock_general SET fecha_actualizacion = CURRENT_TIMESTAMP, cantidad = $nueva_cantidad WHERE vendedor = $vendedor AND mes = '$mes_stock' AND id_producto = $id_producto_np;";
            sql($query_update_nota_pedido_producto, $eo);
        }
    }
    //Fin de actualizar stocks
    // =================================================================================================================
    if ($res_insercion_x) {
        //to redirect a specific page.
        return TRUE;
    } else {
        return FALSE;
    }
}

function recalculo_nota_ventas($data)
{
    $fecha_movimiento = $data['fecha'];

    $tipo_movimiento = 3;

    $numero_contrato = $data['numero_contrato'];

    $vendedor = $data['vendedor'];

    $cliente = $data['nombre'];

    $id_venta = $numero_contrato;

    //Obtengo los productos de esta transaccion

    $query_productos_venta = "SELECT * FROM producto_venta WHERE id_venta = '$id_venta';";

    $res_query_productos_venta = sql($query_productos_venta, $eo);

    $productos_venta = array();

    $array_categorias = array();

    if ($res_query_productos_venta->num_rows > 0) {
        while ($res_X = $res_query_productos_venta->fetch_assoc()) {
            $id_producto = $res_X["id_producto"];
            $cantidad_producto = $res_X["cantidad"];
            $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = $id_producto;");
            $tipo_producto = sqlValue("SELECT p.tipo_producto_p AS tipo FROM producto AS p WHERE p.`id_p` = $id_producto;");

            //Servira para el update del campo de Categoria en la VENTA => row
            array_push($array_categorias, $tipo_producto);

            //Agrego los productos de esa venta
            array_push($productos_venta, array(
                "id_producto" => $id_producto,
                "cantidad_producto" => $cantidad_producto,
                "tipo_producto" => $tipo_producto,
                "codigo_producto" => $codigo_producto
            ));
        }
    }

    $query_insert_stock = "INSERT INTO control_stock (fecha_creacion,fecha_movimiento,tipo_movimiento,numero_guia_contrato,id_transaccion,codigo_producto,vendedor,producto,categoria_producto,cantidad,cliente) VALUES ";

    $query_values = "";

    foreach ($productos_venta as $producto_venta) {

        $query_delete_update = "DELETE FROM control_stock where numero_guia_contrato = '$numero_contrato' AND id_transaccion = '$id_venta' AND codigo_producto = '" . $productos_venta['id_producto'] . "';";

        sql($query_delete_update, $eo);

        $query_values .= "(CURRENT_TIMESTAMP,'" . $fecha_movimiento . "','$tipo_movimiento','" . $numero_contrato . "','$id_venta','" . $producto_venta['id_producto'] . "','" . $vendedor . "','" . $producto_venta['id_producto'] . "','" . $producto_venta['tipo_producto'] . "','" . $producto_venta['cantidad_producto'] . "','$cliente'),";
    }

    $query_values = rtrim($query_values, ',');

    $query_general = $query_insert_stock . $query_values . ";";

    $res_insercion_x = sql($query_general, $eo);


    //Update de Categorias
    $categorias_unicas = array_unique($array_categorias);

    $nombres_categorias_unicas = array();

    foreach ($categorias_unicas as $categoria_unica) {
        $categoria = sqlValue("SELECT nombre_tp FROM tipo_producto WHERE id_tp = $categoria_unica;");
        array_push($nombres_categorias_unicas, $categoria);
    }
    $texto = implode("-", $nombres_categorias_unicas);

    $rquery_update = "UPDATE ventas SET categorias_descripcion = '" . $texto . "' WHERE id = $id_venta;";

    sql($rquery_update, $eo);

    //Fin de Update Categorias

    // =================================================================================================================
    //Actualizacion del Stock actualizado

    //Obtengo el mes del stock general
    $mes_stock = date('m', strtotime($fecha_movimiento));
    $mes_stock = getMes($mes_stock);
    //Productos que ya estan en stock
    $productos_stock_old = array();
    $x = 0;
    $j = 0;

    foreach ($productos_venta as $productopv) {
        $id_producto_pv = $productopv['id_producto'];
        $cantidad_producto_pv = $productopv['cantidad_producto'];
        //$query_validacion = "SELECT count (*) FROM stock_general WHERE mes = '$mes_stock' AND vendedor=$vendedor AND id_producto=$id_producto_np;";
        $query_validacion_2 = "SELECT COUNT(*) AS validacion FROM stock_general WHERE vendedor = $vendedor AND mes = '" . $mes_stock . "' AND id_producto = $id_producto_pv;";
        //$validacion = sqlValue($query_validacion, $eo);
        $validacion = null;
        $res_xy = sql($query_validacion_2, $eo);
        if ($res_xy->num_rows > 0) {
            while ($res_ab = $res_xy->fetch_assoc()) {
                $validacion = (int)$res_ab['validacion'];
            }
        }
        if ($validacion >= 1) {
            array_push($productos_stock_old, array(
                "id_producto" => $id_producto_pv,
                "cantidad_producto" => $cantidad_producto_pv
            ));
            $x++;
        } else {
            array_push($productos_stock_new, array(
                "id_producto" => $id_producto_pv,
                "cantidad_producto" => $cantidad_producto_pv
            ));
            $j++;
        }
    }

//    echo $x;
//
//    echo $j;

    //Actualizo la cantidad en los productos que habia en ese mes
    if (count($productos_stock_old) > 0) {
        foreach ($productos_stock_old as $producto) {
            $id_producto_pv = $producto['id_producto'];
            $cantidad_actual = sqlValue("SELECT cantidad FROM stock_general WHERE mes = '" . $mes_stock . "' AND vendedor=$vendedor AND id_producto=$id_producto_pv;");
            $cantidad_producto_pv = $producto['cantidad_producto'];
            $nueva_cantidad = $cantidad_actual - $cantidad_producto_pv;
            $query_update_nota_pedido_producto = "UPDATE stock_general SET fecha_actualizacion = CURRENT_TIMESTAMP, cantidad = $nueva_cantidad WHERE vendedor = $vendedor AND mes = '$mes_stock' AND id_producto = $id_producto_pv;";
            sql($query_update_nota_pedido_producto, $eo);
        }
    }
    //Fin de actualizar stocks
    // =================================================================================================================

    if ($res_insercion_x) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function recalculo_nota_devolucion($data)
{
    $fecha_movimiento = $data['fecha'];

    $tipo_movimiento = 2;

    $numero_guia = $data['numero_guia'];

    $vendedor = $data['vendedor'];

    $id_nota_devolucion = $numero_guia;

    //Obtengo los productos de esta transaccion

    $query_productos_nota_devolucion = "SELECT * FROM producto_nota_devolucion WHERE id_nota_devolucion = '$id_nota_devolucion';";

    $res_query_productos_nota_devolucion = sql($query_productos_nota_devolucion, $eo);

    $productos_nota_devolucion = array();

    if ($res_query_productos_nota_devolucion->num_rows > 0) {
        while ($res_X = $res_query_productos_nota_devolucion->fetch_assoc()) {
            $id_producto = $res_X["id_producto"];
            $cantidad_producto = $res_X["cantidad"];
            $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = $id_producto;");
            $tipo_producto = sqlValue("SELECT p.tipo_producto_p AS tipo FROM producto AS p WHERE p.`id_p` = $id_producto;");

            array_push($productos_nota_devolucion, array(
                "id_producto" => $id_producto,
                "cantidad_producto" => $cantidad_producto,
                "tipo_producto" => $tipo_producto,
                "codigo_producto" => $codigo_producto
            ));
        }
    }

    $query_insert_stock = "INSERT INTO control_stock (fecha_creacion,fecha_movimiento,tipo_movimiento,numero_guia_contrato,id_transaccion,codigo_producto,vendedor,producto,categoria_producto,cantidad) VALUES ";

    $query_values = "";

    foreach ($productos_nota_devolucion as $producto_nota_devolucion) {
        $query_values .= "(CURRENT_TIMESTAMP,'" . $fecha_movimiento . "','$tipo_movimiento','" . $numero_guia . "','$id_nota_devolucion','" . $producto_nota_devolucion['id_producto'] . "','" . $vendedor . "','" . $producto_nota_devolucion['id_producto'] . "','" . $producto_nota_devolucion['tipo_producto'] . "','" . $producto_nota_devolucion['cantidad_producto'] . "'),";
    }

    $query_values = rtrim($query_values, ',');

    $query_general = $query_insert_stock . $query_values . ";";

    $res_insercion_x = sql($query_general, $eo);

    // =================================================================================================================

    //Actualizacion del Stock actualizado

    //Obtengo el mes del stock general
    $mes_stock = date('m', strtotime($fecha_movimiento));
    $mes_stock = getMes($mes_stock);
    //Productos que ya estan en stock
    $productos_stock_old = array();
    //Productos nuevos que no estan en stock
//    $productos_stock_new = array();
    $x = 0;
    $j = 0;

    foreach ($productos_nota_devolucion as $productond) {
        $id_producto_nd = $productond['id_producto'];
        $cantidad_producto_nd = $productond['cantidad_producto'];
        //$query_validacion = "SELECT count (*) FROM stock_general WHERE mes = '$mes_stock' AND vendedor=$vendedor AND id_producto=$id_producto_np;";
        $query_validacion_2 = "SELECT COUNT(*) AS validacion FROM stock_general WHERE vendedor = $vendedor AND mes = '" . $mes_stock . "' AND id_producto = $id_producto_nd;";
        //$validacion = sqlValue($query_validacion, $eo);
        $validacion = null;
        $res_xy = sql($query_validacion_2, $eo);
        if ($res_xy->num_rows > 0) {
            while ($res_ab = $res_xy->fetch_assoc()) {
                $validacion = (int)$res_ab['validacion'];
            }
        }
        if ($validacion >= 1) {
            array_push($productos_stock_old, array(
                "id_producto" => $id_producto_nd,
                "cantidad_producto" => $cantidad_producto_nd
            ));
            $x++;
        } else {
            $productos_stock_new[] = array(
                "id_producto" => $id_producto_nd,
                "cantidad_producto" => $cantidad_producto_nd
            );
//            array_push($productos_stock_new, array(
//                "id_producto" => $id_producto_nd,
//                "cantidad_producto" => $cantidad_producto_nd
//            ));
            $j++;
        }
    }

    echo $x;

    echo $j;

    //Actualizo la cantidad en los productos que habia en ese mes
    if (count($productos_stock_old) > 0) {
        foreach ($productos_stock_old as $producto) {
            $id_producto_nd = $producto['id_producto'];
            $cantidad_actual = sqlValue("SELECT cantidad FROM stock_general WHERE mes = '" . $mes_stock . "' AND vendedor=$vendedor AND id_producto=$id_producto_nd;");
            $cantidad_producto_nd = $producto['cantidad_producto'];
            $nueva_cantidad = $cantidad_actual - $cantidad_producto_nd;
            $query_update_nota_pedido_producto = "UPDATE stock_general SET fecha_actualizacion = CURRENT_TIMESTAMP, cantidad = $nueva_cantidad WHERE vendedor = $vendedor AND mes = '$mes_stock' AND id_producto = $id_producto_nd;";
            sql($query_update_nota_pedido_producto, $eo);
        }
    }
    //Fin de actualizar stocks
    // =================================================================================================================

    if ($res_insercion_x) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function validar_productos_nota_pedido($numero_guia)
{
    $query = "SELECT COUNT(*) FROM producto_nota_pedido WHERE id_nota_pedido =  '$numero_guia';";
    $cantidad = sqlValue($query);
    if ($cantidad > 0) {
        return true;
    } else {
        return false;
    }
}

function validar_productos_venta($numero_contrato)
{
    $query = "SELECT COUNT(*) FROM producto_venta WHERE id_venta =  '$numero_contrato';";
    $cantidad = sqlValue($query);
    if ($cantidad > 0) {
        return true;
    } else {
        return false;
    }
}

function validar_productos_nota_devolucion($numero_guia)
{
    $query = "SELECT COUNT(*) FROM producto_nota_devolucion WHERE id_nota_devolucion = '$numero_guia';";
    $cantidad = sqlValue($query);
    if ($cantidad > 0) {
        return true;
    } else {
        return false;
    }
}

//Footer
include_once("$currDir/footer.php");