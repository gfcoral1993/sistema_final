<?php
error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', true);
header('Content-Type: text/html; charset=utf-8');
header('Content-type: application/json');
date_default_timezone_set('UTC');
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");
$query = "SELECT `id_vendedor`,concat( `nombre`,' ',`apellido`) as fullnombre FROM `vendedor` order by fullnombre asc;";
$result_1 = sql($query,$eo);
$vendedores = array();
if ($result_1->num_rows > 0) {
    while ($res = $result_1->fetch_assoc()) {
        array_push($vendedores, array(
            'id_vendedor' => $res['id_vendedor'],
            'nombre_vendedor' => mb_convert_encoding($res['fullnombre'], 'UTF-8', 'UTF-8')
        ));
    }
}
$json = json_encode($vendedores);
echo $json;