<?php
/*
 * You can add custom links in the home page by appending them here ...
 * The format for each link is:
    $homeLinks[] = array(
        'url' => 'path/to/link',
        'title' => 'Link title',
        'description' => 'Link text',
        'groups' => array('group1', 'group2'), // groups allowed to see this link, use '*' if you want to show the link to all groups
        'grid_column_classes' => '', // optional CSS classes to apply to link block. See: https://getbootstrap.com/css/#grid
        'panel_classes' => '', // optional CSS classes to apply to panel. See: https://getbootstrap.com/components/#panels
        'link_classes' => '', // optional CSS classes to apply to link. See: https://getbootstrap.com/css/#buttons
        'icon' => 'path/to/icon', // optional icon to use with the link
        'table_group' => '' // optional name of the table group you wish to add the link to. If the table group name contains non-Latin characters, you should convert them to html entities.
    );
 */

$homeLinks[] = array(
    'url' => 'hooks/calculos/recalcular.php#',
    'title' => 'Recalcular Stocks',
    'description' => 'Al entrar en este modulo se procederá a recalcular los Stocks de todos los vendedores',
    'groups' => array('Admins'),
    'grid_column_classes' => '',
    'panel_classes' => '',
    'link_classes' => '',
    'icon' => 'hooks/calculos/recursos/recalcular.png',
    'table_group' => 'INVENTARIOS'
);

$homeLinks[] = array(
    'url' => 'hooks/ajax/actualizacion_precios/actualizacion_precios_stock_general.php#',
    'title' => 'Precios Stocks',
    'description' => 'Ejecuta la actaulizacion de precios en Stock General',
    'groups' => array('Admins'),
    'grid_column_classes' => '',
    'panel_classes' => '',
    'link_classes' => '',
    'icon' => 'hooks/calculos/recursos/recalcular.png',
    'table_group' => 'INVENTARIOS'
);

