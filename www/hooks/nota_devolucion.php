<?php
//Libreria para generar pdf's
require('fpdf182/fpdf.php');
function nota_devolucion_init(&$options, $memberInfo, &$args)
{

    return TRUE;
}

function nota_devolucion_header($contentType, $memberInfo, &$args)
{
    $header = '';

    switch ($contentType) {
        case 'tableview':
            $header = '';
            break;

        case 'detailview':
            $header = '';
            break;

        case 'tableview+detailview':
            $header = '';
            break;

        case 'print-tableview':
            $header = '';
            break;

        case 'print-detailview':
            $header = '';
            break;

        case 'filters':
            $header = '';
            break;
    }

    return $header;
}

function nota_devolucion_footer($contentType, $memberInfo, &$args)
{
    $footer = '';

    switch ($contentType) {
        case 'tableview':
            $footer = '';
            break;

        case 'detailview':
            $footer = '';
            break;

        case 'tableview+detailview':
            $footer = '';
            break;

        case 'print-tableview':
            $footer = '';
            break;

        case 'print-detailview':
            $footer = '';
            break;

        case 'filters':
            $footer = '';
            break;
    }

    return $footer;
}

function nota_devolucion_before_insert(&$data, $memberInfo, &$args)
{

    return TRUE;
}

function nota_devolucion_after_insert($data, $memberInfo, &$args)
{
    $fecha_movimiento = $data['fecha'];
    $tipo_movimiento = 2;
    $numero_guia = $data['numero_guia'];
    $vendedor = $data['vendedor'];
    $id_nota_devolucion = $numero_guia;

    //Fin de actualizar stocks
    // =================================================================================================================
    //endregion

    //region CREACION DEL PDF
    $pdf = new FPDF();
    //Agrego una nueva pagina al PDF
    $pdf->AddPage();
    //Defino el tipo de letra y el tamano
    $pdf->SetFont('Arial', 'B', 10);
    //NOMBRE VENDEDOR
    $nombre_Vendedor = sqlValue("SELECT CONCAT(nombre,' ',apellido) AS full FROM vendedor WHERE id_vendedor = $vendedor;");
    $pdf->SetXY(17, 50);
    $pdf->Cell(15, 6, "VENDEDOR: $nombre_Vendedor", 0, 1);
    //CEDULA
    $cedula_Vendedor = sqlValue("SELECT cedula AS full FROM vendedor WHERE id_vendedor = $vendedor;");
    $pdf->SetXY(117, 50);
    $pdf->Cell(15, 6, "CEDULA: $cedula_Vendedor", 0, 1);
    //FECHA
    $fecha_ingreso = $data['fecha_ingreso'];
    $pdf->SetXY(17, 55);
    $pdf->Cell(15, 6, "FECHA: $fecha_ingreso", 0, 1);
    //USUARIO
    $usuario_creador = $memberInfo['custom'][0];
    $pdf->SetXY(117, 55);
    $pdf->Cell(15, 6, "USUARIO CREADOR: $usuario_creador", 0, 1);
    $numero_nota_devolucion = $numero_guia;
    $query_producto_nota_devolucion = "SELECT * FROM producto_nota_devolucion WHERE id_nota_devolucion = '$numero_nota_devolucion';";
    $res_producto_nota_devolucion = sql($query_producto_nota_devolucion, $eo);
    $y_v = 80;
    $productos_sin_serie = array();
    $pdf->SetFont('Arial', 'B', 12);
    if ($res_producto_nota_devolucion->num_rows > 0) {
        while ($res = $res_producto_nota_devolucion->fetch_assoc()) {
            //TIPO DE PRODUCTO SI MANEJA SERIES O NO
            $query_manejo_serie = "SELECT manejo_serie FROM producto WHERE id_p = '" . $res['id_producto'] . "';";
            $manejo_serie = sqlValue($query_manejo_serie);
            if ($manejo_serie == 'SI') {
                //CODIGO DE PRODUCTO
                $pdf->SetXY(18, $y_v);
                $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
                $pdf->Cell(15, 6, $codigo_producto, 0, 1);
                //DESCRIPCION id_producto
                //Cantidad cantidad
                $pdf->SetXY(43, $y_v);
                $pdf->Cell(15, 6, $res['cantidad'], 0, 1);
                //DESCRIPCION id_producto
                $pdf->SetFont('Arial', 'B', 10);
                $producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
                $pdf->SetXY(58, $y_v);
                $pdf->Cell(15, 6, substr($producto, 0, 35), 0, 1);
                $pdf->SetFont('Arial', 'B', 12);
                //NUMERO DE SERIE
                $pdf->SetXY(133, $y_v);
                $pdf->Cell(15, 6, $res['numero_serie_imei'], 0, 1);
                //VALOR
                $producto_precio = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
                $pdf->SetXY(183, $y_v); //CADA 5 EN Y
                $pdf->Cell(15, 6, $producto_precio, 0, 1);
                $y_v = $y_v + 5;
            } else {
                array_push($productos_sin_serie, $res['id_producto']);
            }
        }
        //IMPRIMIR los que no tienen serie
        //Tratamiento
        //$resultado = array_unique($productos_sin_serie);
        $resultado = $productos_sin_serie;
        $impresos = array();
        for ($u = 0; $u < count($resultado); $u++) {
            if (!in_array($resultado[$u], $impresos)) {
                $query_no_serie = "SELECT SUM(cantidad) AS cantidad_total FROM producto_nota_devolucion WHERE id_nota_devolucion = '$numero_nota_devolucion' AND id_producto=$resultado[$u];";
                $cantidad = sqlValue($query_no_serie);
                //CODIGO DE PRODUCTO
                $pdf->SetXY(18, $y_v);
                $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = '" . $resultado[$u] . "';");
                $pdf->Cell(15, 6, $codigo_producto, 0, 1);
                //DESCRIPCION id_producto
                //Cantidad cantidad
                $pdf->SetXY(43, $y_v);
                $pdf->Cell(15, 6, $cantidad, 0, 1);
                $pdf->SetFont('Arial', 'B', 10);
                //DESCRIPCION id_producto
                $producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = '" . $resultado[$u] . "';");
                $pdf->SetXY(58, $y_v);
                $pdf->Cell(15, 6, substr($producto, 0, 35), 0, 1);
                //NUMERO DE SERIE
                $pdf->SetXY(133, $y_v);
                $pdf->Cell(15, 6, '', 0, 1);
                //VALOR
                $producto_precio = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = '" . $resultado[$u] . "';");
                $pdf->SetXY(183, $y_v); //CADA 5 EN Y
                $pdf->Cell(15, 6, $producto_precio, 0, 1);
                $y_v = $y_v + 5;
                array_push($impresos, $resultado[$u]);
            }
        }
    }
    //OBSERVACIONES
    $pdf->SetXY(17, 252); //CADA 5 EN Y
    $pdf->Cell(15, 6, $data['observaciones'], 0, 1);
    //endregion

    //region GUARDADO DEL PDF EN EL SERVIDOR
    $nombre_pdf = "NOTA_DEVOLUCION_$numero_nota_devolucion";
    $filename = $nombre_pdf . ".pdf";
    $pdf->Output($filename, 'F');
    //Mover archivos
    copy($filename, 'images/notas_devolucion/' . $filename);
    unlink($filename);
    sql("UPDATE nota_devolucion SET  `archivo_nota_devolucion` = 'images/notas_devolucion/$filename' WHERE `numero_guia` = '$numero_nota_devolucion';", $eo);
    redirect('nota_devolucion_view.php');
    //endregion

    return TRUE;
}

function nota_devolucion_before_update(&$data, $memberInfo, &$args)
{

    return TRUE;
}

function nota_devolucion_after_update($data, $memberInfo, &$args)
{
    nota_devolucion_after_insert($data, $memberInfo, $args);
}

function nota_devolucion_before_delete($selectedID, &$skipChecks, $memberInfo, &$args)
{

    return TRUE;
}

function nota_devolucion_after_delete($selectedID, $memberInfo, &$args)
{

}

/**
 * @param $numero
 * @return string
 */
function getMes($numero)
{
    $mes = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"];

    $indice = (int)$numero;

    $indice = $indice - 1;

    return $mes[$indice];

}

function nota_devolucion_dv($selectedID, $memberInfo, &$html, &$args)
{
    $html .= '<div class="col-xs-12 detail_view">
    <h1 align="center">Ingrese los productos a elegir de la Nota de Devolucion</h1>
</div>
<div class="col-xs-12 detail_view">
    <div class="row">
        <input class="col-xs-8 form-control" id="barra_producto"
               placeholder="Ingrese el nombre del producto o su codigo" style="width: 65% !important;"/>
        <input class="col-xs-2 btn btn-success" id="buscar_producto" type="button" value="Buscar"/>
        <input class="col-xs-2 btn btn-danger" id="eliminar_busqueda_producto" type="button" value="Borrar"/>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
</div>
<div class="col-xs-12 detail_view">
    <h3 align="center">Productos Encontrados</h3>
    <div id="result"></div>
    <hr/>
</div>
<div class="col-xs-12 detail_view">
    <div class="table-responsive">
        <table class="table" id="tabla_productos_nota_devolucion" align="center">
            <thead class="thead-dark">
            <tr align="center">
                <th scope="col">Items</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Código</th>
                <th scope="col">Agregar a la Nota de Devolución</th>
            </tr>
            </thead>
            <tbody id="tbl_body_productos_nota_devolucion">
            <tr>
                <td class="items">No</td>
                <td class="cantidad"> se han</td>
                <td class="producto"> agregado</td>
                <td class="codigo"> Productos</td>
                <td class="accion">a la Nota de Devoluci</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <input id="btnAgregarProductos" type="button" value="Agregar a la Nota de Devolución"
           class="col-xs-2 btn btn-success"/>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <h3 align="center">Productos de la Nota de Devolucion</h3>
</div>
<div class="col-xs-12 detail_view">
    <div class="table-responsive">
        <table class="table" id="tabla_productos_nota_devolucion_final">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Items</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Codigo</th>
                <th scope="col">Numero Serie/IMEI</th>
                <th scope="col">Eliminar de la Nota de Devolución</th>
            </tr>
            </thead>
            <tbody id="tbl_body_productos_nota_devolucion_final">

            </tbody>
        </table>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <input id="btnEliminarProductosNotaDevolucion" type="button" value="Eliminar de la Nota de Devolucion"
           class="col-xs-2 btn btn-danger"/>
    <br/>
    <br/>
    <br/>
    <input id="btnSaveHijosProductoNotaDevolucion" type="button" value="Guardar Productos"
           class="col-xs-2 btn btn-success"/>
</div>';
}

function nota_devolucion_csv($query, $memberInfo, &$args)
{

    return $query;
}

function nota_devolucion_batch_actions(&$args)
{

    return [];
}
