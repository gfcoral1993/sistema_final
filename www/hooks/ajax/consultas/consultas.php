<?php

include_once '../../../lib.php';
include_once '../../../defaultLang.php';
include_once '../../../language.php';

$numero_contrato = $_POST['numero_contrato'];
$cedula = $_POST['cedula'];

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://192.168.100.173:8080/sistema_consultas/www/hooks/ajax/webservices/ws/consultaequifax.php?numero_contrato=' . $numero_contrato . '&cedula=' . $cedula,
//    CURLOPT_URL => 'http://172.16.10.162:8080/sistema_consultas/www/hooks/ajax/webservices/ws/consultaequifax.php?numero_contrato=' . $numero_contrato . '&cedula=' . $cedula,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_HTTPHEADER => array(
        'Cookie: sistema_consultas=vucj72kcmde3g20cpmdn5t6je3'
    ),
));

$response = curl_exec($curl);

$respuesta = json_decode($response, true);

curl_close($curl);

$algo = $respuesta['message'];

$persona = $respuesta["data"];
$algo = array();
$i = 0;
$geografia_id = array();

$dependencia_aprobaciones_id = array();

$aprobaciones_id = array();

foreach ($persona as $key => $value) {
    $algo[] = $value;
    if ($i == 6) {
        $dependencia_aprobaciones_id[] = $value;
    }
    if ($i >= 9 and $i < 12) {
        $geografia_id[] = $value;
    }
    if ($i == 12) {
        $aprobaciones_id[] = $value;
    }
    $i++;
}
$dependencia_laboral = sqlValue("select dp.nombre_dependencia_laboral from dependencia_laboral as dp where dp.id_dependencia_laboral = $dependencia_aprobaciones_id[0];");
$provincia_nombre = sqlValue("select p.nombre_pr from provincia as p where id_pr = $geografia_id[0];");
$canton_nombre = sqlValue("select p.nombre_c from canton as p where  id_c = $geografia_id[1];");
$parroquia_nombre = sqlValue("select p.nombre_pa from parroquia as p where id_pa = $geografia_id[2];");
$aprobaciones = sqlValue("SELECT p.`nombre_aprobaciones` FROM aprobaciones AS p WHERE p.`id_aprobaciones` = $aprobaciones_id[0];");

$algo[] = $provincia_nombre;
$algo[] = $canton_nombre;
$algo[] = $parroquia_nombre;
$algo[] = $dependencia_laboral;
$algo[] = $aprobaciones;

echo json_encode($algo);