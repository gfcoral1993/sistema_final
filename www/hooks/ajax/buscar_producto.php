<?php
/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2019-01-24
 * Time: 12:20
 */
include("../../defaultLang.php");
include("../../language.php");
include("../../lib.php");

$texto = $_POST['query'];

$query = "SELECT * FROM producto WHERE nombre_p like '%" . $texto . "%' OR codigo_p like '%" . $texto . "%';";

$res = sql($query, $eo);

$productos = array();
//$pvp_producto = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = $id_producto;");
if ($res->num_rows > 0) {
    while ($respuesta = $res->fetch_assoc()) {
        $productos[] = array(
            "id_p" => $respuesta['id_p'],
            "nombre_p" => $respuesta['nombre_p'],
            "codigo_p" => $respuesta['codigo_p'],
            "tipo_producto_p" => $respuesta['tipo_producto_p'],
            "pvp_p" => $respuesta['pvp_producto']
        );
    }
} else {
    $productos = array(
        "id_p" => "No existe el producto",
        "nombre_p" => "No existe el producto",
        "codigo_p" => "No existe el producto",
        "tipo_producto_p" => "No existe el producto",
        "pvp_p" => "No existe el producto"
    );
}

$html = "";

foreach ($productos as $producto) {
    $html .= "<tr id='" . $producto['id_p'] . "'>
                <td>Nuevo Item</td>
                <td><input type=\"number\" min=\"1\" max=\"1000\" value='1' /></td>
                <td>" . $producto['nombre_p'] . "</td>
                <td>" . $producto['codigo_p'] . "</td>
                <td>" . $producto['pvp_p'] . " $</td>
                <td><input id='" . $producto['id_p'] . "' type='checkbox' style='color: green;'/></td>
             </tr>";
}

$array = array($html);

echo json_encode($array);