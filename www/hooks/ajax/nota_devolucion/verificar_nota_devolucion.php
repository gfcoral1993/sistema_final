<?php

/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2019-01-24
 * Time: 12:20
 */
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");

$numero_guia = $_POST['numero_guia'];
$query = "SELECT COUNT(1) as verificacion FROM nota_devolucion AS np WHERE np.`numero_guia`= '$numero_guia';";
$res = sql($query, $eo);
$verificador = 0;

while ($respuesta = $res->fetch_assoc()) {
    $verificador = $respuesta['verificacion'];
}

echo json_encode($verificador);