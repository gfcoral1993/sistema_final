<?php
/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2020-08-20
 * Time: 12:20
 */
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");
//Obtengo un array de todos los productos a validar en el stock
$productosAvalidar = $_POST['productosAvalidar'];
$vendedor = $_POST['vendedor'];
$mes = $_POST['mes'];
$productosAprobados = array();
$productosNegadosPorFaltaStock = array();
$productosNoPedidos = array();
//SELECT cantidad FROM stock_general WHERE codigo_producto = 629 AND mes = 'AGOSTO' AND vendedor = 14;
foreach ($productosAvalidar as $producto) {
    $query = "SELECT p.codigo_p AS codigo,p.nombre_p AS nombre,sg.cantidad FROM stock_general AS sg JOIN producto AS p ON p.id_p = sg.`id_producto` WHERE sg.codigo_producto = " . $producto['id_producto'] . " AND sg.mes = '$mes' AND sg.vendedor = $vendedor;";
    $res = sql($query, $eo);
    if ($res->num_rows > 0) {
        while ($r = $res->fetch_assoc()) {
            $codigo_validar = $r['codigo'];
            $nombre_validar = $r['nombre'];
            $cantidad_validar = $r['cantidad'];
            //Si tiene el producto y esta dentro de las cantidades de su stock
            if ($producto['cantidad'] <= $r['cantidad']) {
                $productosAprobados[] = array('codigo' => $r['codigo'], 'cantidad' => $r['cantidad'], 'nombre' => $r['nombre']);
            }
            //Si tiene el producto pero no tiene su ficiente cantidad
            if ($producto['cantidad'] > $r['cantidad']) {
                $productosNegadosPorFaltaStock[] = array('codigo' => $r['codigo'], 'cantidad' => $r['cantidad'], 'nombre' => $r['nombre']);
            }
        }
    } else {
        $nombre = sqlValue("SELECT nombre_p FROM producto WHERE id_p = " . $producto['id_producto'] . ";");
        $codigo = sqlValue("SELECT codigo_p FROM producto WHERE id_p = " . $producto['id_producto'] . ";");
        $productosNoPedidos[] = array('codigo' => "$codigo", 'cantidad' => 0, 'nombre' => "$nombre");
    }
}
$array = array($productosAprobados, $productosNegadosPorFaltaStock, $productosNoPedidos);
echo json_encode($array);