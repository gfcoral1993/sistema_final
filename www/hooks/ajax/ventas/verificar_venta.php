<?php

/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2019-01-24
 * Time: 12:20
 */
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");

$cedula = $_POST['cedula'];

$query = "SELECT * FROM ventas WHERE cedula = '$cedula';";

$res = sql($query, $eo);

$contratos = array();

if ($res->num_rows > 0) {
    while ($respuesta = $res->fetch_assoc()) {
        $contratos[] = array(
            "id_co" => $respuesta['id'],
            "numero_contrato" => $respuesta['numero_contrato'],
            "fecha_creacion_contrato" => $respuesta['fecha'],
            "valor_cuota_contrato" => $respuesta['valor_cuota']
        );
    }
    $html = "";

    $html .= "<b>SI</b> tiene contrato(s) y son:<br />";
    foreach ($contratos as $contrato) {
        $html .= "<p>Número de Contrato: " . $contrato['numero_contrato'] . "<br/>Fecha de Contrato: " . $contrato['fecha_creacion_contrato'] . "<br/>Valor de la Cuota de Contrato: $" . $contrato['valor_cuota_contrato'] . "</p><hr /><br/>";
    }

    $array = array($html);

    echo json_encode($array);

} else {

    $array = "";

    echo json_encode($array);
}