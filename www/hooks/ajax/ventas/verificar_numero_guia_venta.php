<?php

/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2020-08-23
 * Time: 12:20
 */
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");

$numero_contrato = $_POST['numero_contrato'];
$query = "SELECT COUNT(1) AS verificacion FROM ventas AS v WHERE v.`numero_contrato` = '$numero_contrato';";
$res = sql($query, $eo);
$verificador = 0;

while ($respuesta = $res->fetch_assoc()) {
    $verificador = $respuesta['verificacion'];
}

echo json_encode($verificador);