<?php
/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2020-08-06
 * Time: 12:20
 */
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");

//Numero de Contrato Unico en Teoria por cada venta
$var_externa = $_POST['numero_venta'];

$numero_venta = $var_externa;

//borrar productos de la nota de pedido
$query_delete = "DELETE FROM producto_venta WHERE id_venta = '$numero_venta';";
$res = sql($query_delete, $eo);

//Lista de productos que iran en este contrato
$productos = $_POST['productos'];

$query_insert = "INSERT INTO `producto_venta` (`id_venta`,`id_producto`,`cantidad`) VALUES ";

$query_values = "";

foreach ($productos as $producto) {
    $query_values .= "('$numero_venta','" . $producto['id_producto'] . "','" . $producto['cantidad'] . "'),";
}

$query_values_final = rtrim($query_values, ",");

$query_values_final = $query_values_final . ';';

$query = $query_insert . $query_values_final;

$res = sql($query, $eo);

$array = array($res);

echo json_encode($array);