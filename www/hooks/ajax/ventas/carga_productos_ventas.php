<?php
/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2020-05-11
 * Time: 12:20
 */
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");

$numero_venta = $_POST['numero_venta'];

$query = "SELECT * FROM `producto_venta` WHERE id_venta = '$numero_venta';";

$res = sql($query, $eo);

$productos = array();

if ($res->num_rows > 0) {
    while ($respuesta = $res->fetch_assoc()) {
        $id_producto = $respuesta["id_producto"];
        $nombre_producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = $id_producto;");
        $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = $id_producto;");
        $cantidad_producto = $respuesta["cantidad"];
        $pvp_producto = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = $id_producto;");
        $productos[] = array(
            "id_p" => $id_producto,
            "nombre_p" => $nombre_producto,
            "cantidad_pc" => $cantidad_producto,
            "numero_venta" => $numero_venta,
            "codigo_p" => $codigo_producto,
            "pvp_p" => $pvp_producto
        );
    }
}

$html = "";

$i = 0;
foreach ($productos as $producto) {
    $i++;
    $html .= "<tr id='" . $producto['id_p'] . "'>
                <td>$i .-</td>
                <td><input type=\"number\" min=\"1\" max=\"1000\" value='" . $producto["cantidad_pc"] . "' /></td>
                <td>" . $producto['nombre_p'] . "</td>
                <td>" . $producto['codigo_p'] . "</td>
                <td>" . $producto['pvp_p'] . " $ </td>
                <td><input id='" . $producto['id_p'] . "' type='checkbox' style='color: green;'/></td>
             </tr>";
}

$array = array($html);

echo json_encode($array);