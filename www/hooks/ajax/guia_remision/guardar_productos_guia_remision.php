<?php
/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2020-08-06
 * Time: 12:20
 */
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");

$vendedor_id = $_POST['vendedor'];
$numero_nota_pedido = $_POST['numero_nota_pedido'];
$fecha_total_nota_pedido = sqlValue("SELECT fecha FROM nota_pedido WHERE numero_guia = '00015';");
$numMes = date("m", strtotime($fecha_total_nota_pedido));
$meses = array("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");
$mes = $meses[(int)$numMes - 1];

//Numero de Contrato Unico en Teoria por cada cliente y vendedor
$var_externa = $_POST['numero_guia_remision'];

$numero_guia_remision = $var_externa;

//borrar productos de la nota de pedido
$query_delete = "DELETE FROM producto_guia_remision WHERE id_guia_remision = '" . $numero_guia_remision . "';";
$res = sql($query_delete, $eo);

//Lista de productos que iran en este contrato
$productos = $_POST['productos'];

//$query_insert = "INSERT INTO `producto_guia_remision` (`id_guia_remision`,`id_producto`,`cantidad`,`numero_serie_imei`) VALUES ";
$query_insert = "INSERT INTO producto_guia_remision(`id_guia_remision`,`id_producto`,`codigo_producto`,`cantidad`,`vendedor`,`mes`,`numero_serie_imei`) VALUES ";

$query_values = "";

foreach ($productos as $producto) {
    $codigo_producto = sqlValue("SELECT `codigo_p` FROM `producto` WHERE id_p = " . $producto['id_producto'] . ";");
//    $query_values .= "('$numero_guia_remision','" . $producto['id_producto'] . "','" . $producto['cantidad'] . "','" . $producto['numero_serie_imei'] . "'),";
    $query_values .= "('$numero_guia_remision','" . $producto['id_producto'] . "','" . $codigo_producto . "','" . $producto['cantidad'] . "','" . $vendedor_id . "','" . $mes . "','" . $producto['numero_serie_imei'] . "'),";
}

$query_values_final = rtrim($query_values, ",");

$query_values_final = $query_values_final . ';';

$query = $query_insert . $query_values_final;

$res = sql($query, $eo);

$array = array($res);

echo json_encode($array);