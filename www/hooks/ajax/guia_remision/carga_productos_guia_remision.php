<?php
/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2020-09-18
 * Time: 12:20
 */
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");

$numero_nota_pedido = $_POST['numero_nota_pedido'];
$numero_guia_remision = $_POST['numero_guia_remision'];

$query = "SELECT * FROM `producto_nota_pedido` WHERE id_nota_pedido = '$numero_nota_pedido';";

$res = sql($query, $eo);

$productos = array();

if ($res->num_rows > 0) {
    while ($respuesta = $res->fetch_assoc()) {
        $id_producto = $respuesta["id_producto"];
        $nombre_producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = $id_producto;");
        $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = $id_producto;");
        $numero_serie_imei = sqlValue("SELECT numero_serie_imei FROM producto_guia_remision WHERE id_guia_remision = $numero_guia_remision AND id_producto = $id_producto");
        $cantidad_producto = $respuesta["cantidad"];
        $productos[] = array(
            "id_p" => $id_producto,
            "nombre_p" => $nombre_producto,
            "cantidad_pc" => $cantidad_producto,
            "numero_nota_pedido" => $numero_nota_pedido,
            "codigo_p" => $codigo_producto,
            "numero_serie_imei" => $numero_serie_imei);
    }
}

$html = "";

$i = 0;
foreach ($productos as $producto) {
    $i++;

    $cantidad = $producto["cantidad_pc"];

    for ($j = 0; $j < $cantidad; $j++) {
        $html .= "<tr id='" . $producto['id_p'] . "'>
                <td>$i .-</td>
                <td>" . $producto['codigo_p'] . "</td>
                <td>" . $producto['nombre_p'] . "</td>
                <td>
                    <input id='numero_serie_imei' name='numero_serie_imei' type=\"text\" value='" . $producto["numero_serie_imei"] . "'/>
                </td>
                <td>
                    <input id='cantidad' name='cantidad' disabled='disabled' type=\"number\" min=\"1\" max=\"1000\" value='1' />
                </td>
                <td style='display: none !important;'><input id='" . $producto['id_p'] . "' type='checkbox' style='color: green;'/></td>
             </tr>";
    }

}

$array = array($html);

echo json_encode($array);