<?php
/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Creation Date: 2020-08-19
 * Review Date: 2021-11-08
 * Time: 17:30
 */
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");

$numero_nota_pedido = $_POST['numero_nota_pedido'];

$query = "SELECT * FROM `producto_nota_pedido` WHERE id_nota_pedido = '$numero_nota_pedido';";

$res = sql($query, $eo);

$productos = array();

if ($res->num_rows > 0) {
    while ($respuesta = $res->fetch_assoc()) {
        $id_producto = $respuesta["id_producto"];
        $nombre_producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = $id_producto;");
        $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = $id_producto;");
        $cantidad_producto = $respuesta["cantidad"];
        $pvp_producto = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = $id_producto;");
        $productos[] = array(
            "id_p" => $id_producto,
            "nombre_p" => $nombre_producto,
            "cantidad_pc" => $cantidad_producto,
            "numero_nota_pedido" => $numero_nota_pedido,
            "codigo_p" => $codigo_producto,
            "pvp_p" => $pvp_producto);
    }
}

$html = "";

$i = 0;
foreach ($productos as $producto) {
    $i++;
    $html .= "<tr id='" . $producto['id_p'] . "'>
                <td>$i .-</td>
                <td><input type=\"number\" min=\"1\" max=\"1000\" value='" . $producto["cantidad_pc"] . "' /></td>
                <td>" . $producto['nombre_p'] . "</td>
                <td>" . $producto['codigo_p'] . "</td>
                <td>" . $producto['pvp_p'] . " $ </td>
                <td><input id='" . $producto['id_p'] . "' type='checkbox' style='color: green;'/></td>
             </tr>";
}

$array = array($html);

echo json_encode($array);