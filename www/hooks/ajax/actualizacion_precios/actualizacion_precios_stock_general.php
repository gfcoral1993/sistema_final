<head>
    <title>Actualizacion de Precios</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <style type="text/css">
    </style>
    <script type='text/javascript' src='../../resources/jquery/js/jquery-1.11.2.min.js'></script>
</head>
<?php
/**
 * Created by PhpStorm.
 * User: franciscocoral
 * Date: 2020-07-09
 * Time: 17:05
 */
define('PREPEND_PATH', '../../../');
include("../../../defaultLang.php");
include("../../../language.php");
include("../../../lib.php");
$hooks_dir = dirname(__FILE__);
include_once("../../../header.php");
/* grant access to the groups 'Admins' and 'Data entry' */
$mi = getMemberInfo();
if (!in_array($mi['group'], array('Admins', 'Profesores'))) {
    echo "<br /><br /><br />";
    echo "<div style=\"text-align: center\">
                <img class='img-responsive' src=\"../../../hooks/resources/acceso_denegado.png\" align=\"middle\" width='200%' height='100%'>
          </div>";
    exit;
}
$query_productos_stock_general = "SELECT * FROM stock_general;";
$res_productos_stock_general = sql($query_productos_stock_general, $eo);
if ($res_productos_stock_general->num_rows > 0) {
    while ($res = $res_productos_stock_general->fetch_assoc()) {
        $id = $res['id'];
        $id_actualizar_producto = $res['id_producto'];
        $valor = sqlValue("SELECT p.`pvp_producto` FROM producto AS p WHERE p.`id_p` = $id_actualizar_producto;");
        //Update
        $query_update = "UPDATE stock_general SET pvp = $valor WHERE id = $id;";
        sql($query_update, $eo);
    }
    echo "<h3>Se actualizo correctamente!</h3>";
} else {
    echo "<h3>Existio un error en la consulta de los valores de la actualizacion!</h3>";
}
//Footer
include_once("$currDir/footer.php");