<?php
// For help on using hooks, please refer to https://bigprof.com/appgini/help/working-with-generated-web-database-application/hooks

function ventas_init(&$options, $memberInfo, &$args)
{

    return TRUE;
}

function ventas_header($contentType, $memberInfo, &$args)
{
    $header = '';

    switch ($contentType) {
        case 'tableview':
            $header = '';
            break;

        case 'detailview':
            $header = '';
            break;

        case 'tableview+detailview':
            $header = '';
            break;

        case 'print-tableview':
            $header = '';
            break;

        case 'print-detailview':
            $header = '';
            break;

        case 'filters':
            $header = '';
            break;
    }

    return $header;
}

function ventas_footer($contentType, $memberInfo, &$args)
{
    $footer = '';

    switch ($contentType) {
        case 'tableview':
            $footer = '';
            break;

        case 'detailview':
            $footer = '';
            break;

        case 'tableview+detailview':
            $footer = '';
            break;

        case 'print-tableview':
            $footer = '';
            break;

        case 'print-detailview':
            $footer = '';
            break;

        case 'filters':
            $footer = '';
            break;
    }

    return $footer;
}

function ventas_before_insert(&$data, $memberInfo, &$args)
{

    return TRUE;
}

function ventas_after_insert($data, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $numero
 * @return string
 */
function getMes($numero)
{
    $mes = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"];

    $indice = (int)$numero;

    $indice = $indice - 1;

    return $mes[$indice];

}

function ventas_before_update(&$data, $memberInfo, &$args)
{

    return TRUE;
}

function ventas_after_update($data, $memberInfo, &$args)
{

    return TRUE;
}

function ventas_before_delete($selectedID, &$skipChecks, $memberInfo, &$args)
{

    return TRUE;
}

function ventas_after_delete($selectedID, $memberInfo, &$args)
{

}

function ventas_dv($selectedID, $memberInfo, &$html, &$args)
{
    $html .= '<div class="col-xs-12 detail_view">
    <h1 align="center">Ingrese los productos a elegir de la Venta</h1>
</div>
<div class="col-xs-12 detail_view">
    <div class="row">
        <input class="col-xs-8 form-control" id="barra_producto"
               placeholder="Ingrese el nombre del producto o su codigo" style="width: 65% !important;"/>
        <input class="col-xs-2 btn btn-success" id="buscar_producto" type="button" value="Buscar"/>
        <input class="col-xs-2 btn btn-danger" id="eliminar_busqueda_producto" type="button" value="Borrar"/>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
</div>
<div class="col-xs-12 detail_view">
    <h3 align="center">Productos Encontrados</h3>
    <div id="result"></div>
    <hr/>
</div>
<div class="col-xs-12 detail_view">
    <div class="table-responsive">
        <table class="table" id="tabla_productos_venta" align="center">
            <thead class="thead-dark">
            <tr align="center">
                <th scope="col">Items</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Código</th>
                <th scope="col">PVP</th>
                <th scope="col">Agregar a la Venta</th>
            </tr>
            </thead>
            <tbody id="tbl_body_productos_venta">
            <tr>
                <td class="items">No</td>
                <td class="cantidad"> se han</td>
                <td class="producto"> agregado</td>
                <td class="codigo"> Productos</td>
                <td class="accion">a la Venta</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <input id="btnAgregarProductos" type="button" value="Agregar a la Venta" class="col-xs-2 btn btn-success"/>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <h3 align="center">Productos de la Venta</h3>
</div>
<div class="col-xs-12 detail_view">
    <div class="table-responsive">
        <table class="table" id="tabla_productos_venta_final">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Items</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Codigo</th>
                <th scope="col">PVP</th>
                <th scope="col">Eliminar de la Venta</th>
            </tr>
            </thead>
            <tbody id="tbl_body_productos_venta_final">

            </tbody>
        </table>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <input id="btnEliminarProductosVenta" type="button" value="Eliminar de la Venta"
           class="col-xs-2 btn btn-danger"/>
    <br/>
    <br/>
    <br/>
    <input id="btnSaveHijosProductoVenta" type="button" value="Guardar Productos"
           class="col-xs-2 btn btn-success"/>
</div>';
}

function ventas_csv($query, $memberInfo, &$args)
{

    return $query;
}

function ventas_batch_actions(&$args)
{

    return [];
}
