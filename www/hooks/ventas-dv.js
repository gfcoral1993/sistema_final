$j(function () {
    //region FUNCIONAL OK
    console.log('ventas-dv.js');
    var numero_venta_global = $j("#numero_contrato").val();
    if (numero_venta_global !== '') {
        console.log('id global de nota de pedido: ' + numero_venta_global);
        $j.ajax({
            url: "hooks/ajax/ventas/carga_productos_ventas.php",
            method: "post",
            dataType: 'json',
            cache: false,
            data: {
                numero_venta: numero_venta_global,
            },
            success: function (data) {
                //console.log(data);
                //Lleno los productos que existen en esa Nota de Pedido
                $j('#tbl_body_productos_venta_final').html("<h4>Cargando.....</h4>");
                $j('#tbl_body_productos_venta_final').html(data[0]);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // console.log(xhr.status);
                console.log(thrownError);
                // var response = JSON.parse(xhr.responseText);
                // console.log(response);
            }
        });
    } else {
        numero_venta_global = 'new nota pedido';
    }
    //Envia el producto a buscar en la base de datos
    $j("#buscar_producto").on('click', function () {
        var texto = $j('#barra_producto').val();
        if (texto != '') {
            cargarBusquedaProductos(texto);
        } else {
            cargarBusquedaProductos();
        }
    });
    //Funcion para agregar los productos a la tabla
    $j("#btnAgregarProductos").click(function () {
        // var $productosSeleccionados = $j("#tabla_productos_venta input[type='checkbox']:checked").closest("tr");
        // $productosSeleccionados.clone().appendTo('#tbl_body_productos_venta_final');
        // return false;
        //De inicio no agregamos nada de nada
        var bandera_aprobados = false;
        var bandera_negado_por_stock = false;
        var bandera_negado_por_no_pedir = false;
        //Vendedor
        var vendedor = $j("#vendedor").val();
        //console.log('El vendedor es: ' + vendedor);
        //Mes
        var mes = $j("#fecha-mm option:selected").text().toUpperCase();
        //console.log('El mes de la venta es: ' + mes);
        //Arrays
        var productosAvalidar = [];
        //console.log('Existen: ' + productosAvalidar.length + " Productos a validar");
        var productosAprobados = [];
        //console.log('Existen: ' + productosAprobados.length + " Productos a aprobados");
        var productosNegadosPorFaltaStock = [];
        //console.log('Existen: ' + productosNegadosPorFaltaStock.length + " Productos negados por falta de stock");
        var productosNoPedidos = [];
        //console.log('Existen: ' + productosNoPedidos.length + " Productos no pedidos");

        //Recorro los productos que tengo o quiero agregar a la venta
        var productosPrevios = [];
        $j('#tabla_productos_venta input:checkbox:checked').map(function () {
            var id = $j(this).attr("id");
            productosPrevios.push(id);
        });
        //FIltro los existentes con check unicamente
        $j('#tabla_productos_venta > tbody  > tr').each(function () {
            var id = $j(this).attr("id");
            if (productosPrevios.includes(id)) {
                var cantidad = $j(this).find("input").val();
                var producto = {};
                producto.id_producto = id;
                producto.cantidad = cantidad;
                productosAvalidar.push(producto);
            }

        });
        console.log('Existen: ' + productosAvalidar.length + " Productos a validar");
        console.log(productosAvalidar);

        console.log(vendedor);
        console.log(mes);
        //Entro a validacion con Ajax
        $j.ajax({
            url: "hooks/ajax/ventas/validacion_productos.php",
            method: "post",
            dataType: 'json',
            cache: false,
            data: {
                vendedor: vendedor,
                mes: mes,
                productosAvalidar: productosAvalidar
            },
            success: function (data) {

                productosAprobados = data[0];
                productosNegadosPorFaltaStock = data[1];
                productosNoPedidos = data[2];

                let a = productosAprobados.length;
                let b = productosNegadosPorFaltaStock.length;
                let c = productosNoPedidos.length;

                if (a > 0) {
                    bandera_aprobados = true;
                }
                if (b > 0) {
                    bandera_negado_por_stock = true;
                    bandera_aprobados = false;
                }

                if (c > 0) {
                    bandera_negado_por_no_pedir = true;
                    bandera_aprobados = false;
                }

                if (bandera_negado_por_stock || bandera_negado_por_no_pedir) {

                    var html = "";
                    let b = productosNegadosPorFaltaStock.length;
                    let c = productosNoPedidos.length;

                    var falta_stock = "";
                    if (b > 0) {
                        for (var i = 0; i < b; i++) {
                            let codigo = productosNegadosPorFaltaStock[i]['codigo'];
                            let nombre = productosNegadosPorFaltaStock[i]['nombre'];
                            falta_stock += "<p>CODIGO: " + codigo + " NOMBRE: " + nombre + "</p><br />";
                        }
                        html += "<h4>EXISTEN PRODUCTOS QUE NO TIENEN EL STOCK SUFICIENTE</h4>";
                        html += falta_stock;
                    }
                    var no_pedidos = "";
                    if (c > 0) {
                        for (var j = 0; j < c; j++) {
                            let codigo = productosNoPedidos[j]['codigo'];
                            let nombre = productosNoPedidos[j]['nombre'];
                            no_pedidos += "<p>CODIGO: " + codigo + " NOMBRE: " + nombre + "</p><br />";
                        }
                        html += "<h4>EXISTEN PRODUCTOS QUE EL VENDEDOR NO TIENE EN SU INVENTARIO</h4>";
                        html += no_pedidos;
                    }
                    $j(function () {
                        modal_window({
                            title: "MENSAJE INFORMATIVO", message: html, button: true
                        });
                    });
                    return false;
                } else if (bandera_aprobados) {
                    var $productosSeleccionados = $j("#tabla_productos_venta input[type='checkbox']:checked").closest("tr");
                    $productosSeleccionados.clone().appendTo('#tbl_body_productos_venta_final');
                    return false;
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                // console.log(xhr.status);
                console.log(thrownError);
                // var response = JSON.parse(xhr.responseText);
                // console.log(response);
            }
        });
    });

    //Funcion para eliminar los productos a la tabla
    $j("#btnEliminarProductosVenta").click(function () {
        var $productosSeleccionados = $j("#tbl_body_productos_venta_final input[type='checkbox']:checked").closest("tr");
        $productosSeleccionados.detach();
        return false;
    });

    //Accion al momento de actualizar
    $j("#update").on('click', function () {
        var numero_venta = $j('#numero_contrato').val();
        //Productos que iran en el Contrato
        var miContrato = [];
        //Obtener todos los productos del Contrato
        $j('#tabla_productos_venta_final > tbody  > tr').each(function () {
            var id = $j(this).attr("id");
            var cantidad = $j(this).find("input").val();
            var producto = {};
            producto.id_producto = id;
            producto.cantidad = cantidad;
            miContrato.push(producto);
        });
        if (numero_venta !== '') {
            $j.ajax({
                url: "hooks/ajax/ventas/guardar_productos_ventas.php",
                method: "post",
                dataType: 'json',
                cache: false,
                data: {
                    numero_venta: numero_venta,
                    productos: miContrato
                },
                success: function (data) {
                    //console.log(data);
                }
            });
        }
    });

    //Boton borrar de la barra de busqueda de producto en el contrato
    $j("#eliminar_busqueda_producto").on('click', function () {
        $j("#barra_producto").val('');
        $j('#tbl_body_productos_venta').html("<h4>Busque un Producto..</h4>");
        $j('#tbl_body_productos_venta').html("<h4>Busque un Producto..</h4>");
    });

    //Funcionalidad de la carga de los productos encontrados a la tabla
    function cargarBusquedaProductos(query) {
        $j.ajax({
            url: "hooks/ajax/buscar_producto.php",
            method: "post",
            dataType: 'json',
            cache: false,
            data: {
                query: query
            },
            success: function (data) {
                $j('#tbl_body_productos_venta').html("<h4>Cargando.....</h4>");
                $j('#tbl_body_productos_venta').html(data[0]);
            }
        });
    }

    //Validaciones
    //Validar numero_contrato | solo numeros del [0-9]
    $j("#numero_contrato").on("keypress keyup blur", function (event) {
        $j(this).val($j(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    function validarCedula(cedula) {
        var bandera = 0;
        var cad = cedula;
        var total = 0;
        var longitud = cad.length;
        var longcheck = longitud - 1;
        if (cad !== "" && longitud === 10) {
            for (i = 0; i < longcheck; i++) {
                if (i % 2 === 0) {
                    var aux = cad.charAt(i) * 2;
                    if (aux > 9) aux -= 9;
                    total += aux;
                } else {
                    total += parseInt(cad.charAt(i)); // parseInt o concatenará en lugar de sumar
                }
            }
            total = total % 10 ? 10 - total % 10 : 0;
            if (cad.charAt(longitud - 1) == total) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    $j("#cedula").blur(function () {
        var cedula = $j("#cedula").val();
        if (validarCedula(cedula)) {
            $j.ajax({
                url: "hooks/ajax/ventas/verificar_venta.php",
                method: "post",
                dataType: 'json',
                cache: false,
                data: {
                    cedula: cedula
                },
                success: function (data) {
                    if (data.length > 0) {
                        $j(function () {
                            modal_window({
                                title: "Verificación de Cliente", message: data[0], button: true
                            });
                        });
                    } else {
                        $j(function () {
                            let numero_contrato_v = $j('#numero_contrato').val();
                            //Consumiendo el webservice de consultas
                            $j.ajax({
                                url: "hooks/ajax/consultas/consultas.php",
                                method: "post",
                                dataType: 'json',
                                cache: false,
                                data: {
                                    cedula: cedula,
                                    numero_contrato: numero_contrato_v
                                },
                                success: function (data) {
                                    //nombre
                                    $j('#nombre').val(data[3]);
                                    //mes_comision_venta
                                    $j('#s2id_mes_comision_venta .select2-chosen').text(data[4]);
                                    $j('#mes_comision_venta').val(data[4]);
                                    //valor contrato
                                    $j('#valor_contrato').val(data[5]);
                                    // //dependencia_laboral
                                    $j('#dependencia_laboral').val(data[6]);
                                    $j('#dependencia_laboral-container').select2('data', {id: data[6], text: data[16]});
                                    // aprobaciones
                                    $j('#aprobaciones').val(data[6]);
                                    $j('#aprobaciones-container').select2('data', {id: data[12], text: data[17]});
                                    //numero de cuotas
                                    $j('#cuotas').val(data[7]);
                                    //valor de la cuota
                                    $j('#valor_cuota').val(data[8]);
                                    // //provincia
                                    $j('#provincia').val(data[10]);
                                    $j('#provincia-container').select2('data', {id: data[10], text: data[13]});
                                    // // //canton
                                    $j('#canton').val(data[11]);
                                    $j('#canton-container').select2('data', {id: data[11], text: data[14]});
                                    // // //parroquia
                                    $j('#parroquia').val(data[12]);
                                    $j('#parroquia-container').select2('data', {id: data[12], text: data[15]});

                                }, error: function (xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.responseText);
                                }
                            });
                        });
                    }
                }
            });
            return true;
        } else {
            alert('Ingrese una cedula valida');
            $j("#cedula").val('')
            return false;
        }
    });

    //Validar Nombre | texto con espacios
    $j("#nombre").keypress(function (key) {
        if ((key.charCode < 97 || key.charCode > 122)//letras mayusculas
            && (key.charCode < 65 || key.charCode > 90) //letras minusculas
            && (key.charCode != 45) //retroceso
            && (key.charCode != 241) //ñ
            && (key.charCode != 209) //Ñ
            && (key.charCode != 32) //espacio
            && (key.charCode != 225) //á
            && (key.charCode != 233) //é
            && (key.charCode != 237) //í
            && (key.charCode != 243) //ó
            && (key.charCode != 250) //ú
            && (key.charCode != 193) //Á
            && (key.charCode != 201) //É
            && (key.charCode != 205) //Í
            && (key.charCode != 211) //Ó
            && (key.charCode != 218) //Ú
        ) {
            return false;
        }
    });

    //Validar valor_total  | decimales con punto
    $j("#valor_contrato").on("keypress keyup blur", function (event) {
        jQuery(this).val(jQuery(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || jQuery(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $j("#pvp").on("keypress keyup blur", function (event) {
        jQuery(this).val(jQuery(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || jQuery(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $j("#cuotas").on("keypress keyup blur", function (event) {
        jQuery(this).val(jQuery(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || jQuery(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $j("#valor_cuota").on("keypress keyup blur", function (event) {
        jQuery(this).val(jQuery(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || jQuery(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $j("#cantidad_productos").on("keypress keyup blur", function (event) {
        jQuery(this).val(jQuery(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || jQuery(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    //Validacion del numero de contrato
    $j("#numero_contrato").blur(function () {
        if (!$j('#delete').length) {
            var numero_contrato = $j("#numero_contrato").val();
            $j.ajax({
                url: "hooks/ajax/ventas/verificar_numero_guia_venta.php",
                method: "post",
                dataType: 'json',
                cache: false,
                data: {
                    numero_contrato: numero_contrato
                },
                success: function (data) {
                    if (data == 1) {
                        $j(function () {
                            modal_window({
                                title: "Verificación de Ventas",
                                message: "Ya existe una VENTA con el numero de CONTRATO ACTUAL",
                                button: true
                            });
                        });
                        $j("#numero_contrato").val('');
                        return false;
                    } else {
                        return true;
                    }
                }
            });
        }
    });

    $j('#btnSaveHijosProductoVenta').on('click', function () {
        var rowCount = $j('#tbl_body_productos_venta_final tr').length;
        console.log('Hay en la tabla ' + rowCount + ' Productos');
        $j('#cantidad_productos').val(rowCount);

        var numero_venta = $j('#numero_contrato').val();
        //Productos que iran en la Nota de Pedido
        var miContrato = [];
        //Obtener todos los productos de la Nota de Pedido
        $j('#tabla_productos_venta_final > tbody  > tr').each(function () {
            var id = $j(this).attr("id");
            var cantidad = $j(this).find("input").val();
            var producto = {};
            producto.id_producto = id;
            producto.cantidad = cantidad;
            miContrato.push(producto);
        });
        if (numero_venta !== '') {
            $j.ajax({
                url: "hooks/ajax/ventas/guardar_productos_ventas.php",
                method: "post",
                dataType: 'json',
                cache: false,
                data: {
                    numero_venta: numero_venta,
                    productos: miContrato
                },
                success: function (data) {
                    //console.log(data);
                    $j(function () {
                        modal_window({
                            title: "Mensaje Informativo",
                            message: "Se guardo exitosamente los productos para la VENTA por favor presione GRABAR",
                            button: true
                        });
                    });
                }, error: function (xhr, ajaxOptions, thrownError) {
                    $j(function () {
                        modal_window({
                            title: "Error de Datos",
                            message: "No se guardo los Productos para la VENTA, intente de nuevo",
                            button: true
                        });
                    });
                }
            });
        } else {
            $j(function () {
                modal_window({
                    title: "Error de Datos",
                    message: "Ingrese el NUMERO DE CONTRATO en para la Nota de VENTA",
                    button: true
                });
            });
        }
    });
    //endregion
});