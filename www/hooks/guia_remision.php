<?php
//Libreria para generar pdf's
require('fpdf182/fpdf.php');
/**
 * @param $options
 * @param $memberInfo
 * @param $args
 * @return bool
 */
function guia_remision_init(&$options, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $contentType
 * @param $memberInfo
 * @param $args
 * @return string
 */
function guia_remision_header($contentType, $memberInfo, &$args)
{
    $header = '';

    switch ($contentType) {
        case 'tableview':
            $header = '';
            break;

        case 'detailview':
            $header = '';
            break;

        case 'tableview+detailview':
            $header = '';
            break;

        case 'print-tableview':
            $header = '';
            break;

        case 'print-detailview':
            $header = '';
            break;

        case 'filters':
            $header = '';
            break;
    }

    return $header;
}

/**
 * @param $contentType
 * @param $memberInfo
 * @param $args
 * @return string
 */
function guia_remision_footer($contentType, $memberInfo, &$args)
{
    $footer = '';

    switch ($contentType) {
        case 'tableview':
            $footer = '';
            break;

        case 'detailview':
            $footer = '';
            break;

        case 'tableview+detailview':
            $footer = '';
            break;

        case 'print-tableview':
            $footer = '';
            break;

        case 'print-detailview':
            $footer = '';
            break;

        case 'filters':
            $footer = '';
            break;
    }

    return $footer;
}

/**
 * @param $data
 * @param $memberInfo
 * @param $args
 * @return bool
 */
function guia_remision_before_insert(&$data, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $data
 * @param $memberInfo
 * @param $args
 * @return bool
 * Función que se ejecuta despues de grabar la GUIA DE REMISION
 */
function guia_remision_after_insert($data, $memberInfo, &$args)
{
    //region Datos de la cabecera
    $fecha_inicio_traslado = $data['fecha_inicio_traslado'];
    $fecha_terminacion_traslado = $data['fecha_terminacion_traslado'];
    $motivo_traslado = $data['motivo_traslado'];
    $punto_partida = $data['punto_partida'];
    $ruc_ci_destinatario = $data['ruc_ci'];
    $punto_llegada = sqlValue("select nombre_pr from provincia where id_pr = " . $data['punto_llegada']);
    $nombre_persona_encargada_transporte = $data['persona_encarga_nombre'];
    $ruc_ci_persona_encargada_transporte = $data['ruc_ci_persona_encargada'];
    $destinatario = $data['destinatario'];
    //endregion

    //region Datos que iran en la cabecera de la ENTREGA DE MERCADERIA
    $numero_guia_remision = $data['numero_guia_remision'];
    $canton_id = $data['punto_llegada_canton'];
    $placa = $data['placa'];
    //endregion

    //region CREACION DEL PDF GUIA DE REMISION
    $pdf = new FPDF();
    //Agrego una nueva pagina al PDF
    $pdf->AddPage();
    //Defino el tipo de letra y el tamano
    $pdf->SetFont('Arial', 'B', 10);

    //region Encabezado Lado IZQUIERDO
    //FECHA INICIO TRASLADO
    $pdf->SetXY(14, 52);
    $pdf->Cell(15, 6, "FECHA INICIO TRASLADO: $fecha_inicio_traslado", 0, 1);

    //MOTIVO TRASLADO
    $pdf->SetXY(14, 56);
    $pdf->Cell(15, 6, "MOTIVO TRASLADO: $motivo_traslado", 0, 1);

    //PUNTO DE PARTIDA
    $pdf->SetXY(14, 60);
    $punto_partida_text = sqlValue("SELECT nombre_c FROM canton WHERE id_c = $punto_partida;");
    $pdf->Cell(15, 6, "PUNTO DE PARTIDA: $punto_partida_text", 0, 1);

    //PUNTO DE LLEGADA
    $pdf->SetXY(14, 64);
    $pdf->Cell(15, 6, "PUNTO DE LLEGADA: $punto_llegada", 0, 1);

    //CI PERSONA TRANSPORTE
    $pdf->SetXY(14, 68);
    $pdf->Cell(15, 6, "CEDULA PERSONA TRANSPORTE: $ruc_ci_persona_encargada_transporte", 0, 1);

    //NOMBRE PERSONA TRANSPORTE
    $pdf->SetXY(14, 73);
    $pdf->Cell(15, 6, "NOMBRE PERSONA TRANSPORTE: $nombre_persona_encargada_transporte", 0, 1);

    //endregion

    //region MITAD

    //DESTINATARIO
    $pdf->SetXY(77, 60);
    $pdf->Cell(15, 6, "DESTINATARIO: $destinatario", 0, 1);

    //endregion

    //FECHA TERMINACION TRASLADO
    $pdf->SetXY(110, 52);
    $pdf->Cell(15, 6, "FECHA FINAL TRASLADO: $fecha_terminacion_traslado", 0, 1);

    //RUC/CI
    $pdf->SetXY(148, 60);
    $pdf->Cell(15, 6, "RUC/CI: $ruc_ci_destinatario", 0, 1);

    //PLACA
    $pdf->SetXY(148, 68);
    $pdf->Cell(15, 6, "PLACA: $placa", 0, 1);

//    //
//    $pdf->SetXY(148, 73);
//    $pdf->Cell(15, 6, "FECHA FINAL TRASLADO: $fecha_terminacion_traslado", 0, 1);


    //Productos de la tabla

    $numero_guia_remision = $data['numero_guia_remision'];

    $query_producto_guia_remision = "SELECT * FROM producto_guia_remision WHERE id_guia_remision = '$numero_guia_remision';";

    $res_producto_guia_remision = sql($query_producto_guia_remision, $eo);

    $y_v = 98;

    $productos_sin_serie = array();

    $pdf->SetFont('Arial', 'B', 12);
    if ($res_producto_guia_remision->num_rows > 0) {
        while ($res = $res_producto_guia_remision->fetch_assoc()) {
            //TIPO DE PRODUCTO SI MANEJA SERIES O NO
            $query_manejo_serie = "SELECT manejo_serie FROM producto WHERE id_p = '" . $res['id_producto'] . "';";
            $manejo_serie = sqlValue($query_manejo_serie);
            if ($manejo_serie == 'SI') {
                //CODIGO DE PRODUCTO
//                $pdf->SetXY(15, $y_v);
//                $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
//                $pdf->Cell(15, 6, $codigo_producto, 0, 1);
                //DESCRIPCION id_producto
                //Cantidad cantidad
                $pdf->SetXY(15, $y_v);
                $pdf->Cell(15, 6, $res['cantidad'], 0, 1);
                //DESCRIPCION id_producto
                $pdf->SetFont('Arial', 'B', 10);
                $producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
                $pdf->SetXY(39, $y_v);
                $pdf->Cell(15, 6, substr($producto, 0, 35), 0, 1);
                $pdf->SetFont('Arial', 'B', 12);
                //NUMERO DE SERIE
                $pdf->SetXY(117, $y_v);
                $pdf->Cell(15, 6, $res['numero_serie_imei'], 0, 1);
                //VALOR
//                $producto_precio = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
//                $pdf->SetXY(176, $y_v); //CADA 5 EN Y
//                $pdf->Cell(15, 6, $producto_precio, 0, 1);
                $y_v = $y_v + 5;
            } else {
                array_push($productos_sin_serie, $res['id_producto']);
            }
        }

        //IMPRIMIR los que no tienen serie
        //Tratamiento
        //$resultado = array_unique($productos_sin_serie);
        $resultado = $productos_sin_serie;
        $impresos = array();
        for ($u = 0; $u < count($resultado); $u++) {
            if (!in_array($resultado[$u], $impresos)) {
                $query_no_serie = "SELECT SUM(cantidad) AS cantidad_total FROM producto_guia_remision WHERE id_guia_remision = '$numero_guia_remision' AND id_producto=$resultado[$u];";
                $cantidad = sqlValue($query_no_serie);
                //CODIGO DE PRODUCTO
//                $pdf->SetXY(15, $y_v);
//                $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = '" . $resultado[$u] . "';");
//                $pdf->Cell(15, 6, $codigo_producto, 0, 1);
                //DESCRIPCION id_producto
                //Cantidad cantidad
//                $pdf->SetXY(39, $y_v);
                $pdf->SetXY(15, $y_v);
                $pdf->Cell(15, 6, $cantidad, 0, 1);
                $pdf->SetFont('Arial', 'B', 10);
                //DESCRIPCION id_producto
                $producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = '" . $resultado[$u] . "';");
//                $pdf->SetXY(55, $y_v);
                $pdf->SetXY(39, $y_v);
                $pdf->Cell(15, 6, substr($producto, 0, 35), 0, 1);
                //NUMERO DE SERIE
//                $pdf->SetXY(135, $y_v);
                $pdf->SetXY(117, $y_v);
                $pdf->Cell(15, 6, '', 0, 1);
                //VALOR
//                $producto_precio = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = '" . $resultado[$u] . "';");
//                $pdf->SetXY(176, $y_v); //CADA 5 EN Y
//                $pdf->Cell(15, 6, $producto_precio, 0, 1);
                $y_v = $y_v + 5;
                array_push($impresos, $resultado[$u]);
            }
        }
    }

    //endregion

    //region GUARDADO DEL PDF EN EL SERVIDOR GUIA REMISION
    $nombre_pdf = "GUIA_REMISION_$numero_guia_remision";
    $filename = $nombre_pdf . ".pdf";
    $pdf->Output($filename, 'F');

    //Mover archivos
    copy($filename, 'images/guias_remision/' . $filename);
    //Borrar del root
    unlink($filename);
    //Actualizacion en base de datos del path del archivo
    sql("UPDATE guia_remision SET  `archivo_guia_remision` = 'images/guias_remision/$filename' WHERE `numero_guia_remision` = '$numero_guia_remision';", $eo);
    //endregion

    //region CREACION DEL PDF ENTREGA DE MERCADERIA

    $vendedor_id = $data['vendedor'];
    $nombre_vendedor = sqlValue("SELECT CONCAT(`nombre`,' ',`apellido`) FROM vendedor WHERE id_vendedor = $vendedor_id;");
    $cedula_vendedor = sqlValue("SELECT cedula FROM vendedor WHERE id_vendedor = $vendedor_id;");
    $canton_id = $data['punto_llegada_canton'];
    $enviado_a = sqlValue("SELECT CONCAT(p.nombre_pr,' - ',c.nombre_c ) AS envio FROM canton AS c, provincia AS p WHERE c.`codigo_provincia_c`=p.`id_pr` AND c.`id_c` = $canton_id;");
    $pdf = new FPDF();
    //Agrego una nueva pagina al PDF
    $pdf->AddPage();
    //Defino el tipo de letra y el tamano
    $pdf->SetFont('Arial', 'B', 13);

    //region Encabezado
    //VENDEDOR
    $pdf->SetXY(14, 52);
    $pdf->Cell(15, 6, "VENDEDOR: $nombre_vendedor", 0, 1);

    $pdf->SetFont('Arial', 'B', 10);
    //CEDULA
    $pdf->SetXY(155, 52);
    $pdf->Cell(15, 6, "CEDULA: $cedula_vendedor", 0, 1);

    //ENVIADO A
    $pdf->SetXY(14, 57);
    $pdf->Cell(15, 6, "ENVIADO A: $enviado_a", 0, 1);

    //FECHA
    $pdf->SetXY(155, 57);
    $pdf->Cell(15, 6, "FECHA: $fecha_inicio_traslado", 0, 1);
    //endregion

    $pdf->SetFont('Arial', 'B', 12);

    //Productos de la tabla

    $numero_entrega_mercaderia = $data['numero_guia_remision'];

    $query_producto_entrega_mercaderia = "SELECT * FROM producto_guia_remision WHERE id_guia_remision = '$numero_entrega_mercaderia';";

    $res_producto_entrega_mercaderia = sql($query_producto_entrega_mercaderia, $eo);

    $y_v = 80;

    $productos_sin_serie = array();

    if ($res_producto_entrega_mercaderia->num_rows > 0) {
        while ($res = $res_producto_entrega_mercaderia->fetch_assoc()) {
            //TIPO DE PRODUCTO SI MANEJA SERIES O NO
            $query_manejo_serie = "SELECT manejo_serie FROM producto WHERE id_p = '" . $res['id_producto'] . "';";
            $manejo_serie = sqlValue($query_manejo_serie);
            if ($manejo_serie == 'SI') {
                //CODIGO DE PRODUCTO
                $pdf->SetXY(13, $y_v);
                $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
                $pdf->Cell(15, 6, $codigo_producto, 0, 1);
                //DESCRIPCION id_producto
                //Cantidad cantidad
                $pdf->SetXY(37, $y_v);
                $pdf->Cell(15, 6, $res['cantidad'], 0, 1);
                //DESCRIPCION id_producto
                $pdf->SetFont('Arial', 'B', 10);
                $producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
                $pdf->SetXY(53, $y_v);
                $pdf->Cell(15, 6, substr($producto, 0, 35), 0, 1);
                $pdf->SetFont('Arial', 'B', 12);
                //NUMERO DE SERIE
                $pdf->SetXY(133, $y_v);
                $pdf->Cell(15, 6, $res['numero_serie_imei'], 0, 1);
                //VALOR
                $producto_precio = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
                $pdf->SetXY(175, $y_v); //CADA 5 EN Y
                $pdf->Cell(15, 6, $producto_precio, 0, 1);
                $y_v = $y_v + 5;
            } else {
                array_push($productos_sin_serie, $res['id_producto']);
            }
        }

        //IMPRIMIR los que no tienen serie
        //Tratamiento
        //$resultado = array_unique($productos_sin_serie);
        $resultado = $productos_sin_serie;
        $impresos = array();
        for ($u = 0; $u < count($resultado); $u++) {
            if (!in_array($resultado[$u], $impresos)) {
                $query_no_serie = "SELECT SUM(cantidad) AS cantidad_total FROM producto_guia_remision WHERE id_guia_remision = '$numero_entrega_mercaderia' AND id_producto=$resultado[$u];";
                $cantidad = sqlValue($query_no_serie);
                //CODIGO DE PRODUCTO
                $pdf->SetXY(13, $y_v);
                $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = '" . $resultado[$u] . "';");
                $pdf->Cell(15, 6, $codigo_producto, 0, 1);
                //DESCRIPCION id_producto
                //Cantidad cantidad
                $pdf->SetXY(37, $y_v);
                $pdf->Cell(15, 6, $cantidad, 0, 1);
                //DESCRIPCION id_producto
                $pdf->SetFont('Arial', 'B', 10);
                $producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = '" . $resultado[$u] . "';");
                $pdf->SetXY(53, $y_v);
                $pdf->Cell(15, 6, substr($producto, 0, 35), 0, 1);
                $pdf->SetFont('Arial', 'B', 12);
                //NUMERO DE SERIE
                $pdf->SetXY(135, $y_v);
                $pdf->Cell(15, 6, '', 0, 1);
                //VALOR
                $producto_precio = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = '" . $resultado[$u] . "';");
                $pdf->SetXY(175, $y_v); //CADA 5 EN Y
                $pdf->Cell(15, 6, $producto_precio, 0, 1);
                $y_v = $y_v + 5;
                array_push($impresos, $resultado[$u]);
            }
        }
    }

    //Observaciones
    $query_observaciones = "SELECT observaciones FROM nota_pedido WHERE numero_guia = '$numero_entrega_mercaderia';";
    $observaciones = sqlValue($query_observaciones);
    $pdf->SetXY(45, 250); //CADA 5 EN Y
    $pdf->Cell(15, 6, $observaciones, 0, 1);
    //endregion

    //region GUARDADO DEL PDF EN EL SERVIDOR ENTREGA DE MERCADERIA
    $nombre_pdf = "ENTREGA_MERCADERIA_$numero_entrega_mercaderia";
    $filename = $nombre_pdf . ".pdf";
    $pdf->Output($filename, 'F');

    //Mover archivos
    copy($filename, 'images/entregas_mercaderia/' . $filename);
    unlink($filename);
    sql("UPDATE guia_remision SET  `archivo_entrega_mercaderia` = 'images/entregas_mercaderia/$filename' WHERE `numero_guia_remision` = '$numero_entrega_mercaderia';", $eo);

    //endregion

    redirect('guia_remision_view.php');

    return TRUE;
}

/**
 * @param $data
 * @param $memberInfo
 * @param $args
 * @return bool
 */
function guia_remision_before_update(&$data, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $data
 * @param $memberInfo
 * @param $args
 */
function guia_remision_after_update($data, $memberInfo, &$args)
{
    guia_remision_after_insert($data, $memberInfo, $args);

}

/**
 * @param $selectedID
 * @param $skipChecks
 * @param $memberInfo
 * @param $args
 * @return bool
 */
function guia_remision_before_delete($selectedID, &$skipChecks, $memberInfo, &$args)
{

    return TRUE;
}

/**
 * @param $selectedID
 * @param $memberInfo
 * @param $args
 */
function guia_remision_after_delete($selectedID, $memberInfo, &$args)
{

}

/**
 * @param $selectedID
 * @param $memberInfo
 * @param $html
 * @param $args
 */
function guia_remision_dv($selectedID, $memberInfo, &$html, &$args)
{
    $html .= '<div class="col-xs-12 detail_view">
    <hr/>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <h3 align="center">Productos de la GUIA DE REMISION A IMPRIMIR</h3>
</div>
<div class="col-xs-12 detail_view">
    <div class="table-responsive">
        <table class="table" id="tabla_productos_guia_remision_final">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Items</th>
                <th scope="col">Codigo</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Numero Serie/IMEI</th>
                <th scope="col">Cantidad</th>
            </tr>
            </thead>
            <tbody id="tbl_body_productos_guia_remision_final">

            </tbody>
        </table>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <br/>
    <input id="btnSaveHijosProductoGuiaRemision" type="button" value="Guardar Productos"
           class="col-xs-2 btn btn-success"/>
</div>';
}

/**
 * @param $query
 * @param $memberInfo
 * @param $args
 * @return mixed
 */
function guia_remision_csv($query, $memberInfo, &$args)
{

    return $query;
}

/**
 * @param $args
 * @return array
 */
function guia_remision_batch_actions(&$args)
{

    return [];
}
