$j(function () {
    console.log('nota_devolucion-dv.js');
    var numero_nota_devolucion_global = $j("#numero_guia").val();
    if (numero_nota_devolucion_global !== '') {
        console.log('id global de nota de devolucion: ' + numero_nota_devolucion_global);
        $j.ajax({
            url: "hooks/ajax/nota_devolucion/carga_productos_nota_devolucion.php",
            method: "post",
            dataType: 'json',
            cache: false,
            data: {
                numero_nota_devolucion: numero_nota_devolucion_global,
            },
            success: function (data) {
                //console.log(data);
                //Lleno los productos que existen en esa Nota de devolucion
                $j('#tbl_body_productos_nota_devolucion_final').html("<h4>Cargando.....</h4>");
                $j('#tbl_body_productos_nota_devolucion_final').html(data[0]);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // console.log(xhr.status);
                console.log(thrownError);
                // var response = JSON.parse(xhr.responseText);
                // console.log(response);
            }
        });
    } else {
        numero_nota_devolucion_global = 'new nota devolucion';
    }
    //Envia el producto a buscar en la base de datos
    $j("#buscar_producto").on('click', function () {
        var texto = $j('#barra_producto').val();
        if (texto != '') {
            cargarBusquedaProductos(texto);
        } else {
            cargarBusquedaProductos();
        }
    });
    //Funcion para agregar los productos a la tabla
    $j("#btnAgregarProductos").click(function () {
        var $productosSeleccionados = $j("#tabla_productos_nota_devolucion input[type='checkbox']:checked").closest("tr");
        $productosSeleccionados.clone().appendTo('#tbl_body_productos_nota_devolucion_final');
        return false;
    });
    //Funcion para eliminar los productos a la tabla
    $j("#btnEliminarProductosNotaDevolucion").click(function () {
        var $productosSeleccionados = $j("#tbl_body_productos_nota_devolucion_final input[type='checkbox']:checked").closest("tr");
        $productosSeleccionados.detach();
        return false;
    });
    //Accion al momento de actualizar
    $j("#update").on('click', function () {
        var numero_nota_devolucion = $j('#numero_guia').val();
        //Productos que iran en el Contrato
        var miContrato = [];
        //Obtener todos los productos del Contrato
        $j('#tabla_productos_nota_devolucion_final > tbody  > tr').each(function () {
            var id = $j(this).attr("id");
            var cantidad = $j(this).find("input").val();
            var producto = {};
            producto.id_producto = id;
            producto.cantidad = cantidad;
            miContrato.push(producto);
        });
        if (numero_nota_devolucion !== '') {
            $j.ajax({
                url: "hooks/ajax/nota_devolucion/guardar_productos_nota_devolucion.php",
                method: "post",
                dataType: 'json',
                cache: false,
                data: {
                    numero_nota_devolucion: numero_nota_devolucion,
                    productos: miContrato
                },
                success: function (data) {
                    //console.log(data);
                }
            });
        }
    });
    //Boton borrar de la barra de busqueda de producto en el contrato
    $j("#eliminar_busqueda_producto").on('click', function () {
        $j("#barra_producto").val('');
        $j('#tbl_body_productos_nota_devolucion').html("<h4>Busque un Producto..</h4>");
        $j('#tbl_body_productos_nota_devolucion').html("<h4>Busque un Producto..</h4>");
    });

    //Funcionalidad de la carga de los productos encontrados a la tabla
    function cargarBusquedaProductos(query) {
        $j.ajax({
            url: "hooks/ajax/buscar_producto.php",
            method: "post",
            dataType: 'json',
            cache: false,
            data: {
                query: query
            },
            success: function (data) {
                $j('#tbl_body_productos_nota_devolucion').html("<h4>Cargando.....</h4>");
                $j('#tbl_body_productos_nota_devolucion').html(data[0]);
            }
        });
    }

    $j("#numero_guia").blur(function () {
        var numero_guia = $j("#numero_guia").val();
        if (!$j('#delete').length) {
            $j.ajax({
                url: "hooks/ajax/nota_devolucion/verificar_nota_devolucion.php",
                method: "post",
                dataType: 'json',
                cache: false,
                data: {
                    numero_guia: numero_guia
                },
                success: function (data) {
                    if (data == 1) {
                        $j(function () {
                            modal_window({
                                title: "Verificación de Nota de Devolucion",
                                message: "Ya existe una NOTA DE DEVOLUCION con el numero de GUIA ACTUAL",
                                button: true
                            });
                        });
                        $j("#numero_guia").val('');
                        return false;
                    } else {
                        return true;
                    }
                }
            });
        }
    });
    $j('#btnSaveHijosProductoNotaDevolucion').on('click', function () {

        var rowCount = $j('#tbl_body_productos_nota_devolucion_final tr').length;
        console.log('Hay en la tabla ' + rowCount + ' Productos');
        $j('#cantidad_productos').val(rowCount);

        var numero_nota_devolucion = $j('#numero_guia').val();
        //Productos que iran en la Nota de devolucion
        var miContrato = [];
        //Obtener todos los productos de la Nota de devolucion
        $j('#tabla_productos_nota_devolucion_final > tbody  > tr').each(function () {
            var id = $j(this).attr("id");
            var cantidad = $j(this).find("input").val();
            var producto = {};
            producto.id_producto = id;
            producto.cantidad = cantidad;
            miContrato.push(producto);
        });
        if (numero_nota_devolucion !== '') {
            $j.ajax({
                url: "hooks/ajax/nota_devolucion/guardar_productos_nota_devolucion.php",
                method: "post",
                dataType: 'json',
                cache: false,
                data: {
                    numero_nota_devolucion: numero_nota_devolucion,
                    productos: miContrato
                },
                success: function (data) {
                    //console.log(data);
                    $j(function () {
                        modal_window({
                            title: "Mensaje Informativo",
                            message: "Se guardo exitosamente los productos para la Nota de Devolucion por favor presione GRABAR",
                            button: true
                        });
                    });
                }, error: function (xhr, ajaxOptions, thrownError) {
                    $j(function () {
                        modal_window({
                            title: "Error de Datos",
                            message: "No se guardo los Productos para la Nota de Devolucion, intente de nuevo",
                            button: true
                        });
                    });
                }
            });
        } else {
            $j(function () {
                modal_window({
                    title: "Error de Datos",
                    message: "Ingrese el NUMERO DE GUIA en para la Nota de Devolucion",
                    button: true
                });
            });
        }
    });
    $j("#numero_guia").on("keypress keyup blur", function (event) {
        jQuery(this).val(jQuery(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || jQuery(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $j("#cantidad_productos").on("keypress keyup blur", function (event) {
        jQuery(this).val(jQuery(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || jQuery(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
});