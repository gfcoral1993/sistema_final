$j(function () {

    //#region Ejecucion
    var insertar = false;

    var actualizar = false;

    if ($j("#update").length) {
        actualizar = true;
    }
    if ($j("#insert").length) {
        insertar = true;
    }

    var button = '<br /><button id="btn_cargar_nota_pedido" name="btn_cargar_nota_pedido" type="button" style="background-color: yellow;">Cargar Nota Pedido</button>';
    $j('#entrega_mercaderia_dv_action_buttons').append(button);
    //#endregion

    //#region JS

    if (insertar === true && actualizar === false) {
        console.log('entrega_mercaderia-dv.js');
        var numero_entrega_mercaderia_global = $j("#numero_entrega_mercaderia").val();
        //Setear el id de la nota de entrega_mercaderia
        $j('#numero_entrega_mercaderia').val(aleatorio(100000, 999999));
        //Bloquear el input del id de la nota de entrega_mercaderia
        // $j('#numero_entrega_mercaderia').attr('disabled', 'disabled');
    }

    if (insertar === true && actualizar === true) {
        var numero_nota_pedido = $j('#nota_pedido').val();
        var numero_entrega_mercaderia = $j('#numero_entrega_mercaderia').val();
        console.log('El numero de nota de Pedido es: ' + numero_nota_pedido);
        if (numero_nota_pedido !== '') {
            $j.ajax({
                url: "hooks/ajax/entrega_mercaderia/carga_productos_entrega_mercaderia.php",
                method: "post",
                dataType: 'json',
                cache: false,
                data: {
                    numero_nota_pedido: numero_nota_pedido,
                    numero_entrega_mercaderia: numero_entrega_mercaderia
                },
                success: function (data) {
                    $j('#tbl_body_productos_entrega_mercaderia_final').html("<h4>Cargando.....</h4>");
                    $j('#tbl_body_productos_entrega_mercaderia_final').html(data[0]);
                },
                error: function (xht, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });
        } else {
            modal_window({
                title: "Verificación de Nota de Pedido",
                message: "Seleccione la Nota de Pedido",
                button: true
            });
        }
    }

    $j('#btn_cargar_nota_pedido').on('click', function () {
        if (numero_entrega_mercaderia_global !== '') {

            var numero_nota_pedido = $j('#nota_pedido').val();
            console.log('El numero de nota de Pedido es: ' + numero_nota_pedido);
            if (numero_nota_pedido !== '') {
                $j.ajax({
                    url: "hooks/ajax/entrega_mercaderia/carga_productos_entrega_mercaderia.php",
                    method: "post",
                    dataType: 'json',
                    cache: false,
                    data: {
                        numero_nota_pedido: numero_nota_pedido,
                    },
                    success: function (data) {
                        $j('#tbl_body_productos_entrega_mercaderia_final').html("<h4>Cargando.....</h4>");
                        $j('#tbl_body_productos_entrega_mercaderia_final').html(data[0]);
                    },
                    error: function (xht, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    }
                });
            } else {
                modal_window({
                    title: "Verificación de Nota de Pedido",
                    message: "Seleccione la Nota de Pedido",
                    button: true
                });
            }
        } else {
            numero_entrega_mercaderia_global = 'new nota entrega_mercaderia';
        }

    });

    //#endregion

    //#region Funciones

    function aleatorio(inferior, superior) {
        var numPosibilidades = superior - inferior;
        var aleatorio = Math.random() * (numPosibilidades + 1);
        aleatorio = Math.floor(aleatorio);
        return inferior + aleatorio;
    }

    //Boton borrar de la barra de busqueda de producto en el contrato
    $j("#eliminar_busqueda_producto").on('click', function () {
        $j("#barra_producto").val('');
        $j('#tbl_body_productos_entrega_mercaderia').html("<h4>Busque un Producto..</h4>");
    });

    //Funcion para eliminar los productos a la tabla
    $j("#btnEliminarProductosentrega_mercaderia").click(function () {
        var $productosSeleccionados = $j("#tbl_body_productos_entrega_mercaderia_final input[type='checkbox']:checked").closest("tr");
        $productosSeleccionados.detach();
        return false;
    });

    //Barra de Busqueda de Productos
    $j("#buscar_producto").on('click', function () {
        var texto = $j('#barra_producto').val();
        if (texto != '') {
            cargarBusquedaProductos(texto);
        } else {
            cargarBusquedaProductos();
        }
    });

    //Funcionalidad de la carga de los productos encontrados a la tabla
    function cargarBusquedaProductos(query) {
        $j.ajax({
            url: "hooks/ajax/buscar_producto.php",
            method: "post",
            dataType: 'json',
            cache: false,
            data: {
                query: query
            },
            success: function (data) {
                $j('#tbl_body_productos_entrega_mercaderia').html("<h4>Cargando.....</h4>");
                $j('#tbl_body_productos_entrega_mercaderia').html(data[0]);
            }
        });
    }

    //Funcion para agregar los productos a la tabla
    $j("#btnAgregarProductos").click(function () {
        var $productosSeleccionados = $j("#tabla_productos_entrega_mercaderia input[type='checkbox']:checked").closest("tr");
        $productosSeleccionados.clone().appendTo('#tbl_body_productos_entrega_mercaderia_final');
        return false;
    });

    //#endregion

    //#region Agregar Productos

    //Guardar productos hijos en el entrega_mercaderia
    $j('#btnSaveHijosProductoEntregaMercaderia').on('click', function () {

        var id_vendedor = $j('#vendedor').val();
        var numero_nota_pedido = $j('#nota_pedido').val();
        var rowCount = $j('#tbl_body_productos_entrega_mercaderia_final tr').length;
        console.log('Hay en la tabla ' + rowCount + ' Productos');
        $j('#cantidad_productos').val(rowCount);
        var numero_entrega_mercaderia = $j('#numero_entrega_mercaderia').val();
        console.log('EL numero de entrega_mercaderia es: ' + numero_entrega_mercaderia)
        //Productos que iran en la Nota de Pedido
        var miContrato = [];
        //Obtener todos los productos de la Nota de Pedido
        $j('#tabla_productos_entrega_mercaderia_final > tbody  > tr').each(function () {
            var id = $j(this).attr("id");
            // var cantidad = $j(this).find("input").val();
            var cantidad = $j(this).find("input[name='cantidad']").val();
            var producto = {};
            //numero_serie_imei
            var numero_serie_imei = $j(this).find("input[name='numero_serie_imei']").val();
            producto.id_producto = id;
            producto.cantidad = cantidad;
            producto.numero_serie_imei = numero_serie_imei;
            console.log(numero_serie_imei);
            miContrato.push(producto);
        });
        if (numero_entrega_mercaderia !== '') {
            $j.ajax({
                url: "hooks/ajax/entrega_mercaderia/guardar_productos_entrega_mercaderia.php",
                method: "post",
                dataType: 'json',
                cache: false,
                data: {
                    numero_entrega_mercaderia: numero_entrega_mercaderia,
                    productos: miContrato,
                    vendedor: id_vendedor,
                    numero_nota_pedido: numero_nota_pedido
                },
                success: function (data) {
                    console.log('Se guardo los hijos de producto_entrega_mercaderia....');
                    $j(function () {
                        modal_window({
                            title: "Mensaje Informativo",
                            message: "Se guardo exitosamente los productos para el entrega_mercaderia por favor presione GRABAR",
                            button: true
                        });
                    });
                    //console.log(data);
                }, error: function (xhr, ajaxOptions, thrownError) {
                    //console.log(xhr.status);
                    console.log('No se guardo los hijos de producto_entrega_mercaderia......');
                    // console.log(thrownError);
                    // var response = JSON.parse(xhr.responseText);
                    // console.log(response);
                    $j(function () {
                        modal_window({
                            title: "Error de Datos",
                            message: "No se guardo los Productos para el entrega_mercaderia, intente de nuevo",
                            button: true
                        });
                    });
                }
            });
        } else {
            $j(function () {
                modal_window({
                    title: "Error de Datos",
                    message: "Ingrese el NUMERO DE ENTREGA MERCADERIA para la Nota de Pedido",
                    button: true
                });
            });
        }
    });

    //#endregion
});