<?php
// For help on using hooks, please refer to https://bigprof.com/appgini/help/working-with-generated-web-database-application/hooks
require('fpdf182/fpdf.php');
function entrega_mercaderia_init(&$options, $memberInfo, &$args)
{

    return TRUE;
}

function entrega_mercaderia_header($contentType, $memberInfo, &$args)
{
    $header = '';

    switch ($contentType) {
        case 'tableview':
            $header = '';
            break;

        case 'detailview':
            $header = '';
            break;

        case 'tableview+detailview':
            $header = '';
            break;

        case 'print-tableview':
            $header = '';
            break;

        case 'print-detailview':
            $header = '';
            break;

        case 'filters':
            $header = '';
            break;
    }

    return $header;
}

function entrega_mercaderia_footer($contentType, $memberInfo, &$args)
{
    $footer = '';

    switch ($contentType) {
        case 'tableview':
            $footer = '';
            break;

        case 'detailview':
            $footer = '';
            break;

        case 'tableview+detailview':
            $footer = '';
            break;

        case 'print-tableview':
            $footer = '';
            break;

        case 'print-detailview':
            $footer = '';
            break;

        case 'filters':
            $footer = '';
            break;
    }

    return $footer;
}

function entrega_mercaderia_before_insert(&$data, $memberInfo, &$args)
{

    return TRUE;
}

function entrega_mercaderia_after_insert($data, $memberInfo, &$args)
{
    //Reutilizo la funcion de actualizacion
    entrega_mercaderia_after_update($data, $memberInfo, $args);
    return TRUE;
}

function entrega_mercaderia_before_update(&$data, $memberInfo, &$args)
{

    return TRUE;
}

function entrega_mercaderia_after_update($data, $memberInfo, &$args)
{
    //region Datos que iran en la cabecera de la ENTREGA DE MERCADERIA
    $numero_entrega_mercaderia = $data['numero_entrega_mercaderia'];
    $vendedor_id = $data['vendedor'];
    $nombre_vendedor = sqlValue("SELECT CONCAT(`nombre`,' ',`apellido`) FROM vendedor WHERE id_vendedor = $vendedor_id;");
    $cedula_vendedor = sqlValue("SELECT cedula FROM vendedor WHERE id_vendedor = $vendedor_id;");
    $canton_id = $data['punto_llegada_canton'];
    $enviado_a = sqlValue("SELECT CONCAT(p.nombre_pr,' - ',c.nombre_c ) AS envio FROM canton AS c, provincia AS p WHERE c.`codigo_provincia_c`=p.`id_pr` AND c.`id_c` = $canton_id;");
    $fecha = $data['fecha_inicio_traslado'];
    //endregion

    //region CREACION DEL PDF
    $pdf = new FPDF();
    //Agrego una nueva pagina al PDF
    $pdf->AddPage();
    //Defino el tipo de letra y el tamano
    $pdf->SetFont('Arial', 'B', 13);

    //region Encabezado
    //VENDEDOR
    $pdf->SetXY(14, 52);
    $pdf->Cell(15, 6, "VENDEDOR: $nombre_vendedor", 0, 1);

    $pdf->SetFont('Arial', 'B', 10);
    //CEDULA
    $pdf->SetXY(155, 52);
    $pdf->Cell(15, 6, "CEDULA: $cedula_vendedor", 0, 1);

    //ENVIADO A
    $pdf->SetXY(14, 57);
    $pdf->Cell(15, 6, "ENVIADO A: $enviado_a", 0, 1);

    //FECHA
    $pdf->SetXY(155, 57);
    $pdf->Cell(15, 6, "FECHA: $fecha", 0, 1);
    //endregion

    $pdf->SetFont('Arial', 'B', 12);

    //Productos de la tabla

    $numero_entrega_mercaderia = $data['numero_entrega_mercaderia'];

    $query_producto_entrega_mercaderia = "SELECT * FROM producto_entrega_mercaderia WHERE id_entrega_mercaderia = '$numero_entrega_mercaderia';";

    $res_producto_entrega_mercaderia = sql($query_producto_entrega_mercaderia, $eo);

    $y_v = 80;

    $productos_sin_serie = array();

    if ($res_producto_entrega_mercaderia->num_rows > 0) {
        while ($res = $res_producto_entrega_mercaderia->fetch_assoc()) {
            //TIPO DE PRODUCTO SI MANEJA SERIES O NO
            $query_manejo_serie = "SELECT manejo_serie FROM producto WHERE id_p = '" . $res['id_producto'] . "';";
            $manejo_serie = sqlValue($query_manejo_serie);
            if ($manejo_serie == 'SI') {
                //CODIGO DE PRODUCTO
                $pdf->SetXY(13, $y_v);
                $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
                $pdf->Cell(15, 6, $codigo_producto, 0, 1);
                //DESCRIPCION id_producto
                //Cantidad cantidad
                $pdf->SetXY(37, $y_v);
                $pdf->Cell(15, 6, $res['cantidad'], 0, 1);
                //DESCRIPCION id_producto
                $pdf->SetFont('Arial', 'B', 10);
                $producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
                $pdf->SetXY(53, $y_v);
                $pdf->Cell(15, 6, substr($producto, 0, 35), 0, 1);
                $pdf->SetFont('Arial', 'B', 12);
                //NUMERO DE SERIE
                $pdf->SetXY(133, $y_v);
                $pdf->Cell(15, 6, $res['numero_serie_imei'], 0, 1);
                //VALOR
                $producto_precio = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = '" . $res['id_producto'] . "';");
                $pdf->SetXY(175, $y_v); //CADA 5 EN Y
                $pdf->Cell(15, 6, $producto_precio, 0, 1);
                $y_v = $y_v + 5;
            } else {
                array_push($productos_sin_serie, $res['id_producto']);
            }
        }

        //IMPRIMIR los que no tienen serie
        //Tratamiento
        //$resultado = array_unique($productos_sin_serie);
        $resultado = $productos_sin_serie;
        $impresos = array();
        for ($u = 0; $u < count($resultado); $u++) {
            if (!in_array($resultado[$u], $impresos)) {
                $query_no_serie = "SELECT SUM(cantidad) AS cantidad_total FROM producto_entrega_mercaderia WHERE id_entrega_mercaderia = '$numero_entrega_mercaderia' AND id_producto=$resultado[$u];";
                $cantidad = sqlValue($query_no_serie);
                //CODIGO DE PRODUCTO
                $pdf->SetXY(13, $y_v);
                $codigo_producto = sqlValue("SELECT codigo_p FROM producto WHERE id_p = '" . $resultado[$u] . "';");
                $pdf->Cell(15, 6, $codigo_producto, 0, 1);
                //DESCRIPCION id_producto
                //Cantidad cantidad
                $pdf->SetXY(37, $y_v);
                $pdf->Cell(15, 6, $cantidad, 0, 1);
                //DESCRIPCION id_producto
                $pdf->SetFont('Arial', 'B', 10);
                $producto = sqlValue("SELECT nombre_p FROM producto WHERE id_p = '" . $resultado[$u] . "';");
                $pdf->SetXY(53, $y_v);
                $pdf->Cell(15, 6, substr($producto, 0, 35), 0, 1);
                $pdf->SetFont('Arial', 'B', 12);
                //NUMERO DE SERIE
                $pdf->SetXY(135, $y_v);
                $pdf->Cell(15, 6, '', 0, 1);
                //VALOR
                $producto_precio = sqlValue("SELECT pvp_producto FROM producto WHERE id_p = '" . $resultado[$u] . "';");
                $pdf->SetXY(175, $y_v); //CADA 5 EN Y
                $pdf->Cell(15, 6, $producto_precio, 0, 1);
                $y_v = $y_v + 5;
                array_push($impresos, $resultado[$u]);
            }
        }
    }

    //Observaciones
    $query_observaciones = "SELECT observaciones FROM nota_pedido WHERE numero_guia = '$numero_entrega_mercaderia';";
    $observaciones = sqlValue($query_observaciones);
    $pdf->SetXY(45, 250); //CADA 5 EN Y
    $pdf->Cell(15, 6, $observaciones, 0, 1);
    //endregion

    //region GUARDADO DEL PDF EN EL SERVIDOR
    $nombre_pdf = "ENTREGA_MERCADERIA_$numero_entrega_mercaderia";
    $filename = $nombre_pdf . ".pdf";
    $pdf->Output($filename, 'F');

    //Mover archivos
    copy($filename, 'images/' . $filename);

    sql("UPDATE entrega_mercaderia SET  `archivo_entrega_mercaderia` = '$filename' WHERE `numero_entrega_mercaderia` = '$numero_entrega_mercaderia';", $eo);

    redirect('entrega_mercaderia_view.php');

    return TRUE;
}

function entrega_mercaderia_before_delete($selectedID, &$skipChecks, $memberInfo, &$args)
{

    return TRUE;
}

function entrega_mercaderia_after_delete($selectedID, $memberInfo, &$args)
{

}

function entrega_mercaderia_dv($selectedID, $memberInfo, &$html, &$args)
{
    $html .= '<div class="col-xs-12 detail_view">
    <hr/>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <h3 align="center">Productos de la ENTREGA DE MERCADERIA A IMPRIMIR</h3>
</div>
<div class="col-xs-12 detail_view">
    <div class="table-responsive">
        <table class="table" id="tabla_productos_entrega_mercaderia_final">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Items</th>
                <th scope="col">Codigo</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Numero Serie/IMEI</th>
                <th scope="col">Cantidad</th>
            </tr>
            </thead>
            <tbody id="tbl_body_productos_entrega_mercaderia_final">

            </tbody>
        </table>
    </div>
</div>
<div class="col-xs-12 detail_view">
    <hr/>
    <br/>
    <input id="btnSaveHijosProductoEntregaMercaderia" type="button" value="Guardar Productos"
           class="col-xs-2 btn btn-success"/>
</div>';
}

function entrega_mercaderia_csv($query, $memberInfo, &$args)
{

    return $query;
}

function entrega_mercaderia_batch_actions(&$args)
{

    return [];
}
