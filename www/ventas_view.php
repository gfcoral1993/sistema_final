<?php
// This script and data application were generated by AppGini 5.97
// Download AppGini for free from https://bigprof.com/appgini/download/

	$currDir = dirname(__FILE__);
	include_once("{$currDir}/lib.php");
	@include_once("{$currDir}/hooks/ventas.php");
	include_once("{$currDir}/ventas_dml.php");

	// mm: can the current member access this page?
	$perm = getTablePermissions('ventas');
	if(!$perm['access']) {
		echo error_message($Translation['tableAccessDenied']);
		exit;
	}

	$x = new DataList;
	$x->TableName = 'ventas';

	// Fields that can be displayed in the table view
	$x->QueryFieldsTV = [
		"`ventas`.`id`" => "id",
		"IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') /* VENDEDOR */" => "vendedor",
		"`ventas`.`fecha_ingreso`" => "fecha_ingreso",
		"`ventas`.`usuario_creador`" => "usuario_creador",
		"`ventas`.`fecha_actualizacion`" => "fecha_actualizacion",
		"`ventas`.`usuario_actualizo`" => "usuario_actualizo",
		"if(`ventas`.`fecha`,date_format(`ventas`.`fecha`,'%d/%m/%Y'),'')" => "fecha",
		"`ventas`.`numero_contrato`" => "numero_contrato",
		"`ventas`.`cedula`" => "cedula",
		"`ventas`.`nombre`" => "nombre",
		"`ventas`.`mes_comision_venta`" => "mes_comision_venta",
		"`ventas`.`valor_contrato`" => "valor_contrato",
		"`ventas`.`pvp`" => "pvp",
		"IF(    CHAR_LENGTH(`vendedor2`.`nombre`) || CHAR_LENGTH(`vendedor2`.`apellido`), CONCAT_WS('',   `vendedor2`.`nombre`, ' ', `vendedor2`.`apellido`), '') /* A MEDIAS */" => "a_medias",
		"IF(    CHAR_LENGTH(`dependencia_laboral1`.`nombre_dependencia_laboral`), CONCAT_WS('',   `dependencia_laboral1`.`nombre_dependencia_laboral`), '') /* DEPENDENCIA LABORAL */" => "dependencia_laboral",
		"IF(    CHAR_LENGTH(`aprobaciones1`.`nombre_aprobaciones`), CONCAT_WS('',   `aprobaciones1`.`nombre_aprobaciones`), '') /* APROBACIONES */" => "aprobaciones",
		"`ventas`.`cobro_particular`" => "cobro_particular",
		"`ventas`.`entrega_cedula`" => "entrega_cedula",
		"`ventas`.`cuotas`" => "cuotas",
		"`ventas`.`valor_cuota`" => "valor_cuota",
		"`ventas`.`cantidad_productos`" => "cantidad_productos",
		"IF(    CHAR_LENGTH(`provincia1`.`nombre_pr`), CONCAT_WS('',   `provincia1`.`nombre_pr`), '') /* PROVINCIA */" => "provincia",
		"IF(    CHAR_LENGTH(`canton1`.`nombre_c`), CONCAT_WS('',   `canton1`.`nombre_c`), '') /* CANTON */" => "canton",
		"IF(    CHAR_LENGTH(`parroquia1`.`nombre_pa`), CONCAT_WS('',   `parroquia1`.`nombre_pa`), '') /* PARROQUIA */" => "parroquia",
		"`ventas`.`categorias_descripcion`" => "categorias_descripcion",
		"IF(    CHAR_LENGTH(`tipo_venta1`.`nombre_tipo_venta`), CONCAT_WS('',   `tipo_venta1`.`nombre_tipo_venta`), '') /* TIPO VENTA */" => "tipo_venta",
	];
	// mapping incoming sort by requests to actual query fields
	$x->SortFields = [
		1 => '`ventas`.`id`',
		2 => 2,
		3 => '`ventas`.`fecha_ingreso`',
		4 => 4,
		5 => '`ventas`.`fecha_actualizacion`',
		6 => 6,
		7 => '`ventas`.`fecha`',
		8 => 8,
		9 => 9,
		10 => 10,
		11 => 11,
		12 => '`ventas`.`valor_contrato`',
		13 => '`ventas`.`pvp`',
		14 => 14,
		15 => '`dependencia_laboral1`.`nombre_dependencia_laboral`',
		16 => '`aprobaciones1`.`nombre_aprobaciones`',
		17 => 17,
		18 => 18,
		19 => '`ventas`.`cuotas`',
		20 => '`ventas`.`valor_cuota`',
		21 => '`ventas`.`cantidad_productos`',
		22 => '`provincia1`.`nombre_pr`',
		23 => '`canton1`.`nombre_c`',
		24 => '`parroquia1`.`nombre_pa`',
		25 => 25,
		26 => '`tipo_venta1`.`nombre_tipo_venta`',
	];

	// Fields that can be displayed in the csv file
	$x->QueryFieldsCSV = [
		"`ventas`.`id`" => "id",
		"IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') /* VENDEDOR */" => "vendedor",
		"`ventas`.`fecha_ingreso`" => "fecha_ingreso",
		"`ventas`.`usuario_creador`" => "usuario_creador",
		"`ventas`.`fecha_actualizacion`" => "fecha_actualizacion",
		"`ventas`.`usuario_actualizo`" => "usuario_actualizo",
		"if(`ventas`.`fecha`,date_format(`ventas`.`fecha`,'%d/%m/%Y'),'')" => "fecha",
		"`ventas`.`numero_contrato`" => "numero_contrato",
		"`ventas`.`cedula`" => "cedula",
		"`ventas`.`nombre`" => "nombre",
		"`ventas`.`mes_comision_venta`" => "mes_comision_venta",
		"`ventas`.`valor_contrato`" => "valor_contrato",
		"`ventas`.`pvp`" => "pvp",
		"IF(    CHAR_LENGTH(`vendedor2`.`nombre`) || CHAR_LENGTH(`vendedor2`.`apellido`), CONCAT_WS('',   `vendedor2`.`nombre`, ' ', `vendedor2`.`apellido`), '') /* A MEDIAS */" => "a_medias",
		"IF(    CHAR_LENGTH(`dependencia_laboral1`.`nombre_dependencia_laboral`), CONCAT_WS('',   `dependencia_laboral1`.`nombre_dependencia_laboral`), '') /* DEPENDENCIA LABORAL */" => "dependencia_laboral",
		"IF(    CHAR_LENGTH(`aprobaciones1`.`nombre_aprobaciones`), CONCAT_WS('',   `aprobaciones1`.`nombre_aprobaciones`), '') /* APROBACIONES */" => "aprobaciones",
		"`ventas`.`cobro_particular`" => "cobro_particular",
		"`ventas`.`entrega_cedula`" => "entrega_cedula",
		"`ventas`.`cuotas`" => "cuotas",
		"`ventas`.`valor_cuota`" => "valor_cuota",
		"`ventas`.`cantidad_productos`" => "cantidad_productos",
		"IF(    CHAR_LENGTH(`provincia1`.`nombre_pr`), CONCAT_WS('',   `provincia1`.`nombre_pr`), '') /* PROVINCIA */" => "provincia",
		"IF(    CHAR_LENGTH(`canton1`.`nombre_c`), CONCAT_WS('',   `canton1`.`nombre_c`), '') /* CANTON */" => "canton",
		"IF(    CHAR_LENGTH(`parroquia1`.`nombre_pa`), CONCAT_WS('',   `parroquia1`.`nombre_pa`), '') /* PARROQUIA */" => "parroquia",
		"`ventas`.`categorias_descripcion`" => "categorias_descripcion",
		"IF(    CHAR_LENGTH(`tipo_venta1`.`nombre_tipo_venta`), CONCAT_WS('',   `tipo_venta1`.`nombre_tipo_venta`), '') /* TIPO VENTA */" => "tipo_venta",
	];
	// Fields that can be filtered
	$x->QueryFieldsFilters = [
		"`ventas`.`id`" => "ID",
		"IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') /* VENDEDOR */" => "VENDEDOR",
		"`ventas`.`fecha_ingreso`" => "FECHA INGRESO",
		"`ventas`.`usuario_creador`" => "USUARIO CREADOR",
		"`ventas`.`fecha_actualizacion`" => "FECHA ACTUALIZACI&#211;N",
		"`ventas`.`usuario_actualizo`" => "USUARIO ACTUALIZADOR",
		"`ventas`.`fecha`" => "FECHA",
		"`ventas`.`numero_contrato`" => "NUMERO CONTRATO",
		"`ventas`.`cedula`" => "C&#201;DULA",
		"`ventas`.`nombre`" => "NOMBRE",
		"`ventas`.`mes_comision_venta`" => "MES COMISI&#211;N VENTA",
		"`ventas`.`valor_contrato`" => "VALOR CONTRATO",
		"`ventas`.`pvp`" => "P.V.P",
		"IF(    CHAR_LENGTH(`vendedor2`.`nombre`) || CHAR_LENGTH(`vendedor2`.`apellido`), CONCAT_WS('',   `vendedor2`.`nombre`, ' ', `vendedor2`.`apellido`), '') /* A MEDIAS */" => "A MEDIAS",
		"IF(    CHAR_LENGTH(`dependencia_laboral1`.`nombre_dependencia_laboral`), CONCAT_WS('',   `dependencia_laboral1`.`nombre_dependencia_laboral`), '') /* DEPENDENCIA LABORAL */" => "DEPENDENCIA LABORAL",
		"IF(    CHAR_LENGTH(`aprobaciones1`.`nombre_aprobaciones`), CONCAT_WS('',   `aprobaciones1`.`nombre_aprobaciones`), '') /* APROBACIONES */" => "APROBACIONES",
		"`ventas`.`cobro_particular`" => "COBRO PARTICULAR",
		"`ventas`.`entrega_cedula`" => "ENTREGA CEDULA",
		"`ventas`.`cuotas`" => "N&#218;MERO DE CUOTAS",
		"`ventas`.`valor_cuota`" => "VALOR CUOTA",
		"`ventas`.`cantidad_productos`" => "CANTIDAD ITEMS",
		"IF(    CHAR_LENGTH(`provincia1`.`nombre_pr`), CONCAT_WS('',   `provincia1`.`nombre_pr`), '') /* PROVINCIA */" => "PROVINCIA",
		"IF(    CHAR_LENGTH(`canton1`.`nombre_c`), CONCAT_WS('',   `canton1`.`nombre_c`), '') /* CANTON */" => "CANTON",
		"IF(    CHAR_LENGTH(`parroquia1`.`nombre_pa`), CONCAT_WS('',   `parroquia1`.`nombre_pa`), '') /* PARROQUIA */" => "PARROQUIA",
		"`ventas`.`categorias_descripcion`" => "CATEGORIAS",
		"IF(    CHAR_LENGTH(`tipo_venta1`.`nombre_tipo_venta`), CONCAT_WS('',   `tipo_venta1`.`nombre_tipo_venta`), '') /* TIPO VENTA */" => "TIPO VENTA",
	];

	// Fields that can be quick searched
	$x->QueryFieldsQS = [
		"`ventas`.`id`" => "id",
		"IF(    CHAR_LENGTH(`vendedor1`.`nombre`) || CHAR_LENGTH(`vendedor1`.`apellido`), CONCAT_WS('',   `vendedor1`.`nombre`, ' ', `vendedor1`.`apellido`), '') /* VENDEDOR */" => "vendedor",
		"`ventas`.`fecha_ingreso`" => "fecha_ingreso",
		"`ventas`.`usuario_creador`" => "usuario_creador",
		"`ventas`.`fecha_actualizacion`" => "fecha_actualizacion",
		"`ventas`.`usuario_actualizo`" => "usuario_actualizo",
		"if(`ventas`.`fecha`,date_format(`ventas`.`fecha`,'%d/%m/%Y'),'')" => "fecha",
		"`ventas`.`numero_contrato`" => "numero_contrato",
		"`ventas`.`cedula`" => "cedula",
		"`ventas`.`nombre`" => "nombre",
		"`ventas`.`mes_comision_venta`" => "mes_comision_venta",
		"`ventas`.`valor_contrato`" => "valor_contrato",
		"`ventas`.`pvp`" => "pvp",
		"IF(    CHAR_LENGTH(`vendedor2`.`nombre`) || CHAR_LENGTH(`vendedor2`.`apellido`), CONCAT_WS('',   `vendedor2`.`nombre`, ' ', `vendedor2`.`apellido`), '') /* A MEDIAS */" => "a_medias",
		"IF(    CHAR_LENGTH(`dependencia_laboral1`.`nombre_dependencia_laboral`), CONCAT_WS('',   `dependencia_laboral1`.`nombre_dependencia_laboral`), '') /* DEPENDENCIA LABORAL */" => "dependencia_laboral",
		"IF(    CHAR_LENGTH(`aprobaciones1`.`nombre_aprobaciones`), CONCAT_WS('',   `aprobaciones1`.`nombre_aprobaciones`), '') /* APROBACIONES */" => "aprobaciones",
		"`ventas`.`cobro_particular`" => "cobro_particular",
		"`ventas`.`entrega_cedula`" => "entrega_cedula",
		"`ventas`.`cuotas`" => "cuotas",
		"`ventas`.`valor_cuota`" => "valor_cuota",
		"`ventas`.`cantidad_productos`" => "cantidad_productos",
		"IF(    CHAR_LENGTH(`provincia1`.`nombre_pr`), CONCAT_WS('',   `provincia1`.`nombre_pr`), '') /* PROVINCIA */" => "provincia",
		"IF(    CHAR_LENGTH(`canton1`.`nombre_c`), CONCAT_WS('',   `canton1`.`nombre_c`), '') /* CANTON */" => "canton",
		"IF(    CHAR_LENGTH(`parroquia1`.`nombre_pa`), CONCAT_WS('',   `parroquia1`.`nombre_pa`), '') /* PARROQUIA */" => "parroquia",
		"`ventas`.`categorias_descripcion`" => "categorias_descripcion",
		"IF(    CHAR_LENGTH(`tipo_venta1`.`nombre_tipo_venta`), CONCAT_WS('',   `tipo_venta1`.`nombre_tipo_venta`), '') /* TIPO VENTA */" => "tipo_venta",
	];

	// Lookup fields that can be used as filterers
	$x->filterers = ['vendedor' => 'VENDEDOR', 'a_medias' => 'A MEDIAS', 'dependencia_laboral' => 'DEPENDENCIA LABORAL', 'aprobaciones' => 'APROBACIONES', 'provincia' => 'PROVINCIA', 'canton' => 'CANTON', 'parroquia' => 'PARROQUIA', 'tipo_venta' => 'TIPO VENTA', ];

	$x->QueryFrom = "`ventas` LEFT JOIN `vendedor` as vendedor1 ON `vendedor1`.`id_vendedor`=`ventas`.`vendedor` LEFT JOIN `vendedor` as vendedor2 ON `vendedor2`.`id_vendedor`=`ventas`.`a_medias` LEFT JOIN `dependencia_laboral` as dependencia_laboral1 ON `dependencia_laboral1`.`id_dependencia_laboral`=`ventas`.`dependencia_laboral` LEFT JOIN `aprobaciones` as aprobaciones1 ON `aprobaciones1`.`id_aprobaciones`=`ventas`.`aprobaciones` LEFT JOIN `provincia` as provincia1 ON `provincia1`.`id_pr`=`ventas`.`provincia` LEFT JOIN `canton` as canton1 ON `canton1`.`id_c`=`ventas`.`canton` LEFT JOIN `parroquia` as parroquia1 ON `parroquia1`.`id_pa`=`ventas`.`parroquia` LEFT JOIN `tipo_venta` as tipo_venta1 ON `tipo_venta1`.`id_tipo_venta`=`ventas`.`tipo_venta` ";
	$x->QueryWhere = '';
	$x->QueryOrder = '';

	$x->AllowSelection = 1;
	$x->HideTableView = ($perm['view'] == 0 ? 1 : 0);
	$x->AllowDelete = $perm['delete'];
	$x->AllowMassDelete = true;
	$x->AllowInsert = $perm['insert'];
	$x->AllowUpdate = $perm['edit'];
	$x->SeparateDV = 1;
	$x->AllowDeleteOfParents = 0;
	$x->AllowFilters = 1;
	$x->AllowSavingFilters = (getLoggedAdmin() !== false);
	$x->AllowSorting = 1;
	$x->AllowNavigation = 1;
	$x->AllowPrinting = 1;
	$x->AllowPrintingDV = 1;
	$x->AllowCSV = 1;
	$x->RecordsPerPage = 100;
	$x->QuickSearch = 1;
	$x->QuickSearchText = $Translation['quick search'];
	$x->ScriptFileName = 'ventas_view.php';
	$x->RedirectAfterInsert = 'ventas_view.php';
	$x->TableTitle = 'VENTAS';
	$x->TableIcon = 'resources/table_icons/money_add.png';
	$x->PrimaryKey = '`ventas`.`numero_contrato`';
	$x->DefaultSortField = '1';
	$x->DefaultSortDirection = 'desc';

	$x->ColWidth = [150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, ];
	$x->ColCaption = ['ID', 'VENDEDOR', 'FECHA INGRESO', 'FECHA', 'NUMERO CONTRATO', 'C&#201;DULA', 'NOMBRE', 'MES COMISI&#211;N VENTA', 'VALOR CONTRATO', 'P.V.P', 'A MEDIAS', 'DEPENDENCIA LABORAL', 'APROBACIONES', 'COBRO PARTICULAR', 'ENTREGA CEDULA', 'N&#218;MERO DE CUOTAS', 'VALOR CUOTA', 'CANTIDAD ITEMS', 'PROVINCIA', 'CANTON', 'PARROQUIA', 'CATEGORIAS', 'TIPO VENTA', ];
	$x->ColFieldName = ['id', 'vendedor', 'fecha_ingreso', 'fecha', 'numero_contrato', 'cedula', 'nombre', 'mes_comision_venta', 'valor_contrato', 'pvp', 'a_medias', 'dependencia_laboral', 'aprobaciones', 'cobro_particular', 'entrega_cedula', 'cuotas', 'valor_cuota', 'cantidad_productos', 'provincia', 'canton', 'parroquia', 'categorias_descripcion', 'tipo_venta', ];
	$x->ColNumber  = [1, 2, 3, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, ];

	// template paths below are based on the app main directory
	$x->Template = 'templates/ventas_templateTV.html';
	$x->SelectedTemplate = 'templates/ventas_templateTVS.html';
	$x->TemplateDV = 'templates/ventas_templateDV.html';
	$x->TemplateDVP = 'templates/ventas_templateDVP.html';

	$x->ShowTableHeader = 1;
	$x->TVClasses = "";
	$x->DVClasses = "";
	$x->HasCalculatedFields = false;
	$x->AllowConsoleLog = false;
	$x->AllowDVNavigation = true;

	// hook: ventas_init
	$render = true;
	if(function_exists('ventas_init')) {
		$args = [];
		$render = ventas_init($x, getMemberInfo(), $args);
	}

	if($render) $x->Render();

	// column sums
	if(strpos($x->HTML, '<!-- tv data below -->')) {
		// if printing multi-selection TV, calculate the sum only for the selected records
		if(isset($_REQUEST['Print_x']) && is_array($_REQUEST['record_selector'])) {
			$QueryWhere = '';
			foreach($_REQUEST['record_selector'] as $id) {   // get selected records
				if($id != '') $QueryWhere .= "'" . makeSafe($id) . "',";
			}
			if($QueryWhere != '') {
				$QueryWhere = 'where `ventas`.`numero_contrato` in ('.substr($QueryWhere, 0, -1).')';
			} else { // if no selected records, write the where clause to return an empty result
				$QueryWhere = 'where 1=0';
			}
		} else {
			$QueryWhere = $x->QueryWhere;
		}

		$sumQuery = "SELECT SUM(`ventas`.`valor_contrato`), SUM(`ventas`.`cantidad_productos`) FROM {$x->QueryFrom} {$QueryWhere}";
		$res = sql($sumQuery, $eo);
		if($row = db_fetch_row($res)) {
			$sumRow = '<tr class="success sum">';
			if(!isset($_REQUEST['Print_x'])) $sumRow .= '<th class="text-center sum">&sum;</th>';
			$sumRow .= '<td class="ventas-id sum"></td>';
			$sumRow .= '<td class="ventas-vendedor sum"></td>';
			$sumRow .= '<td class="ventas-fecha_ingreso sum"></td>';
			$sumRow .= '<td class="ventas-fecha sum"></td>';
			$sumRow .= '<td class="ventas-numero_contrato sum"></td>';
			$sumRow .= '<td class="ventas-cedula sum"></td>';
			$sumRow .= '<td class="ventas-nombre sum"></td>';
			$sumRow .= '<td class="ventas-mes_comision_venta sum"></td>';
			$sumRow .= "<td class=\"ventas-valor_contrato text-right sum\">{$row[0]}</td>";
			$sumRow .= '<td class="ventas-pvp sum"></td>';
			$sumRow .= '<td class="ventas-a_medias sum"></td>';
			$sumRow .= '<td class="ventas-dependencia_laboral sum"></td>';
			$sumRow .= '<td class="ventas-aprobaciones sum"></td>';
			$sumRow .= '<td class="ventas-cobro_particular sum"></td>';
			$sumRow .= '<td class="ventas-entrega_cedula sum"></td>';
			$sumRow .= '<td class="ventas-cuotas sum"></td>';
			$sumRow .= '<td class="ventas-valor_cuota sum"></td>';
			$sumRow .= "<td class=\"ventas-cantidad_productos text-right sum\">{$row[1]}</td>";
			$sumRow .= '<td class="ventas-provincia sum"></td>';
			$sumRow .= '<td class="ventas-canton sum"></td>';
			$sumRow .= '<td class="ventas-parroquia sum"></td>';
			$sumRow .= '<td class="ventas-categorias_descripcion sum"></td>';
			$sumRow .= '<td class="ventas-tipo_venta sum"></td>';
			$sumRow .= '</tr>';

			$x->HTML = str_replace('<!-- tv data below -->', '', $x->HTML);
			$x->HTML = str_replace('<!-- tv data above -->', $sumRow, $x->HTML);
		}
	}

	// hook: ventas_header
	$headerCode = '';
	if(function_exists('ventas_header')) {
		$args = [];
		$headerCode = ventas_header($x->ContentType, getMemberInfo(), $args);
	}

	if(!$headerCode) {
		include_once("{$currDir}/header.php"); 
	} else {
		ob_start();
		include_once("{$currDir}/header.php");
		echo str_replace('<%%HEADER%%>', ob_get_clean(), $headerCode);
	}

	echo $x->HTML;

	// hook: ventas_footer
	$footerCode = '';
	if(function_exists('ventas_footer')) {
		$args = [];
		$footerCode = ventas_footer($x->ContentType, getMemberInfo(), $args);
	}

	if(!$footerCode) {
		include_once("{$currDir}/footer.php"); 
	} else {
		ob_start();
		include_once("{$currDir}/footer.php");
		echo str_replace('<%%FOOTER%%>', ob_get_clean(), $footerCode);
	}
