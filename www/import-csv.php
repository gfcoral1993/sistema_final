<?php
	define('PREPEND_PATH', '');
	$app_dir = dirname(__FILE__);
	include_once("{$app_dir}/lib.php");

	// accept a record as an assoc array, return transformed row ready to insert to table
	$transformFunctions = [
		'bitacora' => function($data, $options = []) {

			return $data;
		},
		'provincia' => function($data, $options = []) {
			if(isset($data['fecha_creacion_pr'])) $data['fecha_creacion_pr'] = guessMySQLDateTime($data['fecha_creacion_pr']);

			return $data;
		},
		'canton' => function($data, $options = []) {
			if(isset($data['fecha_creacion_c'])) $data['fecha_creacion_c'] = guessMySQLDateTime($data['fecha_creacion_c']);
			if(isset($data['codigo_provincia_c'])) $data['codigo_provincia_c'] = pkGivenLookupText($data['codigo_provincia_c'], 'canton', 'codigo_provincia_c');

			return $data;
		},
		'parroquia' => function($data, $options = []) {
			if(isset($data['fecha_creacion_pa'])) $data['fecha_creacion_pa'] = guessMySQLDateTime($data['fecha_creacion_pa']);
			if(isset($data['codigo_canton_pa'])) $data['codigo_canton_pa'] = pkGivenLookupText($data['codigo_canton_pa'], 'parroquia', 'codigo_canton_pa');

			return $data;
		},
		'dependencia_laboral' => function($data, $options = []) {

			return $data;
		},
		'tipo_producto' => function($data, $options = []) {
			if(isset($data['fecha_creacion_tp'])) $data['fecha_creacion_tp'] = guessMySQLDateTime($data['fecha_creacion_tp']);

			return $data;
		},
		'vendedor' => function($data, $options = []) {
			if(isset($data['fecha_registro'])) $data['fecha_registro'] = guessMySQLDateTime($data['fecha_registro']);

			return $data;
		},
		'tipo_movimiento' => function($data, $options = []) {

			return $data;
		},
		'aprobaciones' => function($data, $options = []) {

			return $data;
		},
		'cliente' => function($data, $options = []) {

			return $data;
		},
		'producto' => function($data, $options = []) {
			if(isset($data['fecha_creacion_p'])) $data['fecha_creacion_p'] = guessMySQLDateTime($data['fecha_creacion_p']);
			if(isset($data['tipo_producto_p'])) $data['tipo_producto_p'] = pkGivenLookupText($data['tipo_producto_p'], 'producto', 'tipo_producto_p');

			return $data;
		},
		'ventas' => function($data, $options = []) {
			if(isset($data['vendedor'])) $data['vendedor'] = pkGivenLookupText($data['vendedor'], 'ventas', 'vendedor');
			if(isset($data['fecha_ingreso'])) $data['fecha_ingreso'] = guessMySQLDateTime($data['fecha_ingreso']);
			if(isset($data['fecha_actualizacion'])) $data['fecha_actualizacion'] = guessMySQLDateTime($data['fecha_actualizacion']);
			if(isset($data['fecha'])) $data['fecha'] = guessMySQLDateTime($data['fecha']);
			if(isset($data['a_medias'])) $data['a_medias'] = pkGivenLookupText($data['a_medias'], 'ventas', 'a_medias');
			if(isset($data['dependencia_laboral'])) $data['dependencia_laboral'] = pkGivenLookupText($data['dependencia_laboral'], 'ventas', 'dependencia_laboral');
			if(isset($data['aprobaciones'])) $data['aprobaciones'] = pkGivenLookupText($data['aprobaciones'], 'ventas', 'aprobaciones');
			if(isset($data['provincia'])) $data['provincia'] = pkGivenLookupText($data['provincia'], 'ventas', 'provincia');
			if(isset($data['canton'])) $data['canton'] = pkGivenLookupText($data['canton'], 'ventas', 'canton');
			if(isset($data['parroquia'])) $data['parroquia'] = pkGivenLookupText($data['parroquia'], 'ventas', 'parroquia');
			if(isset($data['tipo_venta'])) $data['tipo_venta'] = pkGivenLookupText($data['tipo_venta'], 'ventas', 'tipo_venta');

			return $data;
		},
		'producto_venta' => function($data, $options = []) {
			if(isset($data['id_venta'])) $data['id_venta'] = pkGivenLookupText($data['id_venta'], 'producto_venta', 'id_venta');
			if(isset($data['id_producto'])) $data['id_producto'] = pkGivenLookupText($data['id_producto'], 'producto_venta', 'id_producto');

			return $data;
		},
		'nota_pedido' => function($data, $options = []) {
			if(isset($data['fecha_ingreso'])) $data['fecha_ingreso'] = guessMySQLDateTime($data['fecha_ingreso']);
			if(isset($data['vendedor'])) $data['vendedor'] = pkGivenLookupText($data['vendedor'], 'nota_pedido', 'vendedor');
			if(isset($data['fecha'])) $data['fecha'] = guessMySQLDateTime($data['fecha']);

			return $data;
		},
		'producto_nota_pedido' => function($data, $options = []) {
			if(isset($data['id_nota_pedido'])) $data['id_nota_pedido'] = pkGivenLookupText($data['id_nota_pedido'], 'producto_nota_pedido', 'id_nota_pedido');
			if(isset($data['id_producto'])) $data['id_producto'] = pkGivenLookupText($data['id_producto'], 'producto_nota_pedido', 'id_producto');

			return $data;
		},
		'nota_devolucion' => function($data, $options = []) {
			if(isset($data['fecha_ingreso'])) $data['fecha_ingreso'] = guessMySQLDateTime($data['fecha_ingreso']);
			if(isset($data['vendedor'])) $data['vendedor'] = pkGivenLookupText($data['vendedor'], 'nota_devolucion', 'vendedor');
			if(isset($data['fecha'])) $data['fecha'] = guessMySQLDateTime($data['fecha']);

			return $data;
		},
		'producto_nota_devolucion' => function($data, $options = []) {
			if(isset($data['id_nota_devolucion'])) $data['id_nota_devolucion'] = pkGivenLookupText($data['id_nota_devolucion'], 'producto_nota_devolucion', 'id_nota_devolucion');
			if(isset($data['id_producto'])) $data['id_producto'] = pkGivenLookupText($data['id_producto'], 'producto_nota_devolucion', 'id_producto');

			return $data;
		},
		'stock_general' => function($data, $options = []) {
			if(isset($data['fecha_ingreso'])) $data['fecha_ingreso'] = guessMySQLDateTime($data['fecha_ingreso']);
			if(isset($data['fecha_actualizacion'])) $data['fecha_actualizacion'] = guessMySQLDateTime($data['fecha_actualizacion']);
			if(isset($data['vendedor'])) $data['vendedor'] = pkGivenLookupText($data['vendedor'], 'stock_general', 'vendedor');
			if(isset($data['id_producto'])) $data['id_producto'] = pkGivenLookupText($data['id_producto'], 'stock_general', 'id_producto');
			if(isset($data['codigo_producto'])) $data['codigo_producto'] = pkGivenLookupText($data['codigo_producto'], 'stock_general', 'codigo_producto');
			if(isset($data['tipo_producto'])) $data['tipo_producto'] = pkGivenLookupText($data['tipo_producto'], 'stock_general', 'tipo_producto');
			if(isset($data['nombre_producto'])) $data['nombre_producto'] = pkGivenLookupText($data['nombre_producto'], 'stock_general', 'nombre_producto');

			return $data;
		},
		'control_stock' => function($data, $options = []) {
			if(isset($data['fecha_creacion'])) $data['fecha_creacion'] = guessMySQLDateTime($data['fecha_creacion']);
			if(isset($data['fecha_movimiento'])) $data['fecha_movimiento'] = guessMySQLDateTime($data['fecha_movimiento']);
			if(isset($data['tipo_movimiento'])) $data['tipo_movimiento'] = pkGivenLookupText($data['tipo_movimiento'], 'control_stock', 'tipo_movimiento');
			if(isset($data['vendedor'])) $data['vendedor'] = pkGivenLookupText($data['vendedor'], 'control_stock', 'vendedor');
			if(isset($data['categoria_producto'])) $data['categoria_producto'] = pkGivenLookupText($data['categoria_producto'], 'control_stock', 'categoria_producto');
			if(isset($data['producto'])) $data['producto'] = pkGivenLookupText($data['producto'], 'control_stock', 'producto');
			if(isset($data['codigo_producto'])) $data['codigo_producto'] = thisOr($data['producto'], pkGivenLookupText($data['codigo_producto'], 'control_stock', 'codigo_producto'));

			return $data;
		},
		'guia_remision' => function($data, $options = []) {
			if(isset($data['fecha_creacion_guia_remision'])) $data['fecha_creacion_guia_remision'] = guessMySQLDateTime($data['fecha_creacion_guia_remision']);
			if(isset($data['fecha_actualizacion'])) $data['fecha_actualizacion'] = guessMySQLDateTime($data['fecha_actualizacion']);
			if(isset($data['fecha_inicio_traslado'])) $data['fecha_inicio_traslado'] = guessMySQLDateTime($data['fecha_inicio_traslado']);
			if(isset($data['fecha_terminacion_traslado'])) $data['fecha_terminacion_traslado'] = guessMySQLDateTime($data['fecha_terminacion_traslado']);
			if(isset($data['punto_partida'])) $data['punto_partida'] = pkGivenLookupText($data['punto_partida'], 'guia_remision', 'punto_partida');
			if(isset($data['punto_llegada'])) $data['punto_llegada'] = pkGivenLookupText($data['punto_llegada'], 'guia_remision', 'punto_llegada');
			if(isset($data['punto_llegada_canton'])) $data['punto_llegada_canton'] = pkGivenLookupText($data['punto_llegada_canton'], 'guia_remision', 'punto_llegada_canton');
			if(isset($data['vendedor'])) $data['vendedor'] = pkGivenLookupText($data['vendedor'], 'guia_remision', 'vendedor');
			if(isset($data['nota_pedido'])) $data['nota_pedido'] = pkGivenLookupText($data['nota_pedido'], 'guia_remision', 'nota_pedido');

			return $data;
		},
		'producto_guia_remision' => function($data, $options = []) {
			if(isset($data['id_producto'])) $data['id_producto'] = pkGivenLookupText($data['id_producto'], 'producto_guia_remision', 'id_producto');
			if(isset($data['vendedor'])) $data['vendedor'] = pkGivenLookupText($data['vendedor'], 'producto_guia_remision', 'vendedor');
			if(isset($data['codigo_producto'])) $data['codigo_producto'] = thisOr($data['id_producto'], pkGivenLookupText($data['codigo_producto'], 'producto_guia_remision', 'codigo_producto'));

			return $data;
		},
		'tipo_venta' => function($data, $options = []) {

			return $data;
		},
		'inventario_bodega_central' => function($data, $options = []) {
			if(isset($data['codigo_producto'])) $data['codigo_producto'] = pkGivenLookupText($data['codigo_producto'], 'inventario_bodega_central', 'codigo_producto');

			return $data;
		},
		'compras' => function($data, $options = []) {
			if(isset($data['ruc'])) $data['ruc'] = pkGivenLookupText($data['ruc'], 'compras', 'ruc');

			return $data;
		},
		'compra_producto' => function($data, $options = []) {
			if(isset($data['compra_compra_producto'])) $data['compra_compra_producto'] = pkGivenLookupText($data['compra_compra_producto'], 'compra_producto', 'compra_compra_producto');
			if(isset($data['producto_compra_producto'])) $data['producto_compra_producto'] = pkGivenLookupText($data['producto_compra_producto'], 'compra_producto', 'producto_compra_producto');
			if(isset($data['nombre_producto_compra_producto'])) $data['nombre_producto_compra_producto'] = thisOr($data['producto_compra_producto'], pkGivenLookupText($data['nombre_producto_compra_producto'], 'compra_producto', 'nombre_producto_compra_producto'));

			return $data;
		},
		'ruc' => function($data, $options = []) {

			return $data;
		},
	];

	// accept a record as an assoc array, return a boolean indicating whether to import or skip record
	$filterFunctions = [
		'bitacora' => function($data, $options = []) { return true; },
		'provincia' => function($data, $options = []) { return true; },
		'canton' => function($data, $options = []) { return true; },
		'parroquia' => function($data, $options = []) { return true; },
		'dependencia_laboral' => function($data, $options = []) { return true; },
		'tipo_producto' => function($data, $options = []) { return true; },
		'vendedor' => function($data, $options = []) { return true; },
		'tipo_movimiento' => function($data, $options = []) { return true; },
		'aprobaciones' => function($data, $options = []) { return true; },
		'cliente' => function($data, $options = []) { return true; },
		'producto' => function($data, $options = []) { return true; },
		'ventas' => function($data, $options = []) { return true; },
		'producto_venta' => function($data, $options = []) { return true; },
		'nota_pedido' => function($data, $options = []) { return true; },
		'producto_nota_pedido' => function($data, $options = []) { return true; },
		'nota_devolucion' => function($data, $options = []) { return true; },
		'producto_nota_devolucion' => function($data, $options = []) { return true; },
		'stock_general' => function($data, $options = []) { return true; },
		'control_stock' => function($data, $options = []) { return true; },
		'guia_remision' => function($data, $options = []) { return true; },
		'producto_guia_remision' => function($data, $options = []) { return true; },
		'tipo_venta' => function($data, $options = []) { return true; },
		'inventario_bodega_central' => function($data, $options = []) { return true; },
		'compras' => function($data, $options = []) { return true; },
		'compra_producto' => function($data, $options = []) { return true; },
		'ruc' => function($data, $options = []) { return true; },
	];

	/*
	Hook file for overwriting/amending $transformFunctions and $filterFunctions:
	hooks/import-csv.php
	If found, it's included below

	The way this works is by either completely overwriting any of the above 2 arrays,
	or, more commonly, overwriting a single function, for example:
		$transformFunctions['tablename'] = function($data, $options = []) {
			// new definition here
			// then you must return transformed data
			return $data;
		};

	Another scenario is transforming a specific field and leaving other fields to the default
	transformation. One possible way of doing this is to store the original transformation function
	in GLOBALS array, calling it inside the custom transformation function, then modifying the
	specific field:
		$GLOBALS['originalTransformationFunction'] = $transformFunctions['tablename'];
		$transformFunctions['tablename'] = function($data, $options = []) {
			$data = call_user_func_array($GLOBALS['originalTransformationFunction'], [$data, $options]);
			$data['fieldname'] = 'transformed value';
			return $data;
		};
	*/

	@include("{$app_dir}/hooks/import-csv.php");

	$ui = new CSVImportUI($transformFunctions, $filterFunctions);
