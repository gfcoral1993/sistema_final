var FiltersEnabled = 0; // if your not going to use transitions or filters in any of the tips set this to 0
var spacer="&nbsp; &nbsp; &nbsp; ";

// email notifications to admin
notifyAdminNewMembers0Tip=["", spacer+"No email notifications to admin."];
notifyAdminNewMembers1Tip=["", spacer+"Notify admin only when a new member is waiting for approval."];
notifyAdminNewMembers2Tip=["", spacer+"Notify admin for all new sign-ups."];

// visitorSignup
visitorSignup0Tip=["", spacer+"If this option is selected, visitors will not be able to join this group unless the admin manually moves them to this group from the admin area."];
visitorSignup1Tip=["", spacer+"If this option is selected, visitors can join this group but will not be able to sign in unless the admin approves them from the admin area."];
visitorSignup2Tip=["", spacer+"If this option is selected, visitors can join this group and will be able to sign in instantly with no need for admin approval."];

// bitacora table
bitacora_addTip=["",spacer+"This option allows all members of the group to add records to the 'BITACORA' table. A member who adds a record to the table becomes the 'owner' of that record."];

bitacora_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'BITACORA' table."];
bitacora_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'BITACORA' table."];
bitacora_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'BITACORA' table."];
bitacora_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'BITACORA' table."];

bitacora_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'BITACORA' table."];
bitacora_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'BITACORA' table."];
bitacora_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'BITACORA' table."];
bitacora_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'BITACORA' table, regardless of their owner."];

bitacora_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'BITACORA' table."];
bitacora_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'BITACORA' table."];
bitacora_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'BITACORA' table."];
bitacora_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'BITACORA' table."];

// provincia table
provincia_addTip=["",spacer+"This option allows all members of the group to add records to the 'PROVINCIA' table. A member who adds a record to the table becomes the 'owner' of that record."];

provincia_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'PROVINCIA' table."];
provincia_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'PROVINCIA' table."];
provincia_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'PROVINCIA' table."];
provincia_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'PROVINCIA' table."];

provincia_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'PROVINCIA' table."];
provincia_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'PROVINCIA' table."];
provincia_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'PROVINCIA' table."];
provincia_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'PROVINCIA' table, regardless of their owner."];

provincia_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'PROVINCIA' table."];
provincia_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'PROVINCIA' table."];
provincia_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'PROVINCIA' table."];
provincia_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'PROVINCIA' table."];

// canton table
canton_addTip=["",spacer+"This option allows all members of the group to add records to the 'CANT&#211;N' table. A member who adds a record to the table becomes the 'owner' of that record."];

canton_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'CANT&#211;N' table."];
canton_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'CANT&#211;N' table."];
canton_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'CANT&#211;N' table."];
canton_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'CANT&#211;N' table."];

canton_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'CANT&#211;N' table."];
canton_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'CANT&#211;N' table."];
canton_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'CANT&#211;N' table."];
canton_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'CANT&#211;N' table, regardless of their owner."];

canton_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'CANT&#211;N' table."];
canton_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'CANT&#211;N' table."];
canton_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'CANT&#211;N' table."];
canton_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'CANT&#211;N' table."];

// parroquia table
parroquia_addTip=["",spacer+"This option allows all members of the group to add records to the 'PARROQUIA' table. A member who adds a record to the table becomes the 'owner' of that record."];

parroquia_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'PARROQUIA' table."];
parroquia_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'PARROQUIA' table."];
parroquia_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'PARROQUIA' table."];
parroquia_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'PARROQUIA' table."];

parroquia_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'PARROQUIA' table."];
parroquia_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'PARROQUIA' table."];
parroquia_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'PARROQUIA' table."];
parroquia_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'PARROQUIA' table, regardless of their owner."];

parroquia_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'PARROQUIA' table."];
parroquia_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'PARROQUIA' table."];
parroquia_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'PARROQUIA' table."];
parroquia_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'PARROQUIA' table."];

// dependencia_laboral table
dependencia_laboral_addTip=["",spacer+"This option allows all members of the group to add records to the 'DEPENDENCIA LABORAL' table. A member who adds a record to the table becomes the 'owner' of that record."];

dependencia_laboral_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'DEPENDENCIA LABORAL' table."];
dependencia_laboral_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'DEPENDENCIA LABORAL' table."];
dependencia_laboral_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'DEPENDENCIA LABORAL' table."];
dependencia_laboral_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'DEPENDENCIA LABORAL' table."];

dependencia_laboral_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'DEPENDENCIA LABORAL' table."];
dependencia_laboral_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'DEPENDENCIA LABORAL' table."];
dependencia_laboral_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'DEPENDENCIA LABORAL' table."];
dependencia_laboral_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'DEPENDENCIA LABORAL' table, regardless of their owner."];

dependencia_laboral_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'DEPENDENCIA LABORAL' table."];
dependencia_laboral_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'DEPENDENCIA LABORAL' table."];
dependencia_laboral_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'DEPENDENCIA LABORAL' table."];
dependencia_laboral_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'DEPENDENCIA LABORAL' table."];

// tipo_producto table
tipo_producto_addTip=["",spacer+"This option allows all members of the group to add records to the 'TIPO DE PRODUCTO' table. A member who adds a record to the table becomes the 'owner' of that record."];

tipo_producto_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'TIPO DE PRODUCTO' table."];
tipo_producto_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'TIPO DE PRODUCTO' table."];
tipo_producto_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'TIPO DE PRODUCTO' table."];
tipo_producto_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'TIPO DE PRODUCTO' table."];

tipo_producto_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'TIPO DE PRODUCTO' table."];
tipo_producto_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'TIPO DE PRODUCTO' table."];
tipo_producto_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'TIPO DE PRODUCTO' table."];
tipo_producto_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'TIPO DE PRODUCTO' table, regardless of their owner."];

tipo_producto_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'TIPO DE PRODUCTO' table."];
tipo_producto_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'TIPO DE PRODUCTO' table."];
tipo_producto_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'TIPO DE PRODUCTO' table."];
tipo_producto_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'TIPO DE PRODUCTO' table."];

// vendedor table
vendedor_addTip=["",spacer+"This option allows all members of the group to add records to the 'VENDEDORES' table. A member who adds a record to the table becomes the 'owner' of that record."];

vendedor_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'VENDEDORES' table."];
vendedor_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'VENDEDORES' table."];
vendedor_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'VENDEDORES' table."];
vendedor_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'VENDEDORES' table."];

vendedor_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'VENDEDORES' table."];
vendedor_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'VENDEDORES' table."];
vendedor_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'VENDEDORES' table."];
vendedor_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'VENDEDORES' table, regardless of their owner."];

vendedor_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'VENDEDORES' table."];
vendedor_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'VENDEDORES' table."];
vendedor_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'VENDEDORES' table."];
vendedor_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'VENDEDORES' table."];

// tipo_movimiento table
tipo_movimiento_addTip=["",spacer+"This option allows all members of the group to add records to the 'TIPO DE MOVIMIENTO' table. A member who adds a record to the table becomes the 'owner' of that record."];

tipo_movimiento_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'TIPO DE MOVIMIENTO' table."];
tipo_movimiento_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'TIPO DE MOVIMIENTO' table."];
tipo_movimiento_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'TIPO DE MOVIMIENTO' table."];
tipo_movimiento_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'TIPO DE MOVIMIENTO' table."];

tipo_movimiento_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'TIPO DE MOVIMIENTO' table."];
tipo_movimiento_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'TIPO DE MOVIMIENTO' table."];
tipo_movimiento_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'TIPO DE MOVIMIENTO' table."];
tipo_movimiento_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'TIPO DE MOVIMIENTO' table, regardless of their owner."];

tipo_movimiento_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'TIPO DE MOVIMIENTO' table."];
tipo_movimiento_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'TIPO DE MOVIMIENTO' table."];
tipo_movimiento_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'TIPO DE MOVIMIENTO' table."];
tipo_movimiento_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'TIPO DE MOVIMIENTO' table."];

// aprobaciones table
aprobaciones_addTip=["",spacer+"This option allows all members of the group to add records to the 'APROBACIONES' table. A member who adds a record to the table becomes the 'owner' of that record."];

aprobaciones_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'APROBACIONES' table."];
aprobaciones_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'APROBACIONES' table."];
aprobaciones_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'APROBACIONES' table."];
aprobaciones_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'APROBACIONES' table."];

aprobaciones_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'APROBACIONES' table."];
aprobaciones_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'APROBACIONES' table."];
aprobaciones_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'APROBACIONES' table."];
aprobaciones_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'APROBACIONES' table, regardless of their owner."];

aprobaciones_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'APROBACIONES' table."];
aprobaciones_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'APROBACIONES' table."];
aprobaciones_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'APROBACIONES' table."];
aprobaciones_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'APROBACIONES' table."];

// cliente table
cliente_addTip=["",spacer+"This option allows all members of the group to add records to the 'CLIENTE' table. A member who adds a record to the table becomes the 'owner' of that record."];

cliente_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'CLIENTE' table."];
cliente_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'CLIENTE' table."];
cliente_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'CLIENTE' table."];
cliente_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'CLIENTE' table."];

cliente_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'CLIENTE' table."];
cliente_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'CLIENTE' table."];
cliente_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'CLIENTE' table."];
cliente_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'CLIENTE' table, regardless of their owner."];

cliente_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'CLIENTE' table."];
cliente_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'CLIENTE' table."];
cliente_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'CLIENTE' table."];
cliente_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'CLIENTE' table."];

// producto table
producto_addTip=["",spacer+"This option allows all members of the group to add records to the 'PRODUCTO' table. A member who adds a record to the table becomes the 'owner' of that record."];

producto_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'PRODUCTO' table."];
producto_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'PRODUCTO' table."];
producto_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'PRODUCTO' table."];
producto_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'PRODUCTO' table."];

producto_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'PRODUCTO' table."];
producto_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'PRODUCTO' table."];
producto_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'PRODUCTO' table."];
producto_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'PRODUCTO' table, regardless of their owner."];

producto_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'PRODUCTO' table."];
producto_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'PRODUCTO' table."];
producto_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'PRODUCTO' table."];
producto_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'PRODUCTO' table."];

// ventas table
ventas_addTip=["",spacer+"This option allows all members of the group to add records to the 'VENTAS' table. A member who adds a record to the table becomes the 'owner' of that record."];

ventas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'VENTAS' table."];
ventas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'VENTAS' table."];
ventas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'VENTAS' table."];
ventas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'VENTAS' table."];

ventas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'VENTAS' table."];
ventas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'VENTAS' table."];
ventas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'VENTAS' table."];
ventas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'VENTAS' table, regardless of their owner."];

ventas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'VENTAS' table."];
ventas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'VENTAS' table."];
ventas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'VENTAS' table."];
ventas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'VENTAS' table."];

// producto_venta table
producto_venta_addTip=["",spacer+"This option allows all members of the group to add records to the 'PRODUCTO VS VENTAS' table. A member who adds a record to the table becomes the 'owner' of that record."];

producto_venta_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'PRODUCTO VS VENTAS' table."];
producto_venta_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'PRODUCTO VS VENTAS' table."];
producto_venta_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'PRODUCTO VS VENTAS' table."];
producto_venta_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'PRODUCTO VS VENTAS' table."];

producto_venta_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'PRODUCTO VS VENTAS' table."];
producto_venta_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'PRODUCTO VS VENTAS' table."];
producto_venta_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'PRODUCTO VS VENTAS' table."];
producto_venta_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'PRODUCTO VS VENTAS' table, regardless of their owner."];

producto_venta_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'PRODUCTO VS VENTAS' table."];
producto_venta_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'PRODUCTO VS VENTAS' table."];
producto_venta_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'PRODUCTO VS VENTAS' table."];
producto_venta_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'PRODUCTO VS VENTAS' table."];

// nota_pedido table
nota_pedido_addTip=["",spacer+"This option allows all members of the group to add records to the 'NOTA DE PEDIDO' table. A member who adds a record to the table becomes the 'owner' of that record."];

nota_pedido_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'NOTA DE PEDIDO' table."];
nota_pedido_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'NOTA DE PEDIDO' table."];
nota_pedido_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'NOTA DE PEDIDO' table."];
nota_pedido_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'NOTA DE PEDIDO' table."];

nota_pedido_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'NOTA DE PEDIDO' table."];
nota_pedido_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'NOTA DE PEDIDO' table."];
nota_pedido_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'NOTA DE PEDIDO' table."];
nota_pedido_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'NOTA DE PEDIDO' table, regardless of their owner."];

nota_pedido_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'NOTA DE PEDIDO' table."];
nota_pedido_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'NOTA DE PEDIDO' table."];
nota_pedido_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'NOTA DE PEDIDO' table."];
nota_pedido_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'NOTA DE PEDIDO' table."];

// producto_nota_pedido table
producto_nota_pedido_addTip=["",spacer+"This option allows all members of the group to add records to the 'PROD. VS NOTA PEDIDO' table. A member who adds a record to the table becomes the 'owner' of that record."];

producto_nota_pedido_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'PROD. VS NOTA PEDIDO' table."];
producto_nota_pedido_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'PROD. VS NOTA PEDIDO' table."];
producto_nota_pedido_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'PROD. VS NOTA PEDIDO' table."];
producto_nota_pedido_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'PROD. VS NOTA PEDIDO' table."];

producto_nota_pedido_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'PROD. VS NOTA PEDIDO' table."];
producto_nota_pedido_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'PROD. VS NOTA PEDIDO' table."];
producto_nota_pedido_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'PROD. VS NOTA PEDIDO' table."];
producto_nota_pedido_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'PROD. VS NOTA PEDIDO' table, regardless of their owner."];

producto_nota_pedido_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'PROD. VS NOTA PEDIDO' table."];
producto_nota_pedido_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'PROD. VS NOTA PEDIDO' table."];
producto_nota_pedido_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'PROD. VS NOTA PEDIDO' table."];
producto_nota_pedido_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'PROD. VS NOTA PEDIDO' table."];

// nota_devolucion table
nota_devolucion_addTip=["",spacer+"This option allows all members of the group to add records to the 'DEVOLUCIONES' table. A member who adds a record to the table becomes the 'owner' of that record."];

nota_devolucion_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'DEVOLUCIONES' table."];
nota_devolucion_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'DEVOLUCIONES' table."];
nota_devolucion_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'DEVOLUCIONES' table."];
nota_devolucion_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'DEVOLUCIONES' table."];

nota_devolucion_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'DEVOLUCIONES' table."];
nota_devolucion_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'DEVOLUCIONES' table."];
nota_devolucion_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'DEVOLUCIONES' table."];
nota_devolucion_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'DEVOLUCIONES' table, regardless of their owner."];

nota_devolucion_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'DEVOLUCIONES' table."];
nota_devolucion_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'DEVOLUCIONES' table."];
nota_devolucion_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'DEVOLUCIONES' table."];
nota_devolucion_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'DEVOLUCIONES' table."];

// producto_nota_devolucion table
producto_nota_devolucion_addTip=["",spacer+"This option allows all members of the group to add records to the 'PROD. VS NOTA DEVOLUCI&#211;N' table. A member who adds a record to the table becomes the 'owner' of that record."];

producto_nota_devolucion_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];
producto_nota_devolucion_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];
producto_nota_devolucion_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];
producto_nota_devolucion_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];

producto_nota_devolucion_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];
producto_nota_devolucion_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];
producto_nota_devolucion_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];
producto_nota_devolucion_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'PROD. VS NOTA DEVOLUCI&#211;N' table, regardless of their owner."];

producto_nota_devolucion_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];
producto_nota_devolucion_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];
producto_nota_devolucion_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];
producto_nota_devolucion_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'PROD. VS NOTA DEVOLUCI&#211;N' table."];

// stock_general table
stock_general_addTip=["",spacer+"This option allows all members of the group to add records to the 'STOCK GENERAL' table. A member who adds a record to the table becomes the 'owner' of that record."];

stock_general_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'STOCK GENERAL' table."];
stock_general_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'STOCK GENERAL' table."];
stock_general_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'STOCK GENERAL' table."];
stock_general_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'STOCK GENERAL' table."];

stock_general_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'STOCK GENERAL' table."];
stock_general_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'STOCK GENERAL' table."];
stock_general_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'STOCK GENERAL' table."];
stock_general_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'STOCK GENERAL' table, regardless of their owner."];

stock_general_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'STOCK GENERAL' table."];
stock_general_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'STOCK GENERAL' table."];
stock_general_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'STOCK GENERAL' table."];
stock_general_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'STOCK GENERAL' table."];

// control_stock table
control_stock_addTip=["",spacer+"This option allows all members of the group to add records to the 'CONTROL STOCK' table. A member who adds a record to the table becomes the 'owner' of that record."];

control_stock_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'CONTROL STOCK' table."];
control_stock_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'CONTROL STOCK' table."];
control_stock_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'CONTROL STOCK' table."];
control_stock_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'CONTROL STOCK' table."];

control_stock_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'CONTROL STOCK' table."];
control_stock_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'CONTROL STOCK' table."];
control_stock_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'CONTROL STOCK' table."];
control_stock_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'CONTROL STOCK' table, regardless of their owner."];

control_stock_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'CONTROL STOCK' table."];
control_stock_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'CONTROL STOCK' table."];
control_stock_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'CONTROL STOCK' table."];
control_stock_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'CONTROL STOCK' table."];

// guia_remision table
guia_remision_addTip=["",spacer+"This option allows all members of the group to add records to the 'GUIA DE REMISION' table. A member who adds a record to the table becomes the 'owner' of that record."];

guia_remision_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'GUIA DE REMISION' table."];
guia_remision_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'GUIA DE REMISION' table."];
guia_remision_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'GUIA DE REMISION' table."];
guia_remision_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'GUIA DE REMISION' table."];

guia_remision_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'GUIA DE REMISION' table."];
guia_remision_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'GUIA DE REMISION' table."];
guia_remision_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'GUIA DE REMISION' table."];
guia_remision_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'GUIA DE REMISION' table, regardless of their owner."];

guia_remision_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'GUIA DE REMISION' table."];
guia_remision_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'GUIA DE REMISION' table."];
guia_remision_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'GUIA DE REMISION' table."];
guia_remision_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'GUIA DE REMISION' table."];

// producto_guia_remision table
producto_guia_remision_addTip=["",spacer+"This option allows all members of the group to add records to the 'PROD VS GUIA REMISION' table. A member who adds a record to the table becomes the 'owner' of that record."];

producto_guia_remision_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'PROD VS GUIA REMISION' table."];
producto_guia_remision_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'PROD VS GUIA REMISION' table."];
producto_guia_remision_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'PROD VS GUIA REMISION' table."];
producto_guia_remision_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'PROD VS GUIA REMISION' table."];

producto_guia_remision_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'PROD VS GUIA REMISION' table."];
producto_guia_remision_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'PROD VS GUIA REMISION' table."];
producto_guia_remision_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'PROD VS GUIA REMISION' table."];
producto_guia_remision_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'PROD VS GUIA REMISION' table, regardless of their owner."];

producto_guia_remision_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'PROD VS GUIA REMISION' table."];
producto_guia_remision_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'PROD VS GUIA REMISION' table."];
producto_guia_remision_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'PROD VS GUIA REMISION' table."];
producto_guia_remision_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'PROD VS GUIA REMISION' table."];

// tipo_venta table
tipo_venta_addTip=["",spacer+"This option allows all members of the group to add records to the 'TIPO DE VENTA  ' table. A member who adds a record to the table becomes the 'owner' of that record."];

tipo_venta_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'TIPO DE VENTA  ' table."];
tipo_venta_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'TIPO DE VENTA  ' table."];
tipo_venta_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'TIPO DE VENTA  ' table."];
tipo_venta_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'TIPO DE VENTA  ' table."];

tipo_venta_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'TIPO DE VENTA  ' table."];
tipo_venta_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'TIPO DE VENTA  ' table."];
tipo_venta_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'TIPO DE VENTA  ' table."];
tipo_venta_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'TIPO DE VENTA  ' table, regardless of their owner."];

tipo_venta_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'TIPO DE VENTA  ' table."];
tipo_venta_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'TIPO DE VENTA  ' table."];
tipo_venta_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'TIPO DE VENTA  ' table."];
tipo_venta_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'TIPO DE VENTA  ' table."];

// inventario_bodega_central table
inventario_bodega_central_addTip=["",spacer+"This option allows all members of the group to add records to the 'INVENTARIO BC' table. A member who adds a record to the table becomes the 'owner' of that record."];

inventario_bodega_central_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'INVENTARIO BC' table."];
inventario_bodega_central_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'INVENTARIO BC' table."];
inventario_bodega_central_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'INVENTARIO BC' table."];
inventario_bodega_central_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'INVENTARIO BC' table."];

inventario_bodega_central_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'INVENTARIO BC' table."];
inventario_bodega_central_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'INVENTARIO BC' table."];
inventario_bodega_central_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'INVENTARIO BC' table."];
inventario_bodega_central_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'INVENTARIO BC' table, regardless of their owner."];

inventario_bodega_central_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'INVENTARIO BC' table."];
inventario_bodega_central_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'INVENTARIO BC' table."];
inventario_bodega_central_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'INVENTARIO BC' table."];
inventario_bodega_central_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'INVENTARIO BC' table."];

// compras table
compras_addTip=["",spacer+"This option allows all members of the group to add records to the 'COMPRAS' table. A member who adds a record to the table becomes the 'owner' of that record."];

compras_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'COMPRAS' table."];
compras_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'COMPRAS' table."];
compras_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'COMPRAS' table."];
compras_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'COMPRAS' table."];

compras_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'COMPRAS' table."];
compras_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'COMPRAS' table."];
compras_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'COMPRAS' table."];
compras_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'COMPRAS' table, regardless of their owner."];

compras_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'COMPRAS' table."];
compras_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'COMPRAS' table."];
compras_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'COMPRAS' table."];
compras_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'COMPRAS' table."];

// compra_producto table
compra_producto_addTip=["",spacer+"This option allows all members of the group to add records to the 'COMPRA VS PRODUCTO' table. A member who adds a record to the table becomes the 'owner' of that record."];

compra_producto_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'COMPRA VS PRODUCTO' table."];
compra_producto_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'COMPRA VS PRODUCTO' table."];
compra_producto_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'COMPRA VS PRODUCTO' table."];
compra_producto_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'COMPRA VS PRODUCTO' table."];

compra_producto_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'COMPRA VS PRODUCTO' table."];
compra_producto_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'COMPRA VS PRODUCTO' table."];
compra_producto_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'COMPRA VS PRODUCTO' table."];
compra_producto_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'COMPRA VS PRODUCTO' table, regardless of their owner."];

compra_producto_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'COMPRA VS PRODUCTO' table."];
compra_producto_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'COMPRA VS PRODUCTO' table."];
compra_producto_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'COMPRA VS PRODUCTO' table."];
compra_producto_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'COMPRA VS PRODUCTO' table."];

// ruc table
ruc_addTip=["",spacer+"This option allows all members of the group to add records to the 'Ruc' table. A member who adds a record to the table becomes the 'owner' of that record."];

ruc_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Ruc' table."];
ruc_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Ruc' table."];
ruc_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Ruc' table."];
ruc_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Ruc' table."];

ruc_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Ruc' table."];
ruc_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Ruc' table."];
ruc_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Ruc' table."];
ruc_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Ruc' table, regardless of their owner."];

ruc_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Ruc' table."];
ruc_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Ruc' table."];
ruc_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Ruc' table."];
ruc_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Ruc' table."];

/*
	Style syntax:
	-------------
	[TitleColor,TextColor,TitleBgColor,TextBgColor,TitleBgImag,TextBgImag,TitleTextAlign,
	TextTextAlign,TitleFontFace,TextFontFace, TipPosition, StickyStyle, TitleFontSize,
	TextFontSize, Width, Height, BorderSize, PadTextArea, CoordinateX , CoordinateY,
	TransitionNumber, TransitionDuration, TransparencyLevel ,ShadowType, ShadowColor]

*/

toolTipStyle=["white","#00008B","#000099","#E6E6FA","","images/helpBg.gif","","","","\"Trebuchet MS\", sans-serif","","","","3",400,"",1,2,10,10,51,1,0,"",""];

applyCssFilter();
