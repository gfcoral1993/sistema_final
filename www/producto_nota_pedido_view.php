<?php
// This script and data application were generated by AppGini 5.97
// Download AppGini for free from https://bigprof.com/appgini/download/

	$currDir = dirname(__FILE__);
	include_once("{$currDir}/lib.php");
	@include_once("{$currDir}/hooks/producto_nota_pedido.php");
	include_once("{$currDir}/producto_nota_pedido_dml.php");

	// mm: can the current member access this page?
	$perm = getTablePermissions('producto_nota_pedido');
	if(!$perm['access']) {
		echo error_message($Translation['tableAccessDenied']);
		exit;
	}

	$x = new DataList;
	$x->TableName = 'producto_nota_pedido';

	// Fields that can be displayed in the table view
	$x->QueryFieldsTV = [
		"`producto_nota_pedido`.`id`" => "id",
		"IF(    CHAR_LENGTH(`nota_pedido1`.`numero_guia`), CONCAT_WS('',   `nota_pedido1`.`numero_guia`), '') /* NOTA DE PEDIDO */" => "id_nota_pedido",
		"IF(    CHAR_LENGTH(`producto1`.`nombre_p`), CONCAT_WS('',   `producto1`.`nombre_p`), '') /* PRODUCTO */" => "id_producto",
		"`producto_nota_pedido`.`cantidad`" => "cantidad",
	];
	// mapping incoming sort by requests to actual query fields
	$x->SortFields = [
		1 => '`producto_nota_pedido`.`id`',
		2 => '`nota_pedido1`.`numero_guia`',
		3 => '`producto1`.`nombre_p`',
		4 => '`producto_nota_pedido`.`cantidad`',
	];

	// Fields that can be displayed in the csv file
	$x->QueryFieldsCSV = [
		"`producto_nota_pedido`.`id`" => "id",
		"IF(    CHAR_LENGTH(`nota_pedido1`.`numero_guia`), CONCAT_WS('',   `nota_pedido1`.`numero_guia`), '') /* NOTA DE PEDIDO */" => "id_nota_pedido",
		"IF(    CHAR_LENGTH(`producto1`.`nombre_p`), CONCAT_WS('',   `producto1`.`nombre_p`), '') /* PRODUCTO */" => "id_producto",
		"`producto_nota_pedido`.`cantidad`" => "cantidad",
	];
	// Fields that can be filtered
	$x->QueryFieldsFilters = [
		"`producto_nota_pedido`.`id`" => "ID",
		"IF(    CHAR_LENGTH(`nota_pedido1`.`numero_guia`), CONCAT_WS('',   `nota_pedido1`.`numero_guia`), '') /* NOTA DE PEDIDO */" => "NOTA DE PEDIDO",
		"IF(    CHAR_LENGTH(`producto1`.`nombre_p`), CONCAT_WS('',   `producto1`.`nombre_p`), '') /* PRODUCTO */" => "PRODUCTO",
		"`producto_nota_pedido`.`cantidad`" => "CANTIDAD",
	];

	// Fields that can be quick searched
	$x->QueryFieldsQS = [
		"`producto_nota_pedido`.`id`" => "id",
		"IF(    CHAR_LENGTH(`nota_pedido1`.`numero_guia`), CONCAT_WS('',   `nota_pedido1`.`numero_guia`), '') /* NOTA DE PEDIDO */" => "id_nota_pedido",
		"IF(    CHAR_LENGTH(`producto1`.`nombre_p`), CONCAT_WS('',   `producto1`.`nombre_p`), '') /* PRODUCTO */" => "id_producto",
		"`producto_nota_pedido`.`cantidad`" => "cantidad",
	];

	// Lookup fields that can be used as filterers
	$x->filterers = ['id_nota_pedido' => 'NOTA DE PEDIDO', 'id_producto' => 'PRODUCTO', ];

	$x->QueryFrom = "`producto_nota_pedido` LEFT JOIN `nota_pedido` as nota_pedido1 ON `nota_pedido1`.`numero_guia`=`producto_nota_pedido`.`id_nota_pedido` LEFT JOIN `producto` as producto1 ON `producto1`.`id_p`=`producto_nota_pedido`.`id_producto` ";
	$x->QueryWhere = '';
	$x->QueryOrder = '';

	$x->AllowSelection = 1;
	$x->HideTableView = ($perm['view'] == 0 ? 1 : 0);
	$x->AllowDelete = $perm['delete'];
	$x->AllowMassDelete = true;
	$x->AllowInsert = $perm['insert'];
	$x->AllowUpdate = $perm['edit'];
	$x->SeparateDV = 1;
	$x->AllowDeleteOfParents = 0;
	$x->AllowFilters = 1;
	$x->AllowSavingFilters = (getLoggedAdmin() !== false);
	$x->AllowSorting = 1;
	$x->AllowNavigation = 1;
	$x->AllowPrinting = 1;
	$x->AllowPrintingDV = 1;
	$x->AllowCSV = 1;
	$x->RecordsPerPage = 100;
	$x->QuickSearch = 1;
	$x->QuickSearchText = $Translation['quick search'];
	$x->ScriptFileName = 'producto_nota_pedido_view.php';
	$x->RedirectAfterInsert = 'producto_nota_pedido_view.php';
	$x->TableTitle = 'PROD. VS NOTA PEDIDO';
	$x->TableIcon = 'resources/table_icons/newspaper_add.png';
	$x->PrimaryKey = '`producto_nota_pedido`.`id`';
	$x->DefaultSortField = '1';
	$x->DefaultSortDirection = 'desc';

	$x->ColWidth = [150, 150, 150, 150, ];
	$x->ColCaption = ['ID', 'NOTA DE PEDIDO', 'PRODUCTO', 'CANTIDAD', ];
	$x->ColFieldName = ['id', 'id_nota_pedido', 'id_producto', 'cantidad', ];
	$x->ColNumber  = [1, 2, 3, 4, ];

	// template paths below are based on the app main directory
	$x->Template = 'templates/producto_nota_pedido_templateTV.html';
	$x->SelectedTemplate = 'templates/producto_nota_pedido_templateTVS.html';
	$x->TemplateDV = 'templates/producto_nota_pedido_templateDV.html';
	$x->TemplateDVP = 'templates/producto_nota_pedido_templateDVP.html';

	$x->ShowTableHeader = 1;
	$x->TVClasses = "";
	$x->DVClasses = "";
	$x->HasCalculatedFields = false;
	$x->AllowConsoleLog = false;
	$x->AllowDVNavigation = true;

	// hook: producto_nota_pedido_init
	$render = true;
	if(function_exists('producto_nota_pedido_init')) {
		$args = [];
		$render = producto_nota_pedido_init($x, getMemberInfo(), $args);
	}

	if($render) $x->Render();

	// column sums
	if(strpos($x->HTML, '<!-- tv data below -->')) {
		// if printing multi-selection TV, calculate the sum only for the selected records
		if(isset($_REQUEST['Print_x']) && is_array($_REQUEST['record_selector'])) {
			$QueryWhere = '';
			foreach($_REQUEST['record_selector'] as $id) {   // get selected records
				if($id != '') $QueryWhere .= "'" . makeSafe($id) . "',";
			}
			if($QueryWhere != '') {
				$QueryWhere = 'where `producto_nota_pedido`.`id` in ('.substr($QueryWhere, 0, -1).')';
			} else { // if no selected records, write the where clause to return an empty result
				$QueryWhere = 'where 1=0';
			}
		} else {
			$QueryWhere = $x->QueryWhere;
		}

		$sumQuery = "SELECT SUM(`producto_nota_pedido`.`cantidad`) FROM {$x->QueryFrom} {$QueryWhere}";
		$res = sql($sumQuery, $eo);
		if($row = db_fetch_row($res)) {
			$sumRow = '<tr class="success sum">';
			if(!isset($_REQUEST['Print_x'])) $sumRow .= '<th class="text-center sum">&sum;</th>';
			$sumRow .= '<td class="producto_nota_pedido-id sum"></td>';
			$sumRow .= '<td class="producto_nota_pedido-id_nota_pedido sum"></td>';
			$sumRow .= '<td class="producto_nota_pedido-id_producto sum"></td>';
			$sumRow .= "<td class=\"producto_nota_pedido-cantidad text-center sum\">{$row[0]}</td>";
			$sumRow .= '</tr>';

			$x->HTML = str_replace('<!-- tv data below -->', '', $x->HTML);
			$x->HTML = str_replace('<!-- tv data above -->', $sumRow, $x->HTML);
		}
	}

	// hook: producto_nota_pedido_header
	$headerCode = '';
	if(function_exists('producto_nota_pedido_header')) {
		$args = [];
		$headerCode = producto_nota_pedido_header($x->ContentType, getMemberInfo(), $args);
	}

	if(!$headerCode) {
		include_once("{$currDir}/header.php"); 
	} else {
		ob_start();
		include_once("{$currDir}/header.php");
		echo str_replace('<%%HEADER%%>', ob_get_clean(), $headerCode);
	}

	echo $x->HTML;

	// hook: producto_nota_pedido_footer
	$footerCode = '';
	if(function_exists('producto_nota_pedido_footer')) {
		$args = [];
		$footerCode = producto_nota_pedido_footer($x->ContentType, getMemberInfo(), $args);
	}

	if(!$footerCode) {
		include_once("{$currDir}/footer.php"); 
	} else {
		ob_start();
		include_once("{$currDir}/footer.php");
		echo str_replace('<%%FOOTER%%>', ob_get_clean(), $footerCode);
	}
