<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'producto_entrega_mercaderia';

		/* data for selected record, or defaults if none is selected */
		var data = {
			id_entrega_mercaderia: <?php echo json_encode(array('id' => $rdata['id_entrega_mercaderia'], 'value' => $rdata['id_entrega_mercaderia'], 'text' => $jdata['id_entrega_mercaderia'])); ?>,
			id_producto: <?php echo json_encode(array('id' => $rdata['id_producto'], 'value' => $rdata['id_producto'], 'text' => $jdata['id_producto'])); ?>,
			codigo_producto: <?php echo json_encode($jdata['codigo_producto']); ?>,
			vendedor: <?php echo json_encode(array('id' => $rdata['vendedor'], 'value' => $rdata['vendedor'], 'text' => $jdata['vendedor'])); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for id_entrega_mercaderia */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'id_entrega_mercaderia' && d.id == data.id_entrega_mercaderia.id)
				return { results: [ data.id_entrega_mercaderia ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for id_producto */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'id_producto' && d.id == data.id_producto.id)
				return { results: [ data.id_producto ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for id_producto autofills */
		cache.addCheck(function(u, d) {
			if(u != tn + '_autofill.php') return false;

			for(var rnd in d) if(rnd.match(/^rnd/)) break;

			if(d.mfk == 'id_producto' && d.id == data.id_producto.id) {
				$j('#codigo_producto' + d[rnd]).html(data.codigo_producto);
				return true;
			}

			return false;
		});

		/* saved value for vendedor */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'vendedor' && d.id == data.vendedor.id)
				return { results: [ data.vendedor ], more: false, elapsed: 0.01 };
			return false;
		});

		cache.start();
	});
</script>

