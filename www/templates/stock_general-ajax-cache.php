<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'stock_general';

		/* data for selected record, or defaults if none is selected */
		var data = {
			vendedor: <?php echo json_encode(array('id' => $rdata['vendedor'], 'value' => $rdata['vendedor'], 'text' => $jdata['vendedor'])); ?>,
			id_producto: <?php echo json_encode(array('id' => $rdata['id_producto'], 'value' => $rdata['id_producto'], 'text' => $jdata['id_producto'])); ?>,
			codigo_producto: <?php echo json_encode(array('id' => $rdata['codigo_producto'], 'value' => $rdata['codigo_producto'], 'text' => $jdata['codigo_producto'])); ?>,
			tipo_producto: <?php echo json_encode(array('id' => $rdata['tipo_producto'], 'value' => $rdata['tipo_producto'], 'text' => $jdata['tipo_producto'])); ?>,
			nombre_producto: <?php echo json_encode(array('id' => $rdata['nombre_producto'], 'value' => $rdata['nombre_producto'], 'text' => $jdata['nombre_producto'])); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for vendedor */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'vendedor' && d.id == data.vendedor.id)
				return { results: [ data.vendedor ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for id_producto */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'id_producto' && d.id == data.id_producto.id)
				return { results: [ data.id_producto ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for codigo_producto */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'codigo_producto' && d.id == data.codigo_producto.id)
				return { results: [ data.codigo_producto ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for tipo_producto */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'tipo_producto' && d.id == data.tipo_producto.id)
				return { results: [ data.tipo_producto ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for nombre_producto */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'nombre_producto' && d.id == data.nombre_producto.id)
				return { results: [ data.nombre_producto ], more: false, elapsed: 0.01 };
			return false;
		});

		cache.start();
	});
</script>

