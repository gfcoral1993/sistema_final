<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'ventas';

		/* data for selected record, or defaults if none is selected */
		var data = {
			vendedor: <?php echo json_encode(array('id' => $rdata['vendedor'], 'value' => $rdata['vendedor'], 'text' => $jdata['vendedor'])); ?>,
			a_medias: <?php echo json_encode(array('id' => $rdata['a_medias'], 'value' => $rdata['a_medias'], 'text' => $jdata['a_medias'])); ?>,
			dependencia_laboral: <?php echo json_encode(array('id' => $rdata['dependencia_laboral'], 'value' => $rdata['dependencia_laboral'], 'text' => $jdata['dependencia_laboral'])); ?>,
			aprobaciones: <?php echo json_encode(array('id' => $rdata['aprobaciones'], 'value' => $rdata['aprobaciones'], 'text' => $jdata['aprobaciones'])); ?>,
			provincia: <?php echo json_encode(array('id' => $rdata['provincia'], 'value' => $rdata['provincia'], 'text' => $jdata['provincia'])); ?>,
			canton: <?php echo json_encode(array('id' => $rdata['canton'], 'value' => $rdata['canton'], 'text' => $jdata['canton'])); ?>,
			parroquia: <?php echo json_encode(array('id' => $rdata['parroquia'], 'value' => $rdata['parroquia'], 'text' => $jdata['parroquia'])); ?>,
			tipo_venta: <?php echo json_encode(array('id' => $rdata['tipo_venta'], 'value' => $rdata['tipo_venta'], 'text' => $jdata['tipo_venta'])); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for vendedor */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'vendedor' && d.id == data.vendedor.id)
				return { results: [ data.vendedor ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for a_medias */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'a_medias' && d.id == data.a_medias.id)
				return { results: [ data.a_medias ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for dependencia_laboral */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'dependencia_laboral' && d.id == data.dependencia_laboral.id)
				return { results: [ data.dependencia_laboral ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for aprobaciones */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'aprobaciones' && d.id == data.aprobaciones.id)
				return { results: [ data.aprobaciones ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for provincia */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'provincia' && d.id == data.provincia.id)
				return { results: [ data.provincia ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for canton */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'canton' && d.id == data.canton.id)
				return { results: [ data.canton ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for parroquia */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'parroquia' && d.id == data.parroquia.id)
				return { results: [ data.parroquia ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for tipo_venta */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'tipo_venta' && d.id == data.tipo_venta.id)
				return { results: [ data.tipo_venta ], more: false, elapsed: 0.01 };
			return false;
		});

		cache.start();
	});
</script>

