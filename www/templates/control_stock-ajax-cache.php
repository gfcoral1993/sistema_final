<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'control_stock';

		/* data for selected record, or defaults if none is selected */
		var data = {
			tipo_movimiento: <?php echo json_encode(array('id' => $rdata['tipo_movimiento'], 'value' => $rdata['tipo_movimiento'], 'text' => $jdata['tipo_movimiento'])); ?>,
			vendedor: <?php echo json_encode(array('id' => $rdata['vendedor'], 'value' => $rdata['vendedor'], 'text' => $jdata['vendedor'])); ?>,
			categoria_producto: <?php echo json_encode(array('id' => $rdata['categoria_producto'], 'value' => $rdata['categoria_producto'], 'text' => $jdata['categoria_producto'])); ?>,
			producto: <?php echo json_encode(array('id' => $rdata['producto'], 'value' => $rdata['producto'], 'text' => $jdata['producto'])); ?>,
			codigo_producto: <?php echo json_encode($jdata['codigo_producto']); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for tipo_movimiento */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'tipo_movimiento' && d.id == data.tipo_movimiento.id)
				return { results: [ data.tipo_movimiento ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for vendedor */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'vendedor' && d.id == data.vendedor.id)
				return { results: [ data.vendedor ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for categoria_producto */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'categoria_producto' && d.id == data.categoria_producto.id)
				return { results: [ data.categoria_producto ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for producto */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'producto' && d.id == data.producto.id)
				return { results: [ data.producto ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for producto autofills */
		cache.addCheck(function(u, d) {
			if(u != tn + '_autofill.php') return false;

			for(var rnd in d) if(rnd.match(/^rnd/)) break;

			if(d.mfk == 'producto' && d.id == data.producto.id) {
				$j('#codigo_producto' + d[rnd]).html(data.codigo_producto);
				return true;
			}

			return false;
		});

		cache.start();
	});
</script>

