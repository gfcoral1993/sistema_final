<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'entrega_mercaderia';

		/* data for selected record, or defaults if none is selected */
		var data = {
			punto_llegada: <?php echo json_encode(array('id' => $rdata['punto_llegada'], 'value' => $rdata['punto_llegada'], 'text' => $jdata['punto_llegada'])); ?>,
			punto_llegada_canton: <?php echo json_encode(array('id' => $rdata['punto_llegada_canton'], 'value' => $rdata['punto_llegada_canton'], 'text' => $jdata['punto_llegada_canton'])); ?>,
			vendedor: <?php echo json_encode(array('id' => $rdata['vendedor'], 'value' => $rdata['vendedor'], 'text' => $jdata['vendedor'])); ?>,
			nota_pedido: <?php echo json_encode(array('id' => $rdata['nota_pedido'], 'value' => $rdata['nota_pedido'], 'text' => $jdata['nota_pedido'])); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for punto_llegada */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'punto_llegada' && d.id == data.punto_llegada.id)
				return { results: [ data.punto_llegada ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for punto_llegada_canton */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'punto_llegada_canton' && d.id == data.punto_llegada_canton.id)
				return { results: [ data.punto_llegada_canton ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for vendedor */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'vendedor' && d.id == data.vendedor.id)
				return { results: [ data.vendedor ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for nota_pedido */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'nota_pedido' && d.id == data.nota_pedido.id)
				return { results: [ data.nota_pedido ], more: false, elapsed: 0.01 };
			return false;
		});

		cache.start();
	});
</script>

