<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'producto_nota_devolucion';

		/* data for selected record, or defaults if none is selected */
		var data = {
			id_nota_devolucion: <?php echo json_encode(array('id' => $rdata['id_nota_devolucion'], 'value' => $rdata['id_nota_devolucion'], 'text' => $jdata['id_nota_devolucion'])); ?>,
			id_producto: <?php echo json_encode(array('id' => $rdata['id_producto'], 'value' => $rdata['id_producto'], 'text' => $jdata['id_producto'])); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for id_nota_devolucion */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'id_nota_devolucion' && d.id == data.id_nota_devolucion.id)
				return { results: [ data.id_nota_devolucion ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for id_producto */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'id_producto' && d.id == data.id_producto.id)
				return { results: [ data.id_producto ], more: false, elapsed: 0.01 };
			return false;
		});

		cache.start();
	});
</script>

