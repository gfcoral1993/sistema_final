<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'compra_producto';

		/* data for selected record, or defaults if none is selected */
		var data = {
			compra_compra_producto: <?php echo json_encode(array('id' => $rdata['compra_compra_producto'], 'value' => $rdata['compra_compra_producto'], 'text' => $jdata['compra_compra_producto'])); ?>,
			producto_compra_producto: <?php echo json_encode(array('id' => $rdata['producto_compra_producto'], 'value' => $rdata['producto_compra_producto'], 'text' => $jdata['producto_compra_producto'])); ?>,
			nombre_producto_compra_producto: <?php echo json_encode($jdata['nombre_producto_compra_producto']); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for compra_compra_producto */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'compra_compra_producto' && d.id == data.compra_compra_producto.id)
				return { results: [ data.compra_compra_producto ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for producto_compra_producto */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'producto_compra_producto' && d.id == data.producto_compra_producto.id)
				return { results: [ data.producto_compra_producto ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for producto_compra_producto autofills */
		cache.addCheck(function(u, d) {
			if(u != tn + '_autofill.php') return false;

			for(var rnd in d) if(rnd.match(/^rnd/)) break;

			if(d.mfk == 'producto_compra_producto' && d.id == data.producto_compra_producto.id) {
				$j('#nombre_producto_compra_producto' + d[rnd]).html(data.nombre_producto_compra_producto);
				return true;
			}

			return false;
		});

		cache.start();
	});
</script>

